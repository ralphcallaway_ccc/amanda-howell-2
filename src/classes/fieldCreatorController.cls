public with sharing class fieldCreatorController {

public String obj { get; set; }
public List<SelectOption> objects { get; set; }
private String sessionId;
private String pod;
private String retUrl;
private Map<String,Schema.Sobjecttype> globalDescribe = Schema.getGlobalDescribe();
private Map<String,String> label2name = new Map<String, String>();
private Map<String,String> name2label = new Map<String, String>();
private static Set<String> addFieldableStandardObjects = new Set<String>();
private String action;


// pass session details and initial action to google app engine url
public String getCreateUrl() {
	if(null == pod || '' == pod)
		setPod();
	
	if(sessionId != null) {
		String metaDataUrl = 'https://' + pod + '-api.salesforce.com/services/Soap/m/15.0/' + UserInfo.getOrganizationId().substring(0,15);
		String endpointUrl = System.Label.FieldCreatorBaseUrl + System.Label.FieldCreatorJSP + '?' +
					+ 'action=' + action
					+ '&srv=' + metaDataUrl
					+ '&sid=' + sessionId;
		
		// don't add object info if we're creating a new object
		if(action == 'fields') {
			endpointUrl += '&objectName=' + obj
						+ '&objectLabel=' + name2Label.get(obj);						
		}		
		
		// strip any params off return url
		List<String> temp = retUrl.split('\\?');
		retUrl = temp[0];
		endpointUrl += '&retUrl=' + retUrl;
		return endpointUrl;
		
/*		if(action == 'fields') 
			baseUrl += System.Label.FieldCreatorJSP;
		else if (action == 'object')
			baseUrl += System.Label.FieldCreatorObjectJSP;
		else {
			system.assert(false,'Someting is wrong, I don\'t know what action to perform, because the action parameter has not been set');
		}			
		baseUrl += '?srv=' + metaDataUrl + '&sid=' + sessionId;
		
		// don't add object info if we're going to the object creator
		String endpointUrl = baseUrl;
		if( action == 'fields') {
			endpointUrl += '&objectName=' + obj;
			endpointUrl += '&objectLabel=' + name2label.get(obj);
		}*/
		
	} else { 
		return null;
	}	
}

public fieldCreatorController() {
	populateAddFieldableStandardObjects();
	populateObjects();
	sessionId = UserInfo.getSessionId();
}

public PageReference selectObject() {
	action = 'fields';
	return Page.fieldCreatorStep2;
}

public PageReference newObject() {
	action = 'object';
	return Page.fieldCreatorStep2;
}

public PageReference changeObject() {
	obj = '';
	return Page.fieldCreatorStep1;
}

private void setPod() {
	String referer = ApexPages.currentPage().getHeaders().get('Referer');
	system.debug('Refererer: ' + referer);
	if(referer.contains('visual.force')) {
		String temp = referer.replace('https://c.','');
		temp = temp.replaceAll('\\..*','');
		pod = temp;
		retUrl = 'https://c.' + pod + '.visual.force.com' + Page.FieldCreatorStep1.getUrl();
	} else {
		String temp = referer.replace('https://','');
		temp = temp.replaceAll('\\..*','');
		pod = temp;
		retUrl = 'https://' + pod + '.salesforce.com' + Page.FieldCreatorStep1.getUrl();
	}
	system.debug('Pod: ' + pod);
}

private void populateAddFieldableStandardObjects() {
	addFieldableStandardObjects.add('campaign');
	addFieldableStandardObjects.add('campaignmember');
	addFieldableStandardObjects.add('lead');
	addFieldableStandardObjects.add('account');
	addFieldableStandardObjects.add('contact');
	addFieldableStandardObjects.add('opportunity');
	addFieldableStandardObjects.add('case');
	addFieldableStandardObjects.add('contract');
	addFieldableStandardObjects.add('solution');
	addFieldableStandardObjects.add('product');
	addFieldableStandardObjects.add('idea');
	addFieldableStandardObjects.add('asset');
	addFieldableStandardObjects.add('user');
	addFieldableStandardObjects.add('opportunitylineitem');
}

private void populateObjects() {
	objects = new List<SelectOption>();
	objects.add(new SelectOption('','--Select Object--'));
	
	for (String s : globaldescribe.keySet()) {
		if(s.contains('__c') || addFieldableStandardObjects.contains(s)) {
			label2name.put(globaldescribe.get(s).getDescribe().getLabel(),globaldescribe.get(s).getDescribe().getName());
			name2label.put(globaldescribe.get(s).getDescribe().getName(),globaldescribe.get(s).getDescribe().getLabel());
		}
	}
	
	Set<String> keySet = label2name.keySet();
	List<String> labels = new List<String>();
	labels.addAll(keySet);
	labels.sort();
	
	for(String s : labels) {
		if(!s.contains('Tag')) // filter out tag objects as we can't add fields to them
			objects.add(new SelectOption(label2name.get(s), s));
	}
}

private static testmethod void tests() {
	fieldCreatorController controller = new fieldCreatorController();
	controller.dummyMethod();
}

public void dummyMethod() {
	Integer i = 0;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
	i++;
}



}