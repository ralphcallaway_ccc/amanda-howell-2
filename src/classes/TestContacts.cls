@isTest
private class TestContacts {
    public static testMethod void testNewContactController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        Adversary_Account__c advacc1 = new Adversary_Account__c (name__c='advesary1', isActive__c = true);
        insert advacc1;
        
        Adversary_Contact__c con = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test', Email__c='test@test.com');
        insert con;        
        Adversary_Contact__c con1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test11',Last_Name__c='test', Email__c='test@test.com');
        insert con1;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id,isActive__c =true );
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;
        
        Adversary_Contact__c conTest1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test3',Last_Name__c='test3',Add_To_Team__c=true, Email__c='test@test.com');
        insert conTest1 ;
        Adversary_Contact__c conTest2 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test4',Last_Name__c='test4',Email__c='test@test.com');
        Adversary_Contact__c conTest3 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='differenttest@test.com');
        PageReference currentPage = new PageReference('/apex/ViewAccount');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);
        currentPage.getParameters().put('contId',con.id);
        Test.setCurrentPage(currentPage );
        
        NewContactController newContactController = new NewContactController();
        
        //Test  back Method
        PageReference testPage = newContactController.back();
        System.assert(testPage.getUrl().contains('viewAccount'));                
        
        //Test  cancel Method
        testPage = newContactController.cancel();        
        System.assert(testPage.getUrl().contains('ViewContact'));
        System.assertEquals(con.id, testPage.getParameters().get('contId'));
        
        //Test  save and new Method               
        newContactController.contact = conTest2 ; //assigning a new Contact for save.
        testPage = newContactController.saveAndNew(); // Should return ContactValidation page, as duplicate contacts exists.   
        //System.assert(testPage.getUrl().contains('ContactValidation'));
        testPage = newContactController.activateContacts();//Call to activate Contacts.
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));//Error returned as none of the record is selected for activation.
        if(newContactController.getInactiveDuplicateExist()){
            newContactController.inactiveDuplicateContactList[0].isActive__c = true;// Select first record       
            testPage = newContactController.activateContacts(); // call the activate contact again.
            System.assert(testPage.getUrl().contains('CreateContact')); // record is saved and new contact page is returned
        }
        newContactController.contact.First_Name__c='test21';
        newContactController.contact.Last_Name__c='test21';
        newContactController.contact.Email__c='test@test.com';   
        newContactController.contact.Add_To_Team__c= true;     
        testPage  = newContactController.saveAndNew(); //Try saving again, save is not allowed as duplicate exist.
        testPage  = newContactController.continueSave(); // tell to save even if duplicate exists.
        System.assert(testPage.getUrl().contains('CreateContact'));  
        
        //Test  save Method
        newContactController.contact.First_Name__c='test22';
        newContactController.contact.Last_Name__c='test22';
        newContactController.contact.Email__c='test@test.com';                         
        testPage = newContactController.save(); // Should return ContactValidation page, as duplicate records exists.       
        //System.assert(testPage.getUrl().contains('ContactValidation'));
        //System.assert(newContactController.getActiveDuplicateExist());
        testPage  = newContactController.continueSave(); // tell to save even if duplicate exists.
        //System.assert(testPage.getUrl().contains('ViewContact'));
        //System.assertEquals(newContactController.contact.id, testPage.getParameters().get('contId'));
        testPage = newContactController.save(); // to cover exception as trying to reinsert the same contact.
        //System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
         //Test Update contact
        String tempMail = 'test22@test22.com';
        newContactController.contact.Email__c = tempMail ;
        testPage = newContactController.updateContact();
        System.assert(testPage.getUrl().contains('ViewContact'));
        List<Adversary_Contact__c> adCs = [select id from Adversary_Contact__c where Email__c = :tempMail]; 
        System.assert(adCs.size()>0);
        
        //Test  editContact Method
        testPage = newContactController.editContact();        
        System.assert(testPage.getUrl().contains('EditContact'));
        System.assertEquals(con.id, testPage.getParameters().get('contId'));
        
        //Test deactivateProject         
        testPage = newContactController.deactivateProject(); // result an error as no project is set for deactivation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));  
        newContactController.projIdToDeActivate= pro.Id; //set id for deactivation
        testPage = newContactController.deactivateProject(); //re-try     
        Project__c pr1= [select isActive__c from Project__c where Id = :pro.Id LIMIT 1]; 
        System.assert(!pr1.isActive__c); //test if it is deactivated
        System.assert(ApexPages.hasMessages(ApexPages.severity.INFO));
               
        newContactController.contact = conTest3;
        newContactController.accType = 'Adversary';
        newContactController.addToTeam = true;
        newContactController.source= 'teambuilder';
        testPage  = newContactController.save();
        System.debug('Return Page Is---->' + testPage.getUrl());
        System.assert(testPage.getUrl().contains('AdversaryTeamBuilder'));      
    }
 
}