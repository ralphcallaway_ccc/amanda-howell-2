public Class AdversaryTeamBuilder{
    Set<String> originalValuesLeft = new Set<String>();
    Set<String> originalValuesRight = new Set<String>();
    Map<String,Adversary_Team_Member__c> currentTeamMemberMap = new Map<String,Adversary_Team_Member__c>();
    Map<String,ID> contactNameIDMap = new Map<String,Id>();
    Public List<String> leftselected{get;set;}
    Public List<String> rightselected{get;set;}
    Set<String> leftvalues = new Set<String>();
    Set<String> rightvalues = new Set<String>();
    Set<String> membersToAdd = new Set<String>();
    Set<String> membersToDelete = new Set<String>();
    public String accId {get; set;}
    public String projId {get; set;}
    public Project__c currentProject {get; set;}
    public Boolean hasEditAccess {get;set;}
    public String AccessLevel {get;set;}
    /* below variables are used for new contact creation */
    public boolean continueWithNewContactCreation {get;set;}
    public String multiple = '';
    public Adversary_Contact__c contact {get; set;}
    public Adversary_Account__c account {get;set;}
    public boolean addToTeam {get; set;}
    public boolean duplicateExist {get; set;}
    public List<Adversary_Contact__c> activeDuplicateContactList {get; set;}
    public List<Adversary_Contact__c> inactiveDuplicateContactList {get; set;}
    
    public AdversaryTeamBuilder(){
        leftselected = new List<String>();
        rightselected = new List<String>();
        hasEditAccess = false;      
        //leftvalues.addAll(originalValues);
        accId = ApexPages.currentPage().getParameters().get('accId');
        projId = ApexPages.currentPage().getParameters().get('projId');
        continueWithNewContactCreation = false;
       
        // Ensure account is active, else give error.
        if(![select Adversary_Account__r.isActive__c from Project__c where id = :projId].Adversary_Account__r.isActive__c){
            hasEditAccess = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, Label.AdversaryAccountInactiveError)); 
            return;
        }   
        //Get all already select team members
        for(Adversary_Team_Member__c teamMember : [select id,Adversary_Contact__r.Name__c from Adversary_Team_Member__c where Project__c =: projId and Adversary_Contact__r.Adversary_Account__c =:accId]){
            rightValues.add(teamMember.Adversary_Contact__r.Name__c); 
            currentTeamMemberMap.put(teamMember.Adversary_Contact__r.Name__c,teamMember);
        }
        
        originalValuesRight.addAll(rightValues);
        
        //Get all contacts related to account
        for(Adversary_Contact__c objCon : [select id,Name__c from Adversary_Contact__c where Adversary_Account__c =:accId and isActive__c=true]){
            contactNameIdMap.put(objCon.Name__c,objCon.Id);
            if(!rightValues.contains(objCon.Name__c))
                leftvalues.add(objCon.Name__c);
        }
        
        originalValuesLeft.addAll(leftValues);
        
        AccessLevel  = CNMSUtility.getCurrentUserProjectAccessLevel(projId);
        if(AccessLevel =='Edit' || AccessLevel =='All'){
            for(Our_Team_Member__c otm: [select User__c from Our_Team_Member__c where Project__c =: projId]){
                if(otm.User__c == UserInfo.getUserId()){
                    hasEditAccess = true;
                    break;
                }
            }                
        }
    }
    
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }

    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
    
    public PageReference addContact(){
        PageReference redirectPage = new PageReference('/apex/createContactFromATMBuilder?accId='+accId+'&projId='+projId);
        return redirectPage;
    }
    
    public PageReference Cancel(){
        return  new PageReference('/apex/AdversaryTeamBuilder?accId='+accId + '&projId=' +projId);
    }
    
    public List<SelectOption> getUnSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<String> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(String s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }

    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<String> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    
    public PageReference done(){
        System.debug('rightvalues****'+rightValues);
        System.debug('originalRightvalue****'+originalvaluesRight);
        System.debug('leftvalues****'+leftValues);
        System.debug('originalLeftvalue****'+originalValuesLeft);
        try{
            //Creating set of newly selected values
            for(String rightValue : rightValues){
                if(!originalValuesRight.contains(rightValue))
                    membersToAdd.add(rightValue);
            }
            //Creating set of unselected values
            for(String originalValueRight : originalValuesRight){
                if(!rightValues.contains(originalValueRight))
                    membersToDelete.add(originalValueRight);
            }
            
            System.debug('membersToAdd****'+membersToAdd);
            System.debug('membersToDelete****'+membersToDelete);
            
            //Create Team Memebers list to be inserted
            List<Adversary_Team_Member__c> teamMembersToInsert = new List<Adversary_Team_Member__c>();
            for(String contactName : membersToAdd){
                Adversary_Team_Member__c newMember = new Adversary_Team_Member__c(Project__c=projId,Adversary_Contact__c=contactNameIdMap.get(contactName));
                teamMembersToInsert.add(newMember);
            }
            
            //Create Team Memebers list to be deleted
            List<Adversary_Team_Member__c> teamMembersToDelete = new List<Adversary_Team_Member__c>();
            for(String contactName : membersToDelete){
                Adversary_Team_Member__c delMember = currentTeamMemberMap.get(contactName);
                teamMembersToDelete.add(delMember);
            }
            
            System.debug('teamMembersToInsert****'+teamMembersToInsert);
            System.debug('teamMembersToDelete****'+teamMembersToDelete);
            
            if(teamMembersToInsert!=null && teamMembersToInsert.size()>0)
                insert teamMembersToInsert;
                
            if(teamMembersToDelete!=null && teamMembersToDelete.size()>0)
                delete teamMembersToDelete;
            
            PageReference redirectPage = new PageReference('/apex/viewProject?Id='+projId);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
        
    }
    
    /* new Contact creation APIs. */
    public PageReference save(){
        try{
            /*if(!continueWithNewContactCreation && checkDuplicates()){
                multiple = '0';
                duplicateExist = true;
                //PageReference redirectPage = new PageReference('/apex/ContactValidation');
                return null ;//redirectPage;
            }*/
            if(checkDuplicates()){
                multiple = '0';  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contact already exist with this name.'));              
                return null;
            }
            
            if(contact.Add_To_Team__c){
                addToTeam = true;
                contact.Add_To_Team__c = false;
            }
            insert contact;
            ProcessAddToTeam();           
            PageReference redirectPage = new PageReference('/apex/AdversaryTeamBuilder?accId='+accId+'&projId='+projId);           
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public PageReference saveAndNew(){
        try{
           /* if(!continueWithNewContactCreation && checkDuplicates()){
                multiple = '1';
                //PageReference redirectPage = new PageReference('/apex/ContactValidation');
                return null;
            }*/
            if(checkDuplicates()){
                multiple = '1';  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contact already exist with this name.'));              
                return null;
            }
            if(contact.Add_To_Team__c){
                addToTeam = true;
                contact.Add_To_Team__c = false;
            }
            insert contact;         
            ProcessAddToTeam();
            contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
            addToTeam = false;
            duplicateExist = false;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }    
        return null;
    }
    
    private void ProcessAddToTeam(){
        Adversary_Contact__c objCon = [select id,Name__c from Adversary_Contact__c where id = :contact.Id];
        contactNameIdMap.put(objCon.Name__c,objCon.Id);                 

        //Add this contact to Adversary Team
        if(addToTeam && projId!=null && projId!=''){
            PageReference redirectPage; 
            Adversary_Team_Member__c newMember = new Adversary_Team_Member__c(Adversary_Contact__c=objCon.id,project__c=projId);
            insert newMember;
            //Adversary_Team_Member__c teamMember = [select id,Adversary_Contact__r.Name__c from Adversary_Team_Member__c where id =:newMember.Id];
            rightValues.add(objCon.Name__c); //teamMember.Adversary_Contact__r.Name__c); 
            currentTeamMemberMap.put(objCon.Name__c, newMember); //teamMember.Adversary_Contact__r.Name__c,teamMember);
            originalValuesRight.add(objCon.Name__c); //teamMember.Adversary_Contact__r.Name__c);
            
        }
        else{
            leftvalues.add(objCon.Name__c);
            originalValuesLeft.add(objCon.Name__c); // this is a new contact created, hence add it to original left values.
        }
        
    }
    public pageReference continueSave(){
        try{
            continueWithNewContactCreation = true;
            if(multiple =='0')        
                return save();
                
            else{
               saveAndNew();
               continueWithNewContactCreation = false;
               multiple = '';
               return null;
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        
    }
    
    public pageReference activateContacts(){
        boolean noneSelected = true;
        for(Adversary_Contact__c a : inactiveDuplicateContactList ){
            if(a.isActive__c){
                noneSelected = false;
                break;
            }            
        }
        if(noneSelected ){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.NoContactSelectedToActivate));
            return null;
        }
        try{
            update inactiveDuplicateContactList ;
            for(Adversary_Contact__c objCon : inactiveDuplicateContactList){
                if(objCon.isActive__c){
                    if(contact.Add_To_Team__c){
                        addToTeam = true;
                        contact.Add_To_Team__c = false;
                    }
                    contactNameIdMap.put(objCon.Name__c,objCon.Id); 
                    //Add this contact to Adversary Team
                    if(addToTeam && projId!=null && projId!=''){                        
                        Adversary_Team_Member__c newMember = new Adversary_Team_Member__c(Adversary_Contact__c=objCon.id,project__c=projId);
                        insert newMember;
                        //Adversary_Team_Member__c teamMember = [select id,Adversary_Contact__r.Name__c from Adversary_Team_Member__c where id =:newMember.Id];
                        rightValues.add(objCon.Name__c); //teamMember.Adversary_Contact__r.Name__c); 
                        currentTeamMemberMap.put(objCon.Name__c, newMember); //teamMember.Adversary_Contact__r.Name__c,teamMember);
                        originalValuesRight.add(objCon.Name__c); //teamMember.Adversary_Contact__r.Name__c);
                        
                    }
                    else{
                        leftvalues.add(objCon.Name__c);
                        originalValuesLeft.add(objCon.Name__c); // this is a old contact activated , hence add it to original left values.  
                    }       
                }
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        //PageReference redirectPage;
        if(multiple =='0')        
            return new PageReference('/apex/AdversaryTeamBuilder?accId='+accId+'&projId='+projId);  
            
        else{
            contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
            multiple = '';
            continueWithNewContactCreation = false;
            duplicateExist = false;
            addToTeam = false;
            return null;
        }
        
        //return redirectPage;
    }
    
    public boolean checkDuplicates(){
        try{
            activeDuplicateContactList = [select id, Name__c, isActive__c, Phone__c, Email__c from Adversary_Contact__c where Adversary_Account__c = :accId AND First_Name__c= :contact.First_Name__c AND Last_Name__c= :contact.Last_Name__c AND isActive__c=true];
            inactiveDuplicateContactList = [select id, Name__c, isActive__c, Phone__c, Email__c from Adversary_Contact__c where Adversary_Account__c = :accId AND First_Name__c= :contact.First_Name__c AND Last_Name__c= :contact.Last_Name__c AND isActive__c = false];
            
            if((activeDuplicateContactList != null && activeDuplicateContactList.size()>0) ||
               (inactiveDuplicateContactList != null && inactiveDuplicateContactList.size()>0)){
                //duplicateExist = true;
                return true;
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        return false;
    }
    
    public boolean getActiveDuplicateExist() {
        return activeDuplicateContactList !=null && activeDuplicateContactList.size()>0 ? true:false;
    }
    
    public boolean getInactiveDuplicateExist() {
        return inactiveDuplicateContactList !=null && inactiveDuplicateContactList.size()>0 ? true:false;
    }
    
    public void initNewContact(){
        continueWithNewContactCreation =false;
        addToTeam = false;
        duplicateExist = false;
        contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
        account = [select Id, name__c from Adversary_Account__c where Id = :accId LIMIT 1];
        
    }
}