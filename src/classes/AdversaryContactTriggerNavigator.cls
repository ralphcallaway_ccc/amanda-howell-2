public Class AdversaryContactTriggerNavigator{
    List<Adversary_Contact__c> newRecords;
    List<Adversary_Contact__c> oldRecords;
    //Constructor
    public AdversaryContactTriggerNavigator(List<Adversary_Contact__c> TriggerNewRecords,  List<Adversary_Contact__c> TriggerOldRecords, Map<Id,Adversary_Contact__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        AdversaryContactTriggerUtility.setNewRecords(newRecords);
        AdversaryContactTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void SetUniqueName(){
        AdversaryContactTriggerUtility.SetUniqueName();
    }
    
    public void DeleteRelatedAdversaryTeamMembers(){
        AdversaryContactTriggerUtility.DeleteRelatedAdversaryTeamMembers();
    }
    
}