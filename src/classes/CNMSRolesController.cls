public Class CNMSRolesController {
    private final String[] Roles = new String[]{Label.RedCrown, Label.CEO, Label.Coach , Label.TeamMember}; // Types of possible roles. 
    public List<CNMS_Role__c> allCNMSRoles {get;set;}              // List holds all the CNMS Roles currently present in the system
    public List<CNMS_Role__c> ceoCNMSRoles {get;set;}                 // List holds all the CEO type CNMS Roles currently present in the system
    public List<CNMS_Role__c> coachCNMSRoles {get;set;}               // List holds all the COACH type CNMS Roles currently present in the system
    public List<CNMS_Role__c> TMCNMSRoles {get;set;}                  // List holds all the TM type CNMS Roles currently present in the system
    public List<CNMS_Role__c> newCNMSRoles {get;set;}                  // List to hold new CNMS Roles to be created for available users.
    
    public Set<Id> allRoleUsersIdList {get;set;}                  // List holds User IDs of All CNMS Roles currently present in the system    
    
    public Boolean isAdmin {get;set;}
    public CNMSRolesController(){      
        isAdmin = true;
        // Check if curent user is admin, then only allow access.      
        if(!CNMSUtility.isAdminUser(UserInfo.getUserId())){
            isAdmin = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, Label.NoAccessPrivilegeMessage));
        }   
        prepareCNMSRolesList(); // Create the individual lists of Roles/Users/Ids
            
        // Display success message if page is redirected with susscess parameter as true.
        if(ApexPages.currentPage().getParameters().get('success')=='true'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info, Label.RoleSuccessMessage));  
        }
    }
    
        
    public Integer getCEOCNMSRolesCount(){
        return ceoCNMSRoles.size();
    }
    public Integer getCoachCNMSRolesCount(){
        return coachCNMSRoles.size();
    }
    public Integer getTMCNMSRolesCount(){
        return TMCNMSRoles.size();
    } 

    private void prepareCNMSRolesList(){
        // Initialize Lists.
        allRoleUsersIdList = new Set<Id>();         
        ceoCNMSRoles       = new List<CNMS_Role__c>();      
        coachCNMSRoles     = new List<CNMS_Role__c>();    
        TMCNMSRoles        = new List<CNMS_Role__c>();
        newCNMSRoles       = new List<CNMS_Role__c>();
        allCNMSRoles = [select name, Role_Type__c, user__c from CNMS_Role__c]; // Pull all the CNMS Roles records from the system.
        for(CNMS_Role__c role: allCNMSRoles){
            allRoleUsersIdList.add(role.User__c);                
            if(role.Role_Type__c == Label.CEO){
                ceoCNMSRoles.add(role);               
            }
            else if(role.Role_Type__c == Label.Coach){
                coachCNMSRoles.add(role);               
            }
            else if(role.Role_Type__c == Label.TeamMember){
                TMCNMSRoles.add(role);               
            }
        }       
       
    }    
  

    public PageReference back(){
        PageReference pr = new PageReference('/apex/CNMSRoles');
        pr.setRedirect(true);        
        return pr;
    } 
    
    public PageReference UpdateCNMSRoles(){
        return new PageReference('/apex/ManageCNMSRoles');
    } 
    
}