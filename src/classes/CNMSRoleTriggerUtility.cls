public Class CNMSRoleTriggerUtility{
    static List<CNMS_Role__c> newRecords;
    static List<CNMS_Role__c> oldRecords;
    static Map<Id,CNMS_Role__c> oldRecordsMap;
    
    public static void setNewRecords(List<CNMS_Role__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<CNMS_Role__c> records, Map<Id,CNMS_Role__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
       
    public static void ShareAccountsWithNewUserRole(){
        /*List<Adversary_Account__Share> AccountSharingList = new List<Adversary_Account__Share>();
        List<Adversary_Account__c> allAccounts = [select Id from Adversary_Account__c];
        for(CNMS_Role__c role : newRecords){
            for(Adversary_Account__c aa : allAccounts){
                Adversary_Account__Share newShare = new Adversary_Account__Share(UserOrGroupId = role.User__c, ParentId = aa.Id, AccessLevel = 'Read');
                AccountSharingList.add(newShare );
            }
        }       
        if(AccountSharingList.size()>0){
            Database.Insert(AccountSharingList, false);
        }*/
    }
    
    public static void UnshareAccountsWithDeletedUserRole(){
        List<Adversary_Account__Share> AccountSharingList = new List<Adversary_Account__Share>();       
        Set<Id> userIdOfDeletedRoles = new Set<Id>();
        for(CNMS_Role__c role : oldRecords){
            userIdOfDeletedRoles.add(role.user__c);        
        }
        AccountSharingList = [select id from Adversary_Account__Share where UserOrGroupId IN :userIdOfDeletedRoles];
        if(AccountSharingList.size()>0){
            Database.Delete(AccountSharingList, false);
        }
    }
	
	public static void createOTMForCEORoles(){
    	List<Our_Team_Member__c> newOtms = new List<Our_Team_Member__c>();
	    List<Project__c> allProjects = [select Id, ownerId from Project__c];               
	    for(CNMS_Role__c role: newRecords ){ 
	        if(role.Role_Type__c==Label.CEO){
	            for(Project__c p :allProjects){
	                // Create new our Team Member for each Project n each role.
	                Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = p.Id, User__c=role.User__c, Role__c=role.Role_Type__c, Access__c='Edit');                            
	                if(p.OwnerId == role.User__c)
	                    newMember.Access__c='All';    
	                newOtms.add(newMember);
	            }
	        }
	    }
	    Database.insert(newOtms,false);      
    }
    
    
    public static void updateRolesOperations(){
        Map<ID, CNMS_Role__c> updatedUserToRoleMap = new Map<ID, CNMS_Role__c>();
        Map<ID, CNMS_Role__c> updatedUserToCoachRoleMap = new Map<ID, CNMS_Role__c>();
		Map<ID, CNMS_Role__c> deletedUserToRoleMap = new Map<ID, CNMS_Role__c>();
        List<CNMS_Role__c> rolesToBeUpdated = new List<CNMS_Role__c>();
        List<Our_Team_Member__c> otmsToUpdate = new List<Our_Team_Member__c>();
        List<Our_Team_Member__c> otmsToDelete = new List<Our_Team_Member__c>();
        List<Our_Team_Member__c> otmsToInsert = new List<Our_Team_Member__c>();
        List<Project__c> allProjects = [select id, (select User__c, Role__c, Access__c from Our_Team_Members__r) from Project__c];
        Set<ID> alreadyCreatedGumboOTM = new Set<ID>();
        List<Id> usersIDForWhomRolesToBeDeleted = new List<Id>();
        Set<Id> usersIDForWhomRolesChangedToCoach = new Set<Id>();
        for(CNMS_Role__c role: newRecords ){
			CNMS_Role__c oldRole = oldRecordsMap.get(role.Id);
			// CASE 1: Original Role was CEO
            if(oldRole.Role_Type__c == Label.CEO){ // Role is changing from CEO(Dumbo)
				if(role.Role_Type__c == null){
					// role has been deleted.
					usersIDForWhomRolesToBeDeleted.add(role.User__c);
					deletedUserToRoleMap.put(role.User__c, role);
				}
				else if(role.Role_Type__c == Label.COACH){ // Role changed to Coach, allow only if user is not project owner.
					usersIDForWhomRolesChangedToCoach.add(role.User__c);
					updatedUserToCoachRoleMap.put(role.User__c, role);
				}
				else if(role.Role_Type__c != Label.CEO){ // changed to any other role.
					//role changed.
					updatedUserToRoleMap.put(role.user__c, role);
					rolesToBeUpdated.add(role);
				}     
			}			
			
			// CASE 2: Original Role was Coach
			else if(oldRole.Role_Type__c == Label.Coach){ // Role is changing from Coach
				if(role.Role_Type__c == null){
					// role has been deleted.
					usersIDForWhomRolesToBeDeleted.add(role.User__c);
					deletedUserToRoleMap.put(role.User__c, role);
				}
				else if(role.Role_Type__c != Label.Coach){
					//role changed.
					updatedUserToRoleMap.put(role.user__c, role);
					rolesToBeUpdated.add(role);
				}  
			}
			
			// CASE 3: Original Role was TeamMember
			else if(oldRole.Role_Type__c == Label.TeamMember){ // Role is changing from TeamMember
				if(role.Role_Type__c == null){
					// role has been deleted.
					usersIDForWhomRolesToBeDeleted.add(role.User__c);
					deletedUserToRoleMap.put(role.User__c, role);
				}
				else if(role.Role_Type__c == Label.COACH){ // Role changed to Coach, allow only if user is not project owner.
					usersIDForWhomRolesChangedToCoach.add(role.User__c);
					updatedUserToCoachRoleMap.put(role.User__c, role);
				}
				else if(role.Role_Type__c != Label.TeamMember){ // changed to any other role.
					//role changed.
					updatedUserToRoleMap.put(role.user__c, role);
					rolesToBeUpdated.add(role);
				} 
			}
        }
		
        if(rolesToBeUpdated.size()>0){
            List<Our_Team_Member__c> existingOTMs = [select User__c, Role__c, Access__c, project__r.ownerId from Our_Team_Member__c where User__c IN :updatedUserToRoleMap.keySet()];
            Map<Id, List<Our_Team_Member__c>> userIdOTMListMap =new Map<Id, List<Our_Team_Member__c>>();
            for(Our_Team_Member__c otm : existingOTMs){
                List<Our_Team_Member__c> tempList = userIdOTMListMap.get(otm.User__c);
                if(tempList!=null){
                    tempList.add(otm);
                }
                else{
                    tempList = new List<Our_Team_Member__c>();
                    tempList.add(otm);
                    userIdOTMListMap.put(otm.User__c, tempList);
                }
            }
            for(Id userId : updatedUserToRoleMap.keySet()){
                List<Our_Team_Member__c> existingOTMsForThisUser = userIdOTMListMap.get(userId);
				CNMS_Role__c oldRole = oldRecordsMap.get(updatedUserToRoleMap.get(userId).Id);
                if(existingOTMsForThisUser!=null && existingOTMsForThisUser.size()>0){
                    for(Our_Team_Member__c otm : existingOTMsForThisUser){                       
                        //otmsToUpdate.add(otm);
						
                        // Case 1: Role is changed from Gumbo to TM then delete all the related OTMs except Project Owners
                        if(oldRole.Role_Type__c==Label.CEO && userId != otm.project__r.ownerId){
                            otmsToDelete.add(otm);
                        }
                        // case 2: Role is changed to Gumbo from TM, then update access to 'edit' and create new role if any project doesn't already has a member for this user
                        else if(updatedUserToRoleMap.get(userId).Role_Type__c==Label.CEO){
                            if(userId != otm.project__r.ownerId){
                                otm.Access__c = 'Edit';
                                otm.Role__c = Label.CEO;
                                otmsToUpdate.add(otm);
                            }
                            // create new otm if any project doesn't already have it and OTMs not already created for this user.
                             if(!alreadyCreatedGumboOTM.contains(userId)){   
                                for(project__c pr: allProjects ){
                                    boolean hasMember = false;
                                    for(Our_Team_Member__c otmPr : pr.Our_Team_Members__r){
                                        if(userId == otmPr.user__c){
                                            hasMember = true;
                                            break;
                                        }
                                    }
                                    if(!hasMember){
                                        //create new team member for this project.
                                        Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = pr.Id, User__c=userId, Role__c = Label.CEO, Access__c='Edit');
                                        otmsToInsert.add(newMember);
                                        alreadyCreatedGumboOTM.add(userId);
                                    }
                                }
                            }   
                        }
                        
                        // case 3: Role is changed from Coach to Team Member, then update the role  to TM.
                        else if(oldRole.Role_Type__c==Label.Coach && updatedUserToRoleMap.get(otm.user__c).Role_Type__c==Label.TeamMember && userId != otm.project__r.ownerId){
                            otm.Role__c = Label.TeamMember;
                            otmsToUpdate.add(otm);
                        }
                        // case 4: Role is changed from  Team Member to Coach, then update the role  to Coach and access to view.
                        else if(oldRole.Role_Type__c==Label.TeamMember && updatedUserToRoleMap.get(otm.user__c).Role_Type__c==Label.Coach && userId != otm.project__r.ownerId){
                            otm.Role__c = Label.Coach;
                            otm.access__c = 'View';
                            otmsToUpdate.add(otm);
                        }
                        
                    }
                }
                else { // this is a special case where no OTM for a role exist in the system. Then if the role is Dumbo, we need to create a OTM for each existing project.
                    // case 2: Role is changed to Gumbo from Coach/TM, create new role if any project doesn't already has a member for this user
                    if(updatedUserToRoleMap.get(userId).Role_Type__c==Label.CEO){                        
                        // create new otm on all project.                         
                        for(project__c pr: allProjects ){                                                   
                            //create new team member for this project.
                            Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = pr.Id, User__c=userId, Role__c = Label.CEO, Access__c='Edit');
                            otmsToInsert.add(newMember);                            
                        }
                          
                    }
                }
            }
        }
        
        // Process roles changed to Coach 
        Set<ID> userIdforOtmDelete = new Set<ID>();
        Set<ID> userIdforOtmUpdate= new Set<ID>();
        if(usersIDForWhomRolesChangedToCoach.size()>0){               
            // Do not allow deletion if user is owner of any project.
            List<Project__c> projectsForCoachError  = [select id, OwnerId, Owner.Name, name from Project__c where OwnerId in :usersIDForWhomRolesChangedToCoach];// AND isActive__c = true];
            Set<Id> projectOwners = new Set<Id>();
            if(projectsForCoachError.size()>0){
                
               // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Following users are Project owners, hence their roles cannot be changed to Coach'));
                for(Project__c pr:projectsForCoachError){
                    projectOwners.Add(pr.OwnerId); 
					updatedUserToCoachRoleMap.get(pr.OwnerId).addError(pr.Owner.Name + ' cannot be changed to Coach as the user owns the project '+ pr.name);
                    usersIDForWhomRolesChangedToCoach.remove(pr.OwnerId); // Remove this from update list, as this user's role cannot be updated.
                }
            }
            for(Id id: usersIDForWhomRolesChangedToCoach){
                // Add the role for updation:
                rolesToBeUpdated.add(updatedUserToCoachRoleMap.get(id));
				CNMS_Role__c oldRole = oldRecordsMap.get(updatedUserToCoachRoleMap.get(id).Id);
                // if role changed from DUMBO To Coach
                if(oldRole.Role_Type__c==Label.CEO){
                    // need to delete all corresponding OTMs except proj owners.
                    userIdforOtmDelete.add(id);
                }
                // if role changed from TM To Coach
                else if(oldRole.Role_Type__c==Label.TeamMember){
                    // need to update all corresponding OTMs Role to coach, access to view except proj owner .
                    userIdforOtmUpdate.add(id);
                }
            }
            if(userIdforOtmDelete.size()>0){
                otmsToDelete.addAll([select Id from Our_Team_Member__c where User__c IN :userIdforOtmDelete]);
            }
            if(userIdforOtmUpdate.size()>0){
                for(Our_Team_Member__c otm: [select Id, Role__c,access__c from Our_Team_Member__c where User__c IN :userIdforOtmUpdate]){
                    otm.Role__c = Label.Coach;
                    otm.access__c = 'View';
                    otmsToUpdate.add(otm);
                }
            }           
        }  
        
        if(usersIDForWhomRolesToBeDeleted.size()>0)
        {   
            // Do not allow deletion if user is owner of any project.
            List<Project__c> projectsForError  = [select id, OwnerId, Owner.Name, name from Project__c where OwnerId in :usersIDForWhomRolesToBeDeleted AND isActive__c = true];
            Set<Id> projectOwners = new Set<Id>();
            if(projectsForError.size()>0){
                
               // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error in updating/deleting some of the roles'));
                for(Project__c pr:projectsForError){
                    projectOwners.Add(pr.OwnerId); 
					deletedUserToRoleMap.get(pr.OwnerId).addError(pr.Owner.Name + ' cannot be deleted as the user owns the project ' + pr.Name);
                }
            }
            database.delete([select Id from CNMS_Role__c where User__c IN :usersIDForWhomRolesToBeDeleted AND User__c NOT IN :projectOwners],false);
            database.delete([select Id from Our_Team_Member__c where User__c IN :usersIDForWhomRolesToBeDeleted AND User__c NOT IN :projectOwners],false);//Delete corresponing team members too.                                                               
        }  
            
        
       /* if(rolesToBeUpdated.size()>0){
            update rolesToBeUpdated;
        }*/
        if(otmsToInsert.size()>0){
            insert otmsToInsert;
        }
        if(otmsToUpdate.size()>0){
            update otmsToUpdate;
        }
        if(otmsToDelete.size()>0){
            delete otmsToDelete;
        }
     }   
	
	public static void preventRoleDeletionForProjectowners(){
		Set<Id> usersIDForWhomRolesToBeDeleted = new Set<Id>();
		Map<Id, CNMS_Role__c> deletedUserToRoleMap = new Map<Id, CNMS_Role__c>();
		for(CNMS_Role__c role : oldRecords){
			usersIDForWhomRolesToBeDeleted.add(role.User__c);
			deletedUserToRoleMap.put(role.User__c, role);
		}
		if(usersIDForWhomRolesToBeDeleted.size()>0)
        {   
        // Do not allow deletion if user is owner of any project.                          
           for(Project__c pr:[select id, OwnerId, Owner.Name, name from Project__c where OwnerId in :usersIDForWhomRolesToBeDeleted AND isActive__c = true]){         
				deletedUserToRoleMap.get(pr.OwnerId).addError(pr.Owner.Name + ' cannot be deleted as the user owns the project ' + pr.Name);
            }
                                                                                     
        }  
	}
	
	public static void deleteRelatedOTMS(){
		Set<Id> usersIDForWhomRolesToBeDeleted = new Set<Id>();		
		for(CNMS_Role__c role : oldRecords){
			usersIDForWhomRolesToBeDeleted.add(role.User__c);			
		}
		database.delete([select Id from Our_Team_Member__c where User__c IN :usersIDForWhomRolesToBeDeleted],false);
	}

}