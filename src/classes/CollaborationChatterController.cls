public Class CollaborationChatterController {

   public String collabId {get; set;}
   public List<FeedItem> FeedItems {get; set;}
   public Integer FeedCount {get; set;}
    
    public CollaborationChatterController(){
        collabId = ApexPages.currentPage().getParameters().get('id');
        System.debug('Collab Id---->' + collabId );
        FeedItems = [select id,body,InsertedBy.Name,CreatedDate,(select id,CommentBody,CreatedDate,InsertedBy.Name from FeedComments) from FeedItem where ParentId=:collabId AND ID <> null];
        FeedCount  = FeedItems.size();
    }
   
   
   public static testMethod void testCollaborationChatterController(){
       new CollaborationChatterController();
   }
}