/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

    @isTest
private class TestManageCNMSRolesController {
     public static testMethod void testManageCNMSRolesController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro;
        Project__c pro1 = new Project__c(name='test1',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro1;
        
        String[] RoleTypes = new String[]{Label.CEO,Label.COACH, Label.TeamMember};
        
        List<User> users = new List<User>();
        List<Our_Team_Member__c>  otms = new List<Our_Team_Member__c>();
        List<CNMS_Role__c>  roles = new List<CNMS_Role__c>();
        // Create total 10 users (3+5, 2 extra)
        for(Integer i = 0; i<10; i++){
            User u = createUser('testing'+i+'@testorg.com', 't'+i);
            u.isTeamMember__c = true;
            u.Client_Account__c = acc.id;
            u.isActive = true;
            u.lastname='Testing'+i;
            u.firstname = 'Testing'+i;
            users.add(u);
            
        } 
        insert users;
        
        // Create total 3 Roles
        for(Integer i = 0; i<3; i++){
            CNMS_Role__c rl = new CNMS_Role__c();
            rl.User__c = users[i].Id;
            rl.Role_Type__c = RoleTypes[i];
            roles.add(rl);
        } 
        //create a role for current user
        roles.add(new CNMS_Role__c(User__c = users[3].Id, Role_Type__c = Label.TeamMember));
        insert roles;
        //Create total 3 TMs. user0,1 and 2.
        for(Integer i = 0; i<3; i++){
            Our_Team_Member__c tm = new Our_Team_Member__c (User__c =users[i].Id, Project__c = pro.id); 
            tm.Role__c = RoleTypes[i];
            tm.Access__c = 'View';
            otms.add(tm);            
        }         
        insert otms ;
        
        PageReference currentPage = new PageReference('/apex/ManageCNMSRoles');            
        Test.setCurrentPage(currentPage );
        ManageCNMSRolesController CNMSRolesController = new ManageCNMSRolesController();

        // change some roles.
        CNMSRolesController.allCNMSRoles[0].Role_Type__c = Label.TeamMember; // coach to TM
        CNMSRolesController.allCNMSRoles[1].Role_Type__c = Label.CEO; // TM to DUMBO       
        
        
        CNMSRolesController.save();
        CNMSRolesController.addRole();
		CNMSRolesController.removeRole();
	    CNMSRolesController.addRole();
	    CNMSRolesController.allCNMSRoles[CNMSRolesController.allCNMSRoles.size()-1].User__c = users[8].Id;
	    CNMSRolesController.allCNMSRoles[CNMSRolesController.allCNMSRoles.size()-1].Role_Type__c = Label.CEO;
	    CNMSRolesController.addRole();
	    CNMSRolesController.roleId = CNMSRolesController.allCNMSRoles[0].Id;
	    CNMSRolesController.removeRole();
	    	    
        CNMSRolesController.back();
    }
    
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}