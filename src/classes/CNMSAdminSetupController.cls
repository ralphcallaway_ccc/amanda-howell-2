public Class CNMSAdminSetupController {
    
    public List<User> AllUsers { get; set; }
    public List<User> SysAdminUsers { get; set; }
    public List<User> CNMSAdminUsers { get; set; }
    public List<User> OtherUsers { get; set; }
    public ID currentUserId{ get; set; }
    public Boolean isAdmin { get; set; }
    
    public Integer getSysAdminUsersCount(){
        return SysAdminUsers.size();
    }
    public Integer getCNMSAdminUsersCount(){
        return CNMSAdminUsers.size();
    }
    public Integer getOtherUsersCount(){
        return OtherUsers.size();
    }
    
    public CNMSAdminSetupController(){
        currentUserId = UserInfo.getUserId();
        isAdmin = true; 
        // Check if curent user is admin, then only allow access.   
        if(!CNMSUtility.isAdminUser(currentUserId)){
             isAdmin = false;
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, Label.NoAccessPrivilegeMessage));
             return;
         }          
        PrepareUsersList();
    }
    
    private void PrepareUsersList(){
         SysAdminUsers = [select Id, name, CNMS_Admin__c, Profile.Name from User where Profile.Name = 'System Administrator' Order By Name];
    }
    
    public PageReference save(){
        update SysAdminUsers;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info, 'Users updated successfully.'));
        return null;
    }
    
    public PageReference back(){
        return Page.ProjectList;
    }
}