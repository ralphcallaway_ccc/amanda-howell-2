public with sharing class ClientAccountController {   

    public Client_Account__c clientAccount {get;set;}
    public List<User> users{get;set;}
    public List<User> usersToDisplay {get;set;}
    public String source {get;set;}
    public String projId {get;set;}
    public ClientAccountController() {
             clientAccount = [select name__c, phone__c, Fax__c, Website__c,isActive__c from Client_Account__c where
                               id = :ApexPages.currentPage().getParameters().get('accId') LIMIT 1];
            users = [select id, name, email,phone, isTeamMember__c, isActive from User
                     where isTeamMember__c = true AND isActive = true AND Client_Account__c = :clientAccount.Id ];
            source  = ApexPages.currentPage().getParameters().get('source');
            projId= ApexPages.currentPage().getParameters().get('projId');
    }
    
    
    
    public PageReference cancel() {
        return new PageReference('/apex/ViewProject?projId=' + projId);
    }

}