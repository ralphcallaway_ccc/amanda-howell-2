global Class CNMSInitialSetup implements InstallHandler {
    global void onInstall(InstallContext context) {
        User u = [select CNMS_Admin__c from User where id = :UserInfo.getUserId()];
        u.CNMS_Admin__c = true;
        update u;
        CNMS_Role__c role = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = 'Team Member');
        insert role;
        
        
    }
   
    @isTest
    static void testInstallScript() {
         CNMSInitialSetup  postinstall = new  CNMSInitialSetup ();
        Test.testInstall(postinstall, null);
  }
}