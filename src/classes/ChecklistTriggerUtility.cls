public Class ChecklistTriggerUtility{
    static List<Checklist__c> newRecords;
    static List<Checklist__c> oldRecords;
    static Map<Id,Checklist__c> oldRecordsMap;
    
    public static void setNewRecords(List<Checklist__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Checklist__c> records, Map<Id,Checklist__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
       
    public static void NotifyTeamMembersOnCheklistCreation(){
        Map<Id,Id> CLIdProjId = new Map<Id,Id>();
        Map<Id,Checklist__c> CLIdCL = new Map<Id,Checklist__c>();
        Map<Id,List<String>> projIdMemberEmails = new Map<Id,List<String>>();
        for(Checklist__c objCL : newRecords){
            CLIdProjId.put(objCL.id,objCL.Project__c);
            CLIdCL.put(objCL.id,objCL);
        }
                
        List<String> emails = new List<String>();
        //Query al the team members 
        System.debug('In After Insert CL TRigger---->');
        for(Our_Team_Member__c objOTM : [select id,User__r.Name,User__r.Email,Project__c,Project__r.Name from Our_Team_Member__c where Project__c in:CLIdProjId.values()]){
            //emails.clear();
            if(projIdMemberEmails.containsKey(objOTM.project__c)){
                emails = projIdMemberEmails.get(objOTM.project__c);
                emails.add(objOTM.User__r.Email);
                projIdMemberEmails.put(objOTM.project__c,emails);               
            }
            else{
               projIdMemberEmails.put(objOTM.project__c,new List<String>{objOTM.User__r.Email}); 
            }               
            System.debug('projIdMemberEmails in between***'+projIdMemberEmails);  
        }   
        System.debug('projIdMemberEmails***'+projIdMemberEmails);       
        List<Messaging.SingleEmailMessage> SingleEmailMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        for(Checklist__c objCL : CLIdCL.values()){
            //Create separate emails for each CLoration
            mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(projIdMemberEmails.get(CLIdProjId.get(objCL.id)));
            mail.setSubject('New checklist Created : ' + objCL.Name);
            mail.setPlainTextBody('New checklist has been created');
            system.debug('mail****'+mail);
            SingleEmailMessages.add(mail);
        }
        //Messaging.sendEmail(SingleEmailMessages);
    }
    
    public static void CheckAccess(){   
        Set<ID> projectIDs = new Set<ID>();
        Map<Id, String> projectAccessMap;
        for(CheckList__c cl: newRecords){
            projectIDs.add(cl.Project__c);
        }
        projectAccessMap = CNMSUtility.getCurrentUserProjectAccessLevel(projectIDs);
        for(CheckList__c cl: newRecords){
            String access = projectAccessMap.get(cl.Project__c);
            if(access !='Edit' && access != 'All'){
                cl.addError(Label.NoActionPrivilegeMessage);
            }
        }
    }

}