public with sharing class CollaborationUtil{
    
    Map<String,Id> TempCLFieldNameCollabIdMap = new Map<String,Id>();
    Map<String,Id> CLFieldNameCollabIdMap = new Map<String,Id>();
    Map<String,Id> TempLFieldNameCollabIdMap = new Map<String,Id>();
    Map<String,Id> LFieldNameCollabIdMap = new Map<String,Id>();
    String[] ChecklistFields = new String[] {'Problems','Our Baggage','Adversary Baggage','Changes To Adversary','Critical Research','Current Mission','Date Prepared','Team Activity','Team Behavior Goals','What Happens Next','What We Want','Overall Mission'};
    String[] LogFields = new String[] {'Discuss','Emotion','Energy','Money','Potential Agenda','Project','Who is','Your Thoughts'};
    
    public Map<String,Id> createChecklistCollaboration(String projId,String clId){
        Collaboration__c objCollab;
        List<Collaboration__c> CLCollaborationToInsert = new List<Collaboration__c>();
        List<String> missingFieldsForCollabration = new List<String>();
        TempCLFieldNameCollabIdMap.clear();
        CLFieldNameCollabIdMap.clear();
        Map<String,Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Collaboration__c.getRecordTypeInfosByName();
        
        //Get list of all existing collabration record for this Checklist
        for(Collaboration__c collab : [select id,Related_Field__c from Collaboration__c where Project__c=:projId and Checklist__c=:clId]){
            //Prepare map of existing fieldName and their collab id
            TempCLFieldNameCollabIdMap.put(collab.Related_Field__c,collab.id);
        }
        
        for(String checklistField : ChecklistFields){
            if(!TempCLFieldNameCollabIdMap.containsKey(checklistField)){
                objCollab = new Collaboration__c(Related_Field__c=checklistField,Project__c=projId,Checklist__c=clId,recordTypeId=recordtypes.get('Checklist Collaboration').getRecordTypeId());
                CLCollaborationToInsert.add(objCollab);
            }
        }
        if(CLCollaborationToInsert!=null && CLCollaborationToInsert.size()>0){
            insert CLCollaborationToInsert;
        }
        
        //Create a fresh map of new collaboration record
        
        
        for(Collaboration__c collab : [select id,Related_Field__c from Collaboration__c where Project__c=:projId and Checklist__c=:clId]){
            //Prepare map of existing fieldName and their collab id
            CLFieldNameCollabIdMap.put(collab.Related_Field__c,collab.id);
        }
        
        return CLFieldNameCollabIdMap;
    }
    
    public Map<String,Id> createLogCollaboration(String projId,String logId){
        Collaboration__c objCollab;
        List<Collaboration__c> LCollaborationToInsert = new List<Collaboration__c>();
        List<String> missingFieldsForCollabration = new List<String>();
        TempLFieldNameCollabIdMap.clear();
        LFieldNameCollabIdMap.clear();
        Map<String,Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Collaboration__c.getRecordTypeInfosByName();
        
        //Get list of all existing collabration record for this Checklist
        for(Collaboration__c collab : [select id,Related_Field__c from Collaboration__c where Project__c=:projId and Log__c=:logId]){
            //Prepare map of existing fieldName and their collab id
            TempLFieldNameCollabIdMap.put(collab.Related_Field__c,collab.id);
        }
        
        for(String logField : LogFields){
            if(!TempLFieldNameCollabIdMap.containsKey(logField)){
                objCollab = new Collaboration__c(Related_Field__c=logField,Project__c=projId,Log__c=logId,recordTypeId=recordtypes.get('Log Collaboration').getRecordTypeId());
                LCollaborationToInsert.add(objCollab);
            }
        }
        if(LCollaborationToInsert!=null && LCollaborationToInsert.size()>0){
            insert LCollaborationToInsert;
        }
        
        //Create a fresh map of new collaboration record
        
        
        for(Collaboration__c collab : [select id,Related_Field__c from Collaboration__c where Project__c=:projId and Log__c=:logId]){
            //Prepare map of existing fieldName and their collab id
            LFieldNameCollabIdMap.put(collab.Related_Field__c,collab.id);
        }
        
        return LFieldNameCollabIdMap;
    }

}