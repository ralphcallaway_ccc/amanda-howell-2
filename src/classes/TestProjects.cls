@isTest
private class TestProjects {

    public static testMethod void testProjectController() {
        Client_Account__c acc = new Client_Account__c(name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        
        Adversary_Account__c inactiveAdvacc = new Adversary_Account__c (name__c='InactiveAdvesary', isActive__c = false);
        insert inactiveAdvacc;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
        
        Project__c InactivePro = new Project__c(name='test',Adversary_Account__c=advacc.id, isActive__c = false);
        insert InactivePro;
        
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;  
        
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r2;
        
        PageReference currentPage = new PageReference('/apex/ProjectList');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);
        Test.setCurrentPage(currentPage );
        ProjectController projectController = new ProjectController();  
        //Test  createNewProject Method
        PageReference testPage = projectController.createNewProject();     
        System.assert(testPage.getUrl().contains('InitiateProject'));    
        
        
        //Test  createNewProjectAll Method
        testPage = projectController.createNewProjectAll();      
        System.assert(testPage.getUrl().contains('InitiateProject'));  
        //Test createNewAdversaryAccount
        testPage = projectController.createNewAdversaryAccount();
        System.assert(testPage.getUrl().contains('CreateAccount'));
        //Test createNewAdversaryAccountAll
        testPage = projectController.createNewAdversaryAccountAll();
        System.assert(testPage.getUrl().contains('CreateAccount')); 
        
         //Test editProject
        testPage = projectController.editProject();
        System.assert(testPage.getUrl().contains('EditProject')); 
        
        //Test editAdversaryAccount
        testPage = projectController.editAdversaryAccount();
        System.assert(testPage.getUrl().contains('EditAccount'));
        
        //Test viewProject
        testPage = projectController.viewProject();
        //System.assert(testPage.getUrl().contains('viewProject')); 
        
        //Test viewAdversaryAccount
        testPage = projectController.viewAdversaryAccount();
        System.assert(testPage.getUrl().contains('ViewAccount')); 
        
        //Test confirmDelete
        testPage = projectController.confirmDelete();
        System.assert(testPage.getUrl().contains('ConfirmProjectDelete'));
        
        //Test confirmAdversaryDelete
        testPage = projectController.confirmAdversaryDelete();
        System.assert(testPage.getUrl().contains('ConfirmAccountDelete'));
        
        //Test deactivateProject, deactivateAccount, activate account
        //first deactivate Adv account.
        projectController.accIdToDeActivate = advacc.Id;
        projectController.deactivateAccount();
        Adversary_Account__c ad1 = [select isActive__c from Adversary_Account__c where id=:advacc.Id];
        System.assert(!ad1.isActive__c);//Acc is Deactivted
        //deactivate proj
        projectController.projIdToDeActivate=pro.id;
        testPage = projectController.deactivateProject();
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR)); // Project cannot be deactivated as the account is inactive.
        //Now activate Client account.
        acc.isActive__c = true;
        update acc;
        //Now activate Adv account.
        projectController.activateAccount();//Should result in exception as no accountidis set for activation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL)); //To cover exception.
        projectController.accIdToActivate = advacc.Id; //Set acc id for activation
        projectController.activateAccount(); // try to activate now,
        ad1 = [select isActive__c from Adversary_Account__c where id=:advacc.Id];
        System.assert(ad1.isActive__c);//Acc is activted
        
        //Try deactivating proj now.
        testPage = projectController.deactivateProject();
        Project__c deActiveProj = [select id,isActive__c from Project__c where id=:pro.id];
        System.Assert(!deActiveProj.isActive__c);
        
        //Test deleteProject
        testPage = projectController.deleteProject();
        List<Project__c> deletedProj = [select id from Project__c where id=:pro.id];
        //as the above project is deleted, hence query should return null.
        System.assert(deletedProj.size()==0);
        System.assert(testPage.getUrl().contains('ProjectList'));
        //Now try to delete the same project again to get an exception.
        projectController.deleteProject();
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
         //Test deleteAccount
        testPage = projectController.deleteAccount();
        List<Adversary_Account__c> deleteAccount= [select id from Adversary_Account__c where id=:advacc.Id];
        //as the above Account is deleted, hence query should return null.
        System.assert(deleteAccount.size()==0);
        System.assert(testPage.getUrl().contains('ProjectList'));
        //Now try to delete the same Account again to get an exception.
        projectController.deleteAccount();
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
        //Test cancel
        testPage = projectController.cancel();
        System.assert(testPage.getUrl().contains('ProjectList')); 
        
        //Test addContact
        testPage = projectController.addContact();
        System.debug(testPage.getUrl());
        System.assert(testPage.getUrl().contains('CreateContact'));
        System.assertEquals('home', testPage.getParameters().get('source'));
        
        //Test addContactRoster
        testPage = projectController.addContactRoster();
        System.assert(testPage.getUrl().contains('CreateContact'));
        System.assertEquals('roster', testPage.getParameters().get('source'));
        
        //Test selectOurTeamMembers
        testPage = projectController.selectOurTeamMembers();
         System.assert(testPage.getUrl().contains('SelectOurTeamMembers'));
       
        
        //Test showInactiveAdversaryAccounts
        testPage = projectController.showInactiveAdversaryAccounts();
        System.assert(testPage.getUrl().contains('AllInactiveAdversaryAccounts'));
        
        //Test showInactiveProjects
        testPage = projectController.showInactiveProjects();
        System.assert(testPage.getUrl().contains('AllInactiveProjects'));
        projectController.projIdToActivate = InactivePro.Id;
        projectController.activateProject();
           
    }
    
     public static testMethod void testNewProjectController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        
        Adversary_Account__c advacc1 = new Adversary_Account__c (name__c='test', isActive__c = true);
        insert advacc1 ;
        
        Adversary_Contact__c con =      new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
        Adversary_Contact__c conTest0 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='test@test.com');
        Adversary_Contact__c conTest1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test3',Last_Name__c='test3',Add_To_Team__c=true, Email__c='test@test.com');        
        Adversary_Contact__c conTest2 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test4',Last_Name__c='test4',Email__c='test@test.com');
        insert con ;
        insert conTest0; 
             
        Project__c pro = new Project__c(name='test',Client_Account__c = acc.id, Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
                 
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = u2.Id, Role_Type__c = Label.teamMember);
        insert r2;  
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r3;  
        
        Checklist__c cl = new Checklist__c (Project__c = pro.id, Checklist_Name__c= 'Test Checklist');
        insert cl;
        
        Log__c log = new Log__c(Project__c = pro.id, Checklist__c = cl.id, Log_Name__c='Test Log');
        insert log;
        
        Our_Team_Member__c  oldTm = new Our_Team_Member__c (User__c = u1.Id, Project__c = pro.id, Project_Lead__c=true, Role__c = 'Team Leader', Access__c = 'View');
        insert oldTm;
        
        Adversary_Team_Member__c oldAtm = new Adversary_Team_Member__c (Adversary_Contact__c = con.Id, Project__c = pro.id, Adversary_Lead__c=true);
        insert oldAtm ;
        PageReference currentPage  = new PageReference('/apex/ViewProject?Id='+pro.Id+ '&clId='+cl.Id +'&logId='+ log.Id + '&accId='+advacc.Id);
        Test.setCurrentPage(currentPage);
		// Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(pro);
 
        // create the controller
         
        NewProjectController newProjectController = new NewProjectController(sc);    
        
        //Test  back Method
        PageReference testPage = newProjectController.back();        
        System.assert(testPage.getUrl().contains('ViewChecklist'));
        
        //Test  cancel Method
        testPage = newProjectController.cancel();        
        System.assert(testPage.getUrl().contains('ViewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('Id'));
        
        //Test  acccancel Method
        testPage = newProjectController.acccancel(); 
        System.assert(testPage.getUrl().contains('InitiateProject'));       
        
        //Test  editProject Method
        testPage = newProjectController.editProject();        
        System.assert(testPage.getUrl().contains('EditProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
        
        //Test  createAccount Method
        testPage = newProjectController.createAccount();      
        System.assert(testPage.getUrl().contains('CreateAccountFromProject'));  
        
        //Test  accsave Method
        newProjectController.account.name__c='advesary1';
        testPage = newProjectController.accsave();
        System.assert(testPage.getUrl().contains('InitiateProject'));
        
        //Test updateProject
        
        testPage = newProjectController.updateProject();//Should result in exception as adv is inactive.
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        //Now first activate Client account.
        newProjectController.project.Client_Account__r.isActive__c = true;      
        //Now activate Adv account.
        newProjectController.project.Adversary_Account__r.isActive__c = true;       
        //call update proj again   
        testPage = newProjectController.updateProject() ; 
        System.assert(testPage.getUrl().contains('ViewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
       
        //Test save
        testPage = newProjectController.save();//Should result in exception as trying to insert existing record.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        //Set contrller project as new project
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController.save();
        System.assert(testPage.getUrl().contains('ViewProject'));
        Id newProjId = testPage.getParameters().get('id');
        List<Project__c> newProj = [select Id from Project__c where id = :newProjId ];
        System.assert(newProj.size()>0);
        
        //Test save n new
        newProjectController.saveAndNew();//Should result in exception as trying to insert existing record.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        //Set controller project as new project
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        newProjectController.saveAndNew();              
        System.assert(newProjectController.project.Adversary_Account__c==null);
        
        //reset Controller
        Test.setCurrentPage(currentPage);
		
		
        // create the controller
         
        NewProjectController newProjectController2 = new NewProjectController(sc);     
         //Test saveAndCreateLog
        newProjectController.save(); 
        testPage = newProjectController2.saveAndCreateLog();  // cover exception
        System.assertEquals(null, testPage);
        newProjectController2.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController2.saveAndCreateLog();  
        System.assert(testPage.getUrl().contains('createLog'));  
        
        //Test saveAndCreateChecklist
        testPage = newProjectController2.saveAndCreateChecklist(); // cover exception
        System.assertEquals(null, testPage);
        newProjectController2.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController2.saveAndCreateChecklist();  
        System.assert(testPage.getUrl().contains('createChecklist')); 
                   
        //Test  advsersaryTeamBuilder Method
        testPage = newProjectController2.advsersaryTeamBuilder();        
        System.assert(testPage.getUrl().contains('AdversaryTeamBuilder'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
        //Test  clientTeamBuilder Method
        testPage = newProjectController2.clientTeamBuilder();        
        System.assert(testPage.getUrl().contains('TeamBuilderTool'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));

        
        //Test  deleteChecklist Method
        testPage = newProjectController2.deleteChecklist();        
        System.assert(testPage.getUrl().contains('viewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
       
        //Test  deleteLog Method
        testPage = newProjectController2.deleteLog();        
        System.assert(testPage.getUrl().contains('viewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
        
        //Test  createChecklist Method
        testPage = newProjectController2.createChecklist();        
        System.assert(testPage.getUrl().contains('createChecklist'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
        //Test  createLog Method
        testPage = newProjectController2.createLog();        
        System.assert(testPage.getUrl().contains('createLog'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
         //Test  updateProjectLead Method
        testPage = newProjectController2.updateProjectLead();        
        System.assert(testPage.getUrl().contains('LeadUpdatedConfirmation'));
        
         //Test  updateAdversaryLead Method
        testPage = newProjectController2.updateAdversaryLead();        
        System.assert(testPage.getUrl().contains('LeadUpdatedConfirmation'));
    
    }    
    
    public static testMethod void testNewProjectControllerPopupMethods(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
              
        Project__c pro = new Project__c(name='test',Client_Account__c = acc.id, Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
         
        Adversary_Contact__c con =      new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
        Adversary_Contact__c conTest0 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='test@test.com');
        insert con ;
        insert conTest0; 
        
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = u2.Id, Role_Type__c = Label.teamMember);
        insert r2;  
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r3;  
        Our_Team_Member__c  oldTm = new Our_Team_Member__c (User__c = u1.Id, Project__c = pro.id, Project_Lead__c=true, Role__c = 'Team Leader', Access__c = 'View');
        insert oldTm;
        
        Adversary_Team_Member__c oldAtm = new Adversary_Team_Member__c (Adversary_Contact__c = con.Id, Project__c = pro.id, Adversary_Lead__c=true);
        insert oldAtm ;
        
        PageReference currentPage  = new PageReference('/apex/ViewProject?id='+pro.Id + '&accId='+advacc.Id);
        Test.setCurrentPage(currentPage);
		// Construct the standard controller
        ApexPages.StandardController sc2 = new ApexPages.StandardController(Pro);
		// create the controller
         
        NewProjectController newProjectController3 = new NewProjectController(sc2);     
        
        //Test  closePopup Method      
        NewProjectController3.selectedUser = UserInfo.getUserId();       
        Our_Team_Member__c  selectedTM = new Our_Team_Member__c (User__c = u2.Id, Project__c = pro.id, Role__c = 'Team Leader', Access__c = 'View');
        insert selectedTM;
        newProjectController3.selectedMember = selectedTM.Id ;
        newProjectController3.closePopup();
        Our_Team_Member__c newSelectedTM= [select id,Project_Lead__c, User__c from Our_Team_Member__c where id=:selectedTM.Id LIMIT 1];        
        System.assert(newSelectedTM.Project_Lead__c);//true
        Our_Team_Member__c oldSelectedTM= [select id,Project_Lead__c from Our_Team_Member__c where id=:oldTM.Id LIMIT 1];        
        System.assert(!oldSelectedTM.Project_Lead__c);//false
        Project__c prj = [select id, Project_Lead__c from Project__c where id = :pro.id];
        //System.assertEquals(selectedTM.Id, prj.Project_Lead__c);
             
        
        //Test  closeAdversaryPopup Method        
        Adversary_Team_Member__c selectedATM = new Adversary_Team_Member__c (Adversary_Contact__c = conTest0.Id, Project__c = pro.id);        
        insert selectedATM;
        newProjectController3.selectedAdversaryMember = selectedATM.Id ;
        newProjectController3.closeAdversaryPopup();        
        Adversary_Team_Member__c newSelectedATM= [select id,Adversary_Lead__c, Adversary_Contact__c  from Adversary_Team_Member__c where id=:selectedATM.Id LIMIT 1];        
        System.assert(newSelectedATM.Adversary_Lead__c);//true
        Project__c prj1 = [select id, Adversary_Lead__c from Project__c where id = :newProjectController3.project.Id];
        System.assertEquals(selectedATM.Id, prj1.Adversary_Lead__c);
        
        //Test  closeSuccess Method
        newProjectController3.closeSuccess();        
        System.assert(!newProjectController3.updateInProgress);
        
        //Test  showPopup Method
        newProjectController3.showPopup();        
        System.assert(!newProjectController3.updateInProgress);
        
        //Test  hidePopup Method
        newProjectController3.hidePopup();        
        System.assert(newProjectController3.updateInProgress);
        
        //Test  getAllMembers Method
        List<SelectOption> options = newProjectController3.getMembersForProjectLead();        
        System.assert(options.size()>0);
        
        //Test  getAllAdversaryMembers Method
        List<SelectOption> options1 = newProjectController3.getAllAdversaryMembers();        
        System.assert(options1.size()>0);
        
        newProjectController3.displayPermissionsPage();
        newProjectController3.updateSharing();
        newProjectController3.getAllUsers();
        
        // test new project creation
        currentPage  = new PageReference('/apex/ViewProject?accId=' +advacc.Id);
        Test.setCurrentPage(currentPage);
        NewProjectController newProjectController4 = new NewProjectController(sc2);  
        
    }
    
   
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}