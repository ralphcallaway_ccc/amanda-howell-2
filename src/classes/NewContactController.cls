public class NewContactController{
    public Adversary_Contact__c contact {get; set;}
    public String id {get; set;}
    public String accId {get; set;}
    public String projId {get; set;}
    private Set<ID> relatedProjIds;
    public String acctype {get; set;}
    public boolean showAddToTeam {get; set;}
    public boolean addToTeam {get; set;}
    public String source{get;set;}
    public Adversary_Account__c account {get;set;}
    public String multiple = '';
    public Boolean hasEditAccess {get;set;}
    public List<Adversary_Contact__c> activeDuplicateContactList {get; set;}
    public List<Adversary_Contact__c> inactiveDuplicateContactList {get; set;}
    public List<Project__c> projectsToDisplay {get; set;}
    public List<Project__c> projectList {get; set;}
    public boolean showAllProjects {get; set;}
    public boolean showProjects {get; set;}
    public Integer projectsCount {get; set;}
    public boolean continueWithNewContactCreation {get;set;}
    public String projIdToDeActivate {get; set;}
    public boolean getActiveDuplicateExist() {
        return activeDuplicateContactList !=null && activeDuplicateContactList.size()>0 ? true:false;
    }
    
    public boolean getInactiveDuplicateExist() {
        return inactiveDuplicateContactList !=null && inactiveDuplicateContactList.size()>0 ? true:false;
    }
    
    public NewContactController(ApexPages.StandardController controller) {
        showAddToTeam = false;
        addToTeam = false;
        continueWithNewContactCreation =false;
        id = ApexPages.currentPage().getParameters().get('id');
        accId = ApexPages.currentPage().getParameters().get('accId');
        projId = ApexPages.currentPage().getParameters().get('projId');
        acctype = ApexPages.currentPage().getParameters().get('acctype');
        source = ApexPages.currentPage().getParameters().get('source');
        hasEditAccess = false;
        if(projId!=null && projId!='')
            showAddToTeam = true;
        contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
        if(accId!=null && accId!=''){
            account = [select Id, name__c from Adversary_Account__c where Id = :accId LIMIT 1];
            List<CNMS_Role__c> currentUserRole = CNMSUtility.getCurrentUserRole();
            if(currentUserRole.size()>0){
                String roleType = currentUserRole[0].Role_Type__c;
                if(roleType ==Label.CEO){
                    hasEditAccess = true;
                }
                else if(roleType ==Label.TeamMember || roleType == Label.Coach){
                    String access = CNMSUtility.getCurrentUserAccountAccessLevel(accId);
                    
                    for(Our_Team_Member__c otm : [select User__c, Access__c from Our_Team_Member__c where Project__r.Adversary_Account__c =:accId]){
                        if(otm.User__c == UserInfo.getUserId()){
                            if(access =='Edit' || access =='All' || otm.Access__c == 'Edit' || otm.Access__c == 'All'){
                                hasEditAccess = true;
                                break;
                            }
                        }
                    }                
                    
                }
            } 
        }    
        if(id!=null && id!=''){
           contact = [select First_Name__c,Last_Name__c,Phone__c,Email__c,Title__c,Assistant_Phone__c,Birthdate__c,Description__c,Do_Not_Call__c,Email_OptOut__c,Fax__c,Lead_Source__c,Home_Phone__c,Mobile_Phone__c,Other_Phone__c,Other_Street__c,Salutation__c,Reports_To__c,Assistant_Name__c,Department__c,Mailing_City__c,Mailing_Country__c,Mailing_State__c,Mailing_Street__c,Mailing_Zip__c,Other_City__c,Other_Country__c,Other_State__c,Other_Zip__c, Name__c ,Adversary_Account__c,Adversary_Account__r.Name__c,isActive__c from Adversary_Contact__c where id=:id];
           //Get all related projects
           relatedProjIds = new Set<ID>();
           for(Adversary_Team_Member__c atm: [select Project__c from Adversary_Team_Member__c where Adversary_Contact__c = :id]){
               relatedProjIds.add(atm.Project__c);
           }
           projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c from Project__c where isActive__c=true and ID IN :relatedProjIds]; //Adversary_Account__c=:contact.Adversary_Account__c];
           if(projectList!=null && projectList.size()>0)
               showProjects = true; 
        }
    }    
    
    public PageReference back(){
        PageReference redirectPage;
        if(source=='teambuilder'){
            redirectPage = new PageReference('/apex/AdversaryTeamBuilder?accId='+accId + '&projId=' +projId);
        }
        else if(accId!=null)
            redirectPage = new PageReference('/apex/viewAccount?Id='+accId);
        else if(projId!=null)
            redirectPage = new PageReference('/apex/viewProject?projId='+projId);    
        return redirectPage;
    }
    
    public PageReference cancel(){            
        PageReference redirectPage = new PageReference('/apex/ViewContact?id='+id+'&accId='+accId);
        if(source=='teambuilder'){
        redirectPage = new PageReference('/apex/AdversaryTeamBuilder?accId='+accId + '&projId=' +projId);
        }
        return redirectPage ; 
    }
    
    public PageReference save(){
        try{
            /*if(!continueWithNewContactCreation && checkDuplicates()){
                multiple = '0';
                PageReference redirectPage = new PageReference('/apex/ContactValidation');
                return redirectPage;
            }*/
            if(checkDuplicates()){
                multiple = '0';  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contact already exist with this name.'));              
                return null;
            }
            if(contact.Add_To_Team__c){
                addToTeam = true;
                contact.Add_To_Team__c = false;
            }
            insert contact;
            
            //Add this contact to Adversary Team
            if(addToTeam && projId!=null && projId!='' && source=='teambuilder'){
                PageReference redirectPage; 
                Adversary_Team_Member__c newMember = new Adversary_Team_Member__c(Adversary_Contact__c=contact.id,project__c=projId);
                insert newMember;
                redirectPage = new PageReference('/apex/AdversaryTeamBuilder?accId='+accId+'&projId='+projId);
                return redirectPage;   
            }
            PageReference redirectPage = new PageReference('/apex/ViewContact?id='+contact.Id+'&accId='+accId);
            if(source=='teambuilder'){
                redirectPage = new PageReference('/apex/AdversaryTeamBuilder?accId='+accId+'&projId='+projId);
            }
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public boolean checkDuplicates(){
        try{
            activeDuplicateContactList = [select id, Name__c, isActive__c, Phone__c, Email__c from Adversary_Contact__c where Adversary_Account__c = :accId AND First_Name__c= :contact.First_Name__c AND Last_Name__c= :contact.Last_Name__c AND isActive__c=true];
            inactiveDuplicateContactList = [select id, Name__c, isActive__c, Phone__c, Email__c from Adversary_Contact__c where Adversary_Account__c = :accId AND First_Name__c= :Contact.First_Name__c AND Last_Name__c= :contact.Last_Name__c AND isActive__c = false];
            
            if((activeDuplicateContactList != null && activeDuplicateContactList.size()>0) ||
               (inactiveDuplicateContactList != null && inactiveDuplicateContactList.size()>0)){
            return true;
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        return false;
    }
    
    public PageReference updateContact(){
        List<Adversary_Contact__c> dupes = [select id, Name__c, isActive__c, Phone__c, Email__c from Adversary_Contact__c where Adversary_Account__c = :accId AND First_Name__c= :contact.First_Name__c AND Last_Name__c= :contact.Last_Name__c AND id<>:contact.Id];
        if(dupes.size()>0){
                multiple = '0';  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contact already exist with this name.'));              
                return null;
        }
        try{
            update contact;
            PageReference home = new PageReference('/apex/ViewContact?id='+id+'&accId='+accId);
            return home;
        } 
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    public PageReference saveAndNew(){
        try{
            /*if(!continueWithNewContactCreation && checkDuplicates()){
                multiple = '1';
                PageReference redirectPage = new PageReference('/apex/ContactValidation');
                return redirectPage;
            }*/
            if(checkDuplicates()){
                multiple = '1';  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Contact already exist with this name.'));              
                return null;
            }
            
            insert contact;
            if(contact.Add_To_Team__c){
                addToTeam = true;
                contact.Add_To_Team__c = false;
            }
            if(addToTeam && projId!=null && projId!=''){
                Adversary_Team_Member__c newMember = new Adversary_Team_Member__c(Adversary_Contact__c=contact.id,project__c=projId);
                    insert newMember;
            }
            contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
            addToTeam = false;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }    
        return null;
    }
    
    public PageReference editContact(){
        id = ApexPages.currentPage().getParameters().get('id');
        PageReference redirectPage = new PageReference('/apex/EditContact?id='+id);
        source = 'cont';
        return redirectPage;
    }
    
    public pageReference continueSave(){
        try{
            continueWithNewContactCreation = true;
            if(multiple =='0')        
                return save();
                
            else{
               saveAndNew();
               continueWithNewContactCreation = false;
               multiple = '';
               return new PageReference('/apex/CreateContact');
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        
    }
    
    public pageReference activateContacts(){
        boolean noneSelected = true;
        for(Adversary_Contact__c a : inactiveDuplicateContactList ){
            if(a.isActive__c){
                noneSelected = false;
                break;
            }            
        }
        if(noneSelected ){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.NoContactSelectedToActivate));
            return null;
        }
        try{
            update inactiveDuplicateContactList ;
           }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        PageReference redirectPage;
        if(multiple =='0')        
            redirectPage = new PageReference('/apex/ProjectList');
            
        else{
            contact = new Adversary_Contact__c(Adversary_Account__c=accId,isActive__c=true);
            multiple = '';
            continueWithNewContactCreation = false;
            redirectPage = new PageReference('/apex/CreateContact');
        }
        
        return redirectPage;
    }
    
    public PageReference deactivateProject() {
        try{
            String projId = projIdToDeActivate;// ApexPages.currentPage().getParameters().get('projId');
            Project__c projToDeactivate = [select id, name, isActive__c, Client_Account__c,Client_Account__r.isActive__c,Adversary_Account__c, Adversary_Account__r.isActive__c from Project__c where id=:projId];
            
             if(projToDeactivate.Client_Account__c !=null && !projToDeactivate.Client_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ClientAccountInactiveError));
                return null;
            } 
            if(projToDeactivate.Adversary_Account__c !=null && !projToDeactivate.Adversary_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AdversaryAccountInactiveError));
                return null;
            } 
            projToDeactivate.isActive__c = false;
            update projToDeactivate;
            projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c from Project__c where isActive__c=true and ID IN :relatedProjIds]; //Adversary_Account__c=:contact.Adversary_Account__c];
            if(projectList.size()==0){
                showProjects = false;
            }
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, projToDeactivate.name + ' ' + Label.ProjectDeactivationMessage));
            //PageReference projList = new PageReference('/apex/ProjectList');
            //projList.setRedirect(true);
            return null;//projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
}