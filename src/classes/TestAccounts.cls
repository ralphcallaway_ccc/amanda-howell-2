@isTest
private class TestAccounts {

     public static testMethod void testNewAccountController() {
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        Adversary_Account__c advacc1 = new Adversary_Account__c (name__c='advesary1', isActive__c = true);
        insert advacc1;
        
        Adversary_Contact__c con = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
        insert con;
        Adversary_Contact__c con1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test1',Last_Name__c='test');
        insert con1;

        update con1;
        
        
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
        
        Adversary_Account__c account1 = new Adversary_Account__c (name__c='advesary');
        
        Adversary_Account__c account2 = new Adversary_Account__c (name__c='test');
        CNMS_Role__c role = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert role;
        PageReference currentPage = new PageReference('/apex/ViewAccount');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);
        currentPage.getParameters().put('contId',con.id);
        Test.setCurrentPage(currentPage );
        
        NewAccountController newAccountController = new NewAccountController();
         
         //Test  back Method
        PageReference testPage = newAccountController.back();
        System.assert(testPage.getUrl().contains('ViewProject'));                
        
        //Test  cancel Method
        testPage = newAccountController.cancel();        
        System.assert(testPage.getUrl().contains('ViewAccount'));
        System.assertEquals(advacc.id, testPage.getParameters().get('accId'));
        
        //Test  editAccount Method
        testPage = newAccountController.editAccount();        
        System.assert(testPage.getUrl().contains('EditAccount'));
        System.assertEquals(advacc.id, testPage.getParameters().get('accId'));
         
        //Test  editContact Method
        testPage = newAccountController.editContact();        
        System.assert(testPage.getUrl().contains('EditContact'));
        System.assertEquals(con.id, testPage.getParameters().get('contId'));
        
        //Test  viewContact Method
        testPage = newAccountController.viewContact();        
        System.assert(testPage.getUrl().contains('ViewContact'));
        System.assertEquals(con.id, testPage.getParameters().get('contId'));
        
        //Test  save and new Method               
        newAccountController.account = account1; //assigning a new account for save.
        testPage = newAccountController.saveAndNew();   
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));//Error returned as duplicate account exist.
        newAccountController.account.name__c = 'TestAdvesary2';
        testPage  = newAccountController.saveAndNew(); 
       // testPage  = newAccountController.continueSave(); // tell to save even if duplicate exists.
        System.assert([select id from Adversary_Account__c where name__c = 'TestAdvesary2'].size()>0);              
        
        //Test  save Method
        newAccountController.account.name__c = 'advesary';                       
        testPage = newAccountController.save(); 
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));//Error returned as duplicate account.
        newAccountController.account.name__c = 'TestAdvesary23';   
        testPage = newAccountController.save(); 
        System.assert([select id from Adversary_Account__c where name__c = 'TestAdvesary23'].size()>0);        
        
        //Test Update Account
        newAccountController.account = advacc;
        String tempName = 'Test#Test12';
        newAccountController.account.Name__c = tempName ;
        testPage = newAccountController.updateAccount();
        System.assert(testPage.getUrl().contains('ViewAccount'));
        List<Adversary_Account__c> accs = [select id from Adversary_Account__c where Name__c = :tempName]; 
        System.assert(accs.size()>0);
        
        //Test  addContact Method
        testPage = newAccountController.addContact();        
        System.assert(testPage.getUrl().contains('CreateContact'));
        System.assertEquals(advacc.id, testPage.getParameters().get('accId'));
        
        //Test  confirmContactDelete Method
        testPage = newAccountController.confirmContactDelete();        
        System.assert(testPage.getUrl().contains('ConfirmContactDelete'));

        //Test deactivateContact         
        testPage = newAccountController.deactivateContact(); // result an error as no client is set for deactivation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));  
        newAccountController.contIdToDeActivate = con.Id; //set id for deactivation
        testPage = newAccountController.deactivateContact(); //re-try     
        Adversary_Contact__c advC = [select id, isActive__c from Adversary_Contact__c where Id = :con.Id LIMIT 1]; 
        System.assert(!advC.isActive__c); //test if it is deactivated
        System.assert(ApexPages.hasMessages(ApexPages.severity.INFO));
        
        /*//Test deactivateProject         
        testPage = newAccountController.deactivateProject(); // result an error as no project is set for deactivation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));  
        newAccountController.projIdToDeActivate= pro.Id; //set id for deactivation
        testPage = newAccountController.deactivateProject(); //re-try     
        Project__c pr1= [select isActive__c from Project__c where Id = :pro.Id LIMIT 1]; 
        System.assert(!pr1.isActive__c); //test if it is deactivated
        System.assert(ApexPages.hasMessages(ApexPages.severity.INFO));*/
        
        //Test activateContact         
        testPage = newAccountController.activateContact(); // result an error as no client is set for activation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));  
        newAccountController.contIdToActivate = con.Id; //set id for activation
        testPage = newAccountController.activateContact(); //re-try     
        advC = [select id, isActive__c from Adversary_Contact__c where Id = :con.Id LIMIT 1]; 
        System.assert(advC.isActive__c); // test if it is activated
        System.assert(ApexPages.hasMessages(ApexPages.severity.INFO));
    
        delete con1;
    }
    
     public static testMethod void testClientAccountController() {
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Project__c pro = new Project__c(name='test',Client_Account__c=acc.id, isActive__c = true);
        insert pro;
        
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        u1.isActive = true;
        u1.Client_Account__c = acc.Id;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        u2.isActive = true;
        u2.Client_Account__c = acc.Id;
        insert u2;
        
        PageReference currentPage = new PageReference('/apex/ViewAccount');
        currentPage.getParameters().put('accId',acc.id);
        currentPage.getParameters().put('projId',pro.id);
        Test.setCurrentPage(currentPage );
        
        ClientAccountController clientAccountController = new ClientAccountController();
        //Check that user's list is initialized.
        System.assert(clientAccountController.users.size()>0);
        //Test  cancel Method
        PageReference  testPage = clientAccountController.cancel();        
        System.assert(testPage.getUrl().contains('ViewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('projId'));
        
    }
   
    private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}