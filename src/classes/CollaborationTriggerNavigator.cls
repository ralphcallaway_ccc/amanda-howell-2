public Class CollaborationTriggerNavigator{
    List<Collaboration__c> newRecords;
    List<Collaboration__c> oldRecords;
    //Constructor
    public CollaborationTriggerNavigator(List<Collaboration__c> TriggerNewRecords,  List<Collaboration__c> TriggerOldRecords, Map<Id,Collaboration__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        CollaborationTriggerUtility.setNewRecords(newRecords);
        CollaborationTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void ShareCollaborationWithOTMs(){
        CollaborationTriggerUtility.ShareCollaborationWithOTMs();
    }
    
    public void AutoFollowChatter(){
        CollaborationTriggerUtility.AutoFollowChatter();
    }
    
}