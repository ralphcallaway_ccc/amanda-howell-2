@isTest
private class TestAdversaryTeamBuilder {
     public static testMethod void testadversaryTeamBuilder(){  
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id);
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;
        
        List<Adversary_Contact__c> contacts = new List<Adversary_Contact__c>();
        List<Adversary_Team_Member__c>  atms = new List<Adversary_Team_Member__c>();
        // Create total 5 contacts
        for(Integer i = 0; i<5; i++){
            Adversary_Contact__c ac = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true);
            ac.First_Name__c = 'test'+ i;
            ac.Last_Name__c = 'test' + i;
            contacts.add(ac);
            
        } 
        insert contacts;
        //Create total 3 already selected ATMs. Adversary_Contact__c 0,1 and 2.
        for(Integer i = 0; i<3; i++){
            Adversary_Team_Member__c atm = new Adversary_Team_Member__c (Adversary_Contact__c =contacts[i].Id, Project__c = pro.id);            
            atms.add(atm);            
        }         
        insert atms ;
        // Note that Adversary_Contact__c 3 and 4 are not Team members, hence should be in unselected list.
        PageReference currentPage = new PageReference('/apex/adversaryTeamBuilder');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);        
        Test.setCurrentPage(currentPage );
        AdversaryTeamBuilder adversaryTeamBuilder = new AdversaryTeamBuilder();
        
        //Verify data is prepared as per expectation.       
        System.assert(adversaryTeamBuilder.getSelectedValues().size()== atms.size()); // Check selected values.
        System.assert(adversaryTeamBuilder.getunSelectedValues().size()== contacts.size() - atms.size()); // Check unselected values.
        
        
        // Test Select Click. 
        adversaryTeamBuilder.leftselected.add(contacts[3].First_Name__c + ' ' + contacts[3].Last_Name__c);              
        adversaryTeamBuilder.selectclick();
        //Verify the item is added in the right list and removed from left list.       
        System.assertEquals(adversaryTeamBuilder.getSelectedValues().size(),atms.size()+1);
        System.assertEquals(adversaryTeamBuilder.getunSelectedValues().size(),contacts.size() - atms.size() -1 );
        
        // Test UnSelect Click. 
        adversaryTeamBuilder.RightSelected.add(contacts[0].First_Name__c + ' ' + contacts[0].Last_Name__c);              
        adversaryTeamBuilder.unselectclick();
        //Verify the item is added in the right list and removed from left list.
        System.assertEquals(adversaryTeamBuilder.getSelectedValues().size(),atms.size());
        System.assertEquals(adversaryTeamBuilder.getunSelectedValues().size(),contacts.size() - atms.size());      
        
        //Test Done              
        PageReference testPage = adversaryTeamBuilder.done();
        List<Adversary_Team_Member__c> newatms = [select id from Adversary_Team_Member__c where Adversary_Contact__c =: contacts[3].Id];
        List<Adversary_Team_Member__c> deletedatms = [select id from Adversary_Team_Member__c where Adversary_Contact__c =: contacts[0].Id];
        System.assert(testPage.getUrl().contains('viewProject')); 
        System.assert(newatms.size()>0);
        System.assert(deletedatms.size()==0);
         
        //Test  cancel Method
        testPage = adversaryTeamBuilder.addContact();        
        System.assert(testPage.getUrl().contains('createContactFromATMBuilder'));
        System.assertEquals(advacc.id, testPage.getParameters().get('accId'));
        
    
    }
    
    public static testMethod void testadversaryTeamBuilderConactAPIs(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        
        Adversary_Contact__c con = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test', Email__c='test@test.com');
        insert con;        
        Adversary_Contact__c con1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test11',Last_Name__c='test', Email__c='test@test.com');
        insert con1;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id,isActive__c =true );
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;

        Adversary_Contact__c conTest2 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test4',Last_Name__c='test4',Email__c='test@test.com');
        Adversary_Contact__c conTest3 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='differenttest@test.com');
        
        List<Adversary_Contact__c> contacts = new List<Adversary_Contact__c>();
        List<Adversary_Team_Member__c>  atms = new List<Adversary_Team_Member__c>();
        // Create total 5 contacts
        for(Integer i = 0; i<5; i++){
            Adversary_Contact__c ac = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true);
            ac.First_Name__c = 'test'+ i;
            ac.Last_Name__c = 'test' + i;
            contacts.add(ac);
            
        } 
        insert contacts;
        //update contacts;
        
        //Create total 3 already selected ATMs. Adversary_Contact__c 0,1 and 2.
        for(Integer i = 0; i<3; i++){
            Adversary_Team_Member__c atm = new Adversary_Team_Member__c (Adversary_Contact__c =contacts[i].Id, Project__c = pro.id);            
            atms.add(atm);            
        }         
        insert atms ;
        
        PageReference currentPage = new PageReference('/apex/createContactFromATMBuilder');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);
        Test.setCurrentPage(currentPage );
        
        AdversaryTeamBuilder adversaryTeamBuilder = new AdversaryTeamBuilder();
        adversaryTeamBuilder.initNewContact();
        //Test  save and new Method               
        adversaryTeamBuilder.contact = conTest2 ; //assigning a new Contact for save.
        adversaryTeamBuilder.contact.Add_To_Team__c= true;     
        PageReference  testPage = adversaryTeamBuilder.saveAndNew(); // set duplicateExist to true as duplicate contacts exists.   
        //System.assert(adversaryTeamBuilder.duplicateExist);
        testPage = adversaryTeamBuilder.activateContacts();//Call to activate Contacts.
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));//Error returned as none of the record is selected for activation.
        if(adversaryTeamBuilder.getInactiveDuplicateExist()){
            adversaryTeamBuilder.inactiveDuplicateContactList[0].isActive__c = true;// Select first record       
            testPage = adversaryTeamBuilder.activateContacts(); // call the activate contact again.           
        }
        adversaryTeamBuilder.contact.First_Name__c='test21';
        adversaryTeamBuilder.contact.Last_Name__c='test21';
        adversaryTeamBuilder.contact.Email__c='test@test.com';   
        adversaryTeamBuilder.contact.Add_To_Team__c= true;     
        testPage  = adversaryTeamBuilder.saveAndNew(); //Try saving again, save is not allowed as duplicate exist.
        adversaryTeamBuilder.continueWithNewContactCreation = true;
        testPage  = adversaryTeamBuilder.continueSave(); // tell to save even if duplicate exists.
        
        
        //Test  save Method
        adversaryTeamBuilder.contact.First_Name__c='test22';
        adversaryTeamBuilder.contact.Last_Name__c='test22';
        adversaryTeamBuilder.contact.Email__c='test@test.com';     
        adversaryTeamBuilder.contact.Add_To_Team__c=true;       
        testPage = adversaryTeamBuilder.save(); // Should return ContactValidation page, as duplicate records exists.       
        //System.assert(adversaryTeamBuilder.duplicateExist);
        //System.assert(adversaryTeamBuilder.getActiveDuplicateExist());
        testPage  = adversaryTeamBuilder.continueSave(); // tell to save even if duplicate exists.
        //System.assert(testPage.getUrl().contains('AdversaryTeamBuilder'));       
        testPage = adversaryTeamBuilder.save(); // to cover exception as trying to reinsert the same contact.
        //System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
        adversaryTeamBuilder.cancel();     
    }

}