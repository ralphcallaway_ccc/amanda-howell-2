public Class LogTriggerNavigator{
    List<Log__c> newRecords;
    List<Log__c> oldRecords;
    //Constructor
    public LogTriggerNavigator(List<Log__c> TriggerNewRecords,  List<Log__c> TriggerOldRecords, Map<Id,Log__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        LogTriggerUtility.setNewRecords(newRecords);
        LogTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void NotifyTeamMembersOnLogCreation(){
        LogTriggerUtility.NotifyTeamMembersOnLogCreation();
    }
    
    public void CheckAccess(){
        LogTriggerUtility.CheckAccess();
    }
    
}