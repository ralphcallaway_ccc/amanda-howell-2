public Class OTMTriggerNavigator{
    List<Our_Team_Member__c> newRecords;
    List<Our_Team_Member__c> oldRecords;
    //Constructor
    public OTMTriggerNavigator(List<Our_Team_Member__c> TriggerNewRecords,  List<Our_Team_Member__c> TriggerOldRecords){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        OTMTriggerUtility.setNewRecords(newRecords);
        OTMTriggerUtility.setOldRecords(oldRecords);        
    }
    
    public void PopulateProjectLead (){
        OTMTriggerUtility.PopulateProjectLead();
    }
    
    public void AssignChatterPermissions(){
        OTMTriggerUtility.AssignChatterPermissions();
    }
    
    public void DeleteChatterPermissions(){
        OTMTriggerUtility.DeleteChatterPermissions();
    }
    
    public void updateSharingAccess(){
        OTMTriggerUtility.updateSharingAccess();
    }
    
    public void deleteSharingAccess(){
        OTMTriggerUtility.deleteSharingAccess();
    }
}