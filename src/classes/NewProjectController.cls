public class NewProjectController{
    public boolean displayPopup {get; set;}
    public boolean displaySuccess {get; set;}
    public Project__c project {get; set;}
    public String id {get; set;}
    public String accId {get; set;}
    public String clId {get; set;}
    public String logId {get; set;}
    public String source {get; set;}
    public String prevSource {get; set;}
    public boolean showMembers {get; set;}
    public List<Our_Team_Member__c> clientMembers {get; set;}
    public List<Our_Team_Member__c> clientMembersToDisplay {get; set;}
    public List<Our_Team_Member__c> clientMembersForUpdatePermission {get; set;}
    public List<Our_Team_Member__c> clientMembersForProjectLead {get; set;}
    public String[] rolesForUpdatePermission = new String[]{Label.Observer, Label.TeamMember};
    public String[] rolesForProjectLead = new String[]{Label.TeamMember, Label.CEO};
    public List<Adversary_Team_Member__c> adversaryMembers {get; set;}
    public List<Adversary_Team_Member__c> adversaryMembersToDisplay {get; set;}
    public List<Checklist__c> checklists {get; set;} 
    public List<Checklist__c> checklistsToDisplay {get; set;} 
    public List<Log__c> logs {get; set;} 
    public List<Log__c> logsToDisplay {get; set;} 
    public Adversary_Account__c account {get; set;}
    public String selectedMember {get; set;}
    public String oldSelectedMember='';
    public String selectedAdversaryMember {get; set;}
    public String oldSelectedAdversaryMember='';
    public boolean updateInProgress {get; set;}
    public boolean buttonClicked {get; set;}
    public boolean showTM {get; set;}
    public boolean showATM {get; set;}
    public boolean showCL {get; set;}
    public boolean showL {get; set;}
    public boolean showAllLinkTM {get; set;}
    public boolean showAllLinkATM {get; set;}
    public boolean showAllLinkCL {get; set;}
    public boolean showAllLinkL {get; set;}
    public Integer TMCount {get; set;}
    public Integer TMCountForUpdatePermission {get; set;}
    public Integer TMCountForProjectLead {get; set;}
    public Integer ATMCount {get; set;}
    public Integer CLCount {get; set;}
    public Integer LCount {get; set;}
    public Integer numberOfRecords;
    public Client_Account__c clientAcc {get;set;}
    public Boolean projectCreationAllowed {get;set;}
    public Boolean hasEditAccess {get;set;}
    public Boolean hasViewAccess {get;set;}
    public String AccessLevel {get;set;}
    public String selectedUser {get; set;}
    private Id projectOwnerId {get;set;}
    public Boolean NotifyTM {get;set;}
    public NewProjectController(ApexPages.StandardController controller) {       
        init();
        //If the project is already present
        if(ID!=null && ID!=''){
            List<Project__c> projList = [select Name,isActive__c,Client_Account__r.Name__c, OwnerId, Client_Account__c, Account__c, Opportunity__c, AP_Value__c, Closed_Reason__c, Last_Activity_Date__c, Project_Status__c, Start_Date__c, Client_Account__r.isActive__c, Client_Account__r.Id,Adversary_Account__c, Adversary_Account__r.isActive__c, Adversary_Account__r.Name__c,Adversary_Account__r.Id,Description__c,Mission_and_Purpose__c,Project_Lead__c, Adversary_Lead__c from Project__c where id=:id];       
            if(projList.size()==0){
                hasViewAccess = false;
                return;
            }
            project = projList[0];
            projectOwnerId  = project.OwnerId;
            showMembers = true;
            populateOTMData();
            
            adversaryMembers  = [select Adversary_Contact__r.Name__c,Member_Name__c,Adversary_Lead__c,Adversary_Contact__c from Adversary_Team_Member__c where Project__c=:id order by Adversary_Contact__r.Name__c];
            checklists = [select id,name,Checklist_Name__c, Date_Prepared__c from Checklist__c where project__c=:id order by Date_Prepared__c DESC];
            logs = [select id,Date_Created__c,name,Log_Name__c, Checklist__r.Name,Checklist__r.Checklist_Name__c, Checklist__r.id from Log__c where project__c=:id order by Date_Created__c DESC];            
            //Contraints log to be related to Checklist if checklist id is present.
            if(clID!=null){
                //logs = [select id,Date_Created__c,Log_Name__c, name from Log__c where CheckList__c=:clId];
            }
            populateTMListToBeDisplayed(parseInteger(Label.TeamMembersToBeDisplayed));
            populateATMListToBeDisplayed(parseInteger(Label.AdversaryTeamMembersToBeDisplayed));
            populateCheckListToBeDisplayed(parseInteger(Label.ChecklistToBeDisplayed));
            populateLogListToBeDisplayed(parseInteger(Label.LogsToBeDisplayed));
            System.debug('ADV to display MEmbers---->' + adversaryMembersToDisplay );
            AccessLevel  = CNMSUtility.getCurrentUserProjectAccessLevel(id);
            if(AccessLevel =='Edit' || AccessLevel =='All'){
                for(Our_Team_Member__c otm : clientMembers ){
                    if(otm.User__c == UserInfo.getUserId()){
                        hasEditAccess = true;
                        hasViewAccess = true;
                        break;
                    }
                }                
            }
            if(AccessLevel =='Read' ){
                hasViewAccess = true;
            }
         
            //Current Project Lead
            try{
                selectedMember = [select id from Our_Team_Member__c where Project__c=:id and Project_Lead__c=true limit 1].id;
                oldSelectedMember = selectedMember;
            }
            catch(Exception e){}
            
            try{
                selectedAdversaryMember = [select id from Adversary_Team_Member__c  where Project__c=:id and Adversary_Lead__c=true limit 1].id;
                oldselectedAdversaryMember = selectedAdversaryMember;
            }
            catch(Exception e){
            
            }
            // Ensure account is active, else give error.
            if(!project.Adversary_Account__r.isActive__c){
                hasEditAccess = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, Label.AdversaryAccountInactiveError)); 
            }    
        }
        else{
            //It is a new project being created. Intialize the values           
            List<CNMS_Role__c> currentUserRole = CNMSUtility.getCurrentUserRole();
            if(currentUserRole.size()>0 && currentUserRole[0].Client_Account__c !=null && currentUserRole[0].Role_Type__c != Label.Coach){
                clientAcc  = [select Id, Name__c from Client_Account__c where  id = :currentUserRole[0].Client_Account__c ];               
                project.Client_Account__c = currentUserRole[0].Client_Account__c;  
                project.isActive__c = true;
            }
            else{
                project = null;
                projectCreationAllowed = false;                
            }
        }
           
    }    
    /*
    * This method initialize the class variables.*
    */
    private void init(){
        projectCreationAllowed = true;
        hasEditAccess = false;
        hasViewAccess = false;
        numberOfRecords = 10;
        LCount = 0;
        CLCount = 0;
        NotifyTM  = false;
        showMembers = false;
        showTM = false;
        showATM = false;
        showCL = false;
        showL = false;
        showAllLinkTM = false;
        showAllLinkATM  = false;
        showAllLinkCL = false;
        showAllLinkL= false;
        
        project = new Project__c();
        account = new Adversary_Account__c(isActive__c=true);
        id = ApexPages.currentPage().getParameters().get('id');
        accId = ApexPages.currentPage().getParameters().get('accId');
        clId = ApexPages.currentPage().getParameters().get('clId');
        logId = ApexPages.currentPage().getParameters().get('logId');
        source = ApexPages.currentPage().getParameters().get('source');
        prevSource = ApexPages.currentPage().getParameters().get('prevSource');
    }
    /*
    This method converts an stirng to the equivalant integer. returns default 10, In case of exception.
    */
    private Integer parseInteger(String stringNumber){
        Integer IntNumber;
        try{
            IntNumber = Integer.valueOf(stringNumber);
        
        }
        catch(Exception e){
            IntNumber = 10;
        }
        return IntNumber;
    }
    
    private void populateOTMData(){
        clientMembers = [select User__r.Name,Project_Lead__c,User__c, Role__c, Access__c from Our_Team_Member__c where Project__c=:id order by Display_Order__c,User__r.Name];
        clientMembersForUpdatePermission = [select User__r.Name,Project_Lead__c,User__c, Role__c, Access__c from Our_Team_Member__c where Project__c=:id AND Role__c IN :rolesForUpdatePermission AND User__c<>:projectOwnerId order by Display_Order__c,User__r.Name];
        clientMembersForProjectLead =  [select User__r.Name,Project_Lead__c,User__c, Role__c, Access__c from Our_Team_Member__c where Project__c=:id AND Role__c IN :rolesForProjectLead AND User__c<>:projectOwnerId order by Display_Order__c,User__r.Name];
        TMCountForUpdatePermission  = clientMembersForUpdatePermission.size();
        TMCountForProjectLead  = clientMembersForProjectLead.size();
    }
    /*
    * This method populates the Team members list which is to be displayed on the  page.
    */
    private void populateTMListToBeDisplayed(Integer numberOfTMRecords){
        
        //Populate team members to be displayed
        if(clientMembers!=null && clientMembers.size()>0){
                showTM = true;
                TMCount = clientMembers.size();
                if(TMCount<=numberOfTMRecords)
                        numberOfTMRecords = TMCount;
                else
                    showAllLinkTM = true;
                clientMembersToDisplay = new List<Our_Team_Member__c>();
                for(Integer i=0;i<numberOfTMRecords;i++){
                    clientMembersToDisplay.add(clientMembers[i]);
                }
            }
    }
    
    /*
    * This method populates the Adversary Team members list which is to be displayed on the  page.
    */
    private void populateATMListToBeDisplayed(Integer numberOfATMRecords){
            //Populate adversary team members to be displayed
            if(adversaryMembers!=null && adversaryMembers.size()>0){
                showATM = true;
                ATMCount = adversaryMembers.size();
                if(ATMCount<=numberOfATMRecords)
                        numberOfATMRecords = ATMCount;
                else
                    showAllLinkATM = true;
                adversaryMembersToDisplay = new List<Adversary_Team_Member__c>();
                for(Integer i=0;i<numberOfATMRecords;i++){
                    adversaryMembersToDisplay.add(adversaryMembers[i]);
                }
            }
    }

    /*
    * This method populates the Checklists list which is to be displayed on the  page.
    */
    private void populateCheckListToBeDisplayed(Integer numberOfCLRecords){ 
            //Populate checklists to be displayed
            if(checklists!=null && checklists.size()>0){
                showCL = true;
                CLCount = checklists.size();
                if(CLCount<=numberOfCLRecords)
                        numberOfCLRecords = CLCount;
                else
                    showAllLinkCL = true;
                checklistsToDisplay = new List<Checklist__c>();
                for(Integer i=0;i<numberOfCLRecords;i++){
                    checklistsToDisplay.add(checklists[i]);
                }
            }
    }   

    /*
    * This method populates the Logs list which is to be displayed on the  page.
    */
    private void populateLogListToBeDisplayed(Integer numberOfLRecords){        
            //Populate logs to be displayed
            if(logs!=null && logs.size()>0){
                showL = true;
                LCount = logs.size();
                if(LCount<=numberOfLRecords)
                        numberOfLRecords = LCount;
                else
                    showAllLinkL = true;
                logsToDisplay = new List<Log__c>();
                for(Integer i=0;i<numberOfLRecords;i++){
                    logsToDisplay.add(logs[i]);
                }
            }
    }
    
    /*
    * This method redirects user to the previous page, from where this page is invoked.
    * The previous page is calculated based on the source paramter.
    */
    public PageReference back(){
    
        PageReference redirectPage = new PageReference('/apex/ProjectList?sfdc.tabName=01rU0000000DWrs');
        if(clID!=null){
            redirectPage = new PageReference('/apex/ViewChecklist?clId=' + clId + '&id=' + id);
        }
        
        return redirectPage;
    }
    
    public PageReference cancel(){
        PageReference redirectPage = new PageReference('/apex/ViewProject?id='+id);
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    
    /*
    * This method redirects user to the previous page.
    */
    public PageReference acccancel(){
        PageReference redirectPage = new PageReference('/apex/InitiateProject');
        return redirectPage;
    }
    
    /*
    * This method redirects user to the EditProject Page. The id of the project being edited, is passed as parameter.
    */
    public PageReference editProject(){
        PageReference redirectPage = new PageReference('/apex/EditProject?source=project&id='+id);
        return redirectPage;
    }
    /*
    * This method redirects user to the create Account page.
    */
    public PageReference createAccount(){
        PageReference redirectPage = new PageReference('/apex/CreateAccountFromProject');
        return redirectPage;
    }
    
    /*
    * This method  Inserts the record into database and redirect user to this records detail page.
    */
    public PageReference save(){
        if(!validateProjectData()){
            return null;
        }
        try{
            insert project;
            createCoachGumboTeamMembers();
            source = ApexPages.currentPage().getParameters().get('source');
            
            PageReference redirectPage = new PageReference('/apex/ViewProject?id='+project.id); 
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    private void createCoachGumboTeamMembers(){
        List<CNMS_Role__c> roles = [select Id, Role_Type__c, User__c from CNMS_Role__c];
        List<Our_Team_Member__c> newOtms = new List<Our_Team_Member__c>();
        boolean alreadyAdded = false;
        for(CNMS_Role__c role: roles){
            //Add owner in team member
            if(role.User__c==UserInfo.getUserId()){
                Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = project.Id, User__c=role.User__c, Role__c=role.Role_Type__c, Access__c='Edit');
                if(selectedUser==UserInfo.getUserId()){
                    newMember.Project_Lead__c=true;
                    newMember.Access__c='All'; 
                    newMember.Role__c = Label.TeamLeader;  
                }
                newOtms.add(newMember);
                alreadyAdded = true;
            }
            
            
            //Add selected Lead
            if(role.User__c==selectedUser && selectedUser!=UserInfo.getUserId()){
                Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = project.Id, User__c=role.User__c, Role__c = Label.TeamLeader, Access__c='All',Project_Lead__c=true);
                newOtms.add(newMember);
                alreadyAdded = true;
            }
            
            
            //Add coach and gumbo
            if(!alreadyAdded && (role.Role_Type__c == Label.CEO ||  role.Role_Type__c == Label.RedCrown)){
                Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c = project.Id, User__c=role.User__c, Role__c=role.Role_Type__c, Access__c='View');
                if(role.Role_Type__c==Label.RedCrown || role.Role_Type__c == Label.CEO)
                    newMember.Access__c='Edit';
                newOtms.add(newMember);
                
            }
            alreadyAdded = false;
        }
        if(newOtms.size()>0){
            insert newOtms;
        }                        
    }
    
    /*
    * This method  Inserts the Account record into database and redirect user to InitiatePoject page.
    */
    public PageReference accsave(){
        try{
            List<Adversary_Account__c> duplicateAccounts = [select id from Adversary_Account__c where name__c = :account.Name__c];
            if(account.id==null && duplicateAccounts.size()>0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Account));
                return null;
            }
            account.isActive__c = true;
            upsert account;
            project.Adversary_Account__c = account.id;
            PageReference redirectPage = new PageReference('/apex/InitiateProject');
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    /*
    * This method update the project record into database and redirect user precious page based on the source parameter.
    */
    public PageReference updateProject(){
        //project = [select Name,Account__c, Opportunity__c, AP_Value__c, Closed_Reason__c, Last_Activity_Date__c, Project_Status__c, Start_Date__c,Client_Account__r.Name__c,Client_Account__c, Client_Account__r.isActive__c, Client_Account__r.Id,Adversary_Account__c, Adversary_Account__r.isActive__c, Adversary_Account__r.Name__c,Adversary_Account__r.Id,Description__c,Mission_and_Purpose__c,Project_Lead__c, Adversary_Lead__c,isActive__c from Project__c where id=:id];       
        if(project.Client_Account__c !=null && !project.Client_Account__r.isActive__c){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ClientAccountInactiveError));
            return null;
        } 
        if(project.Adversary_Account__c !=null && !project.Adversary_Account__r.isActive__c){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AdversaryAccountInactiveError));
            return null;
        } 
        try{
            update project;
            if(notifyTM){
                CNMSUtility.sendProjectEmail(project.Id, project.Name, true);
            }
            PageReference redirectPage = new PageReference('/apex/ViewProject?id='+id);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    private boolean validateProjectData(){
        boolean validationResult = true;
        if(project.Name == null || project.Name.trim()==''){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Enter Project Name'));
            validationResult = false;
        }
        
        if(project.Adversary_Account__c == null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Select Adversary Account'));
            validationResult = false;
        }
        return validationResult;
    }
    /*
    * This method Inserts the project record into database and redirect user to new project creation page.
    */
    public PageReference saveAndNew(){
        if(!validateProjectData()){
            return null;
        }
        try{
            insert project;
            createCoachGumboTeamMembers();
            project = new Project__c();
            //It is a new project being created. Intialize the values           
            List<CNMS_Role__c> currentUserRole = CNMSUtility.getCurrentUserRole();
            if(currentUserRole.size()>0 && currentUserRole[0].Client_Account__c !=null){
                clientAcc  = [select Id, Name__c from Client_Account__c where  id = :currentUserRole[0].Client_Account__c ];               
                project.Client_Account__c = currentUserRole[0].Client_Account__c;  
                project.isActive__c = true;
            }           
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }
        return null;
    }
    
   /*
    * This method Inserts the project record into database and redirect user to new Log creation page.
    */
    public PageReference saveAndCreateLog(){
        if(!validateProjectData()){
            return null;
        }
        try{
            insert project;
            createCoachGumboTeamMembers();  
            //return createLog();          
            return new PageReference('/apex/createLog?projid=' + project.Id + '&source=proj');
        }
        catch(Exception e) {
            System.debug('entering saveAndCreateLog exception--->');
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            System.debug('exiting saveAndCreateLog exception--->');
            return null;
        }    
    }
    
     /*
    * This method Inserts the project record into database and redirect user to new Checklist creation page.
    */
    public PageReference saveAndCreateChecklist(){
        if(!validateProjectData()){
            return null;
        }
        try{
            insert project;
            createCoachGumboTeamMembers();
            //return CreateChecklist();
            return new PageReference('/apex/createChecklist?projid=' + project.Id + '&source=proj');
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    
    
    /*
    * This method redirects user to Adversary Team Builder page.
    */
    public PageReference advsersaryTeamBuilder() {
        PageReference redirectPage = new PageReference('/apex/AdversaryTeamBuilder?projid='+id+'&accId='+project.Adversary_Account__c+'&acctype=Adversary');
        return redirectPage;
    }
    
    /*
    * This method redirects user to Client Team Builder page.
    */
    public PageReference clientTeamBuilder() {
        PageReference redirectPage = new PageReference('/apex/TeamBuilderTool?projid='+id+'&accId='+project.Client_Account__c+'&acctype=Our');
        return redirectPage;
    }
    
    public PageReference selectObservers() {
        PageReference redirectPage = new PageReference('/apex/selectObservers?projid='+id+'&accId='+project.Client_Account__c);
        return redirectPage;
    }
    //To be deleted
    /*public PageReference viewChecklist(){
        clId = ApexPages.currentPage().getParameters().get('clId');
        PageReference redirectPage = new PageReference('/apex/ViewChecklist?clId='+clId);
        return redirectPage;
    }
    
    public PageReference viewLog(){
        logId = ApexPages.currentPage().getParameters().get('logId');
        PageReference redirectPage = new PageReference('/apex/ViewLog?logId='+logId);
        return redirectPage;
    }
    
    public PageReference editChecklist(){
        clId = ApexPages.currentPage().getParameters().get('clId');
        PageReference redirectPage = new PageReference('/apex/EditLog?clId='+clId+'&id='+id);
        return redirectPage;
    }
    
    public PageReference editLog(){
        logId = ApexPages.currentPage().getParameters().get('logId');
        PageReference redirectPage = new PageReference('/apex/EditLog?logId='+logId+'&id='+id);
        return redirectPage;
    }
    
    public PageReference confirmChecklistDelete() {
        clId = ApexPages.currentPage().getParameters().get('clId');
        PageReference confirmDelete = new PageReference('/apex/ConfirmChecklistDelete');
        return confirmDelete;
    }
    
    public PageReference confirmLogDelete() {
        logId = ApexPages.currentPage().getParameters().get('logId');
        PageReference confirmDelete = new PageReference('/apex/ConfirmLogDelete');
        return confirmDelete;
    }
    //Delete ends
    
    */
    /*
    * This method deletes the checklist record.
    */
    public PageReference deleteChecklist() {
        try{
            clId = ApexPages.currentPage().getParameters().get('clId');
            Checklist__c cLToDelete = [select id,Project__c from Checklist__c where id=:clId];
            id = cLToDelete.Project__c;
            delete cLToDelete;
            PageReference projList = new PageReference('/apex/viewProject?id='+id);
            projList.setRedirect(true);
            return projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    /*
    * This method deletes the Log record.
    */
    public PageReference deleteLog() {
        try{
            logId = ApexPages.currentPage().getParameters().get('logId');
            Log__c logToDelete = [select id,Project__c,Checklist__r.Name from Log__c where id=:logId];
            id = logToDelete.Project__c;
            delete logToDelete;
            PageReference projList = new PageReference('/apex/viewProject?id='+id);
            projList.setRedirect(true);
            return projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    /*
    * This method redirect user to the create checklist page.
    */
    public PageReference createChecklist(){
        PageReference redirectPage = new PageReference('/apex/createChecklist?projid='+id + '&source=new');
        //redirectPage.setRedirect(true);
        return redirectPage;
    }
    
    /*
    * This method redirect user to the create Log page.
    */
    public PageReference createLog(){
        PageReference redirectPage = new PageReference('/apex/createLog?projid='+id + '&source=new');
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    
    /*
    * This method updates the Team members into database and redirect user update confirmation page.
    */
    public PageReference updateProjectLead(){
        try{
            update clientMembers;   
            PageReference redirectPage = new PageReference('/apex/LeadUpdatedConfirmation');
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
     /*
    * This method updates the Adversary Team members into database and redirect user update confirmation page.
    */
    public PageReference updateAdversaryLead(){
        try{
            update adversaryMembers;  
            PageReference redirectPage = new PageReference('/apex/LeadUpdatedConfirmation');
            return redirectPage;      
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
     /*
    * This method updates the selected and unselected Team members, and Project into database
    */
    public void closePopup() { 
        updateInProgress = true;
        if(selectedMember!=null && selectedMember!=oldselectedMember){ 
            try{
                System.debug('In Close Popup --->');
                List<Our_Team_Member__c> selectedTMs = [select id,Project_Lead__c,Access__c,User__c  from Our_Team_Member__c where id=:selectedMember];
                if(selectedTMs.size()>0){                   
                    Our_Team_Member__c selectedTM = selectedTMs[0];
                    selectedTM.Project_Lead__c = true;
                    selectedTM.Access__c = 'All'; // Change the access as all.
                    selectedTM.Role__c = Label.TeamLeader;
                    update selectedTM;
                    System.debug('Project Lead Updated for selected TM --->');
                    /*// Set this selected TM user as owner of the project. Code is commented and no suuficient user in system.
                    project.ownerId = selectedTM.User__c;
                    update project;*/
                    projectOwnerId  = selectedTM.User__c;
                    if(oldSelectedMember!='' && oldSelectedMember!=null)  { 
                        Our_Team_Member__c unselectedTM = [select id,Project_Lead__c,User__c, Access__c,Role__c  from Our_Team_Member__c where id=:oldSelectedMember];
                        unselectedTM.Project_Lead__c = false;
                        unselectedTM.Access__c = 'Edit'; // Change the access to view.    
                        // Change the role as per the CNMS role for this user.                    
                        unselectedTM.Role__c = [Select Role_Type__c from CNMS_Role__C where user__c = :unselectedTM.User__c LIMIT 1].Role_Type__c;            
                        update unselectedTM;
                        System.debug('Project Lead Updated for Unselected TM --->');
                    }
                    //Populate Project Leader on project
                    //project.Project_Lead__c = selectedTM.id;
                    //update project;
                    
                    oldselectedMember = selectedMember;
                    populateOTMData();
                    //clientMembers = [select User__r.Name,Project_Lead__c, Role__c, Access__c from Our_Team_Member__c where Project__c=:id order by User__r.Name];               
                    update clientMembers ; // this is being done in order to update the project sharing. as project sharing was deleted when owner was changed on chaning project lead.
                    //Populate team members to be displayed
                    populateTMListToBeDisplayed(parseInteger(Label.TeamMembersToBeDisplayed));   
                    populateOTMData();             
                    displaySuccess = true;
                }
            }
            catch(Exception e) {
                System.debug('Error on selected TM --->' + e.getMessage());
                Error_Log.logError(e);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
                return;
            }
        }
        displayPopup = false;    
        
    }

     /*
    * This method updates the selected and unselected Adversary Team members, and Project into database
    */
     public void closeAdversaryPopup() { 
        updateInProgress = true;
        if(selectedAdversaryMember!=oldselectedAdversaryMember){ 
            try{
                Adversary_Team_Member__c selectedATM = [select id,Adversary_Lead__c,Adversary_Contact__c  from Adversary_Team_Member__c where id=:selectedAdversaryMember];
                selectedATM.Adversary_Lead__c = true;
                update selectedATM;
                if(oldselectedAdversaryMember!='' && oldselectedAdversaryMember!=null)  { 
                Adversary_Team_Member__c unselectedATM = [select id,Adversary_Lead__c from Adversary_Team_Member__c where id=:oldselectedAdversaryMember];
                unselectedATM.Adversary_Lead__c = false;
                update unselectedATM;
                }
                //Populate Project Leader on project
                project.Adversary_Lead__c = selectedATM.id;
                update project;
                
                oldselectedAdversaryMember = selectedAdversaryMember;
              
            
                adversaryMembers  = [select Adversary_Contact__r.Name__c,Member_Name__c,Adversary_Lead__c,Adversary_Contact__c from Adversary_Team_Member__c where Project__c=:id order by Adversary_Contact__r.Name__c];
                           
                //Populate adversary team members to be displayed
                populateATMListToBeDisplayed(parseInteger(Label.AdversaryTeamMembersToBeDisplayed));
            
                displaySuccess = true;
            }
            catch(Exception e) {
                Error_Log.logError(e);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
                return;
            }
        }
        displayPopup = false;    
        
    }
    
    /*
    * This method sets the flags
    */
    public void closeSuccess(){
        buttonClicked = true;
        displaySuccess = false;
        updateInProgress = false;     
    }
    
    /*
    * This method sets the flags
    */
    public void showPopup() {        
        displayPopup = true;    
        buttonClicked = false;
        updateInProgress = false; 
    }
    
    /*
    * This method sets the flags
    */
    public void hidePopup() {        
        displayPopup = false;
        buttonClicked = true;   
        updateInProgress = true; 
    }
    /*
    * This method Returns the list of Team members,
    */
    public List<SelectOption> getMembersForProjectLead() {
        List<SelectOption> options = new List<SelectOption>(); 
        //for(Our_Team_Member__c objTM : clientMembers){
        for(Our_Team_Member__c objTM : clientMembersForProjectLead){
            //if(objTM.Role__c!='Coach'&& objTM.Role__c!='Gumbo')
            options.add(new SelectOption(objTM.id,objTM.User__r.Name)); 
        }
        
         return options; 
    }
    
     /*
    * This method Returns the list of Adversary Team members,
    */
    public List<SelectOption> getAllAdversaryMembers() {    
        List<SelectOption> options = new List<SelectOption>(); 
        for(Adversary_Team_Member__c objATM : adversaryMembers){
            options.add(new SelectOption(objATM.id,objATM.Adversary_Contact__r.Name__c)); 
        }
        
         return options; 
    }
    
    public PageReference displayPermissionsPage(){
        return new PageReference('/apex/UpdatePermissions?projid='+id+'');
    }
    
    public PageReference updateSharing(){
        update clientMembersForUpdatePermission ;
        PageReference npr = new PageReference('/apex/ViewProject?id='+id);
        npr.setRedirect(true);
        return npr ;
    }
    
    public List<SelectOption> getAllUsers() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(UserInfo.getUserId(),UserInfo.getFirstName()+' '+UserInfo.getLastName()+' ('+UserInfo.getUserName()+')'));
            
            for(CNMS_Role__c role : [select Id, User__r.Name,User__r.UserName,User__c from CNMS_Role__c where (Role_Type__c='Team Member' or Role_Type__c='Dumbo') and User__c!=:UserInfo.getUserId()]){
                options.add(new SelectOption(role.User__c,role.User__r.Name+' ('+role.User__r.UserName+')'));
            }
            return options;
        }
}