public with sharing class SelectTeamMembersController {
    
    public List<User> TeamMembers;
    public SelectTeamMembersController(ApexPages.StandardSetController controller) {
        TeamMembers = controller.getRecords();
    }
    
    public PageReference save(){
        try{
            update TeamMembers;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,Label.MemberSelectedSuccess));
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
           
        }
        return null;
    } 
    
    public PageReference back(){
        Pagereference redirectPage = new Pagereference('/apex/ProjectList');
        return redirectPage;
    }   

}