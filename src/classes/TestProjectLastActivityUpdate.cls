@IsTest
private class TestProjectLastActivityUpdate
{
    private static TestMethod void testTrigger(){
        
        //Step 1 : Data Insertion
        Adversary_Account__c a=new Adversary_Account__c(Name='Test Account', Name__c='Test Account');
           insert a;
        Project__c p=new Project__c (Name = 'Test Project');
            insert p;
        
        
        
        test.startTest();

        Collaboration__c c= new collaboration__c (Project__c=p.id);
            insert c;

        p = [SELECT Id, Last_Activity_Date__c FROM Project__c WHERE Id=:p.Id];
        system.assertEquals(date.today(), p.Last_Activity_Date__c);
              

        test.stopTest();
    }
}