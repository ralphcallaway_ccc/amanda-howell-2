public Class CNMSUtility{

    public static List<CNMS_Role__c> getCurrentUserRole(){
        return getUserRole(UserInfo.getUserId());
    }
    
    public static List<CNMS_Role__c> getUserRole(Id userId){
        return [Select Id, Role_Type__c, Client_Account__c from CNMS_Role__c where User__c = :userId];
    }
    
    public static boolean isAdminUser(Id userId){
        return [select CNMS_Admin__c from User where id = :userId Limit 1].CNMS_Admin__c;
    }
    
    public static String getCurrentUserProjectAccessLevel(Id parentId){
        return getProjectAccessLevel(parentId, UserInfo.getUserId());
    }
    
    public static String getProjectAccessLevel(Id parentId, Id UserOrGroupId){
        String Access ;
        try{
            Access = [select AccessLevel from Project__Share where ParentId = :parentId AND UserOrGroupId = :UserOrGroupId Limit 1].AccessLevel ;
           }
        catch(Exception e){
            Access = 'none';
        } 
        return Access;    
    }
    
    public static Map<Id, String> getCurrentUserProjectAccessLevel(Set<Id> parentIds){
        return getProjectAccessLevel(parentIds, UserInfo.getUserId());
    }
    
    public static Map<Id, String> getProjectAccessLevel(Set<Id> parentIds, Id UserOrGroupId){
        Map<Id, String> AccessMap = new Map<Id, string>();
        for(Project__Share ps: [select AccessLevel, ParentId  from Project__Share where ParentId IN :parentIds AND UserOrGroupId = :UserOrGroupId]){
            AccessMap.put(ps.parentId, ps.AccessLevel);
        }
        return AccessMap;    
    }
    
    public static String getCurrentUserAccountAccessLevel(Id parentId){
        return getAccountAccessLevel(parentId, UserInfo.getUserId());
    }
    
    public static String getAccountAccessLevel(Id parentId, Id UserOrGroupId){
        String Access ;
        try{
            Access = [select AccessLevel from Adversary_Account__Share where ParentId = :parentId AND UserOrGroupId = :UserOrGroupId Limit 1].AccessLevel ;
           }
        catch(Exception e){
            Access = 'none';
        } 
        return Access;    
    }
    
    public static Map<Id, String> getCurrentUserAccountAccessLevel(Set<Id> parentIds){
        return getAccountAccessLevel(parentIds, UserInfo.getUserId());
    }
    
    public static Map<Id, String> getAccountAccessLevel(Set<Id> parentIds, Id UserOrGroupId){
        Map<Id, String> AccessMap = new Map<Id, string>();
        for(Adversary_Account__Share advs: [select AccessLevel, ParentId  from Adversary_Account__Share where ParentId IN :parentIds AND UserOrGroupId = :UserOrGroupId]){
            AccessMap.put(advs.parentId, advs.AccessLevel);
        }
        return AccessMap;    
    }
    
    @future
    public static void sendLogChecklistEmail(String projId, String objectType, String recordName, Boolean isUpdate, String recordURL){
        List<String> emails = new List<String>();
        String ProjName;
        String ProjStatus;
        //Query al the team members 
        System.debug('In After Insert CL TRigger---->');
        for(Our_Team_Member__c objOTM : [select id,User__r.Name,User__r.Email,Project__c,Project__r.Name, Project__r.Project_status__c from Our_Team_Member__c where Project__c = :projId]){     
            emails.add(objOTM.User__r.Email);
            projName = objOTM.Project__r.Name;
            projStatus = objOTM.Project__r.Project_status__c;
        }
        String BaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        String[] BaseURLSplitted = BaseURL.split('://')[1].split('\\.');
        BaseURL = 'https://c.na12.visual.force.com';
        System.debug('BaseURL ----------->' + BaseURL );
        List<Messaging.SingleEmailMessage> SingleEmailMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(emails);
        mail.setSubject(objectType + ' (' + recordName + ') for '+ projName + ' has been created');
        mail.setHtmlBody(objectType  + ' URL: <p>' + BaseURL  + recordURL + '</p> <p> A new ' + objectType + ' has been created by ' + UserInfo.getName()+'</p>');
        if(isUpdate){
            if(objectType=='Project' && ProjStatus != 'Closed – Fadeaway' && ProjStatus != 'Need Assistance' ){
                mail.setSubject(objectType + ' (' + recordName + ') ' + ' has been updated');
                mail.setHtmlBody(objectType  + ' URL: <p>' + BaseURL  + recordURL + '</p> <p>' +UserInfo.getName()+ ' has updated the ' + objectType + '</p>');
            }
            else if (objectType=='Project' && ProjStatus == 'Closed – Fadeaway'){
                mail.setSubject(objectType + ' (' + recordName + ') ' + ' is Closed – Fadeaway');
                mail.setHtmlBody(objectType  + ' URL: <p>'  + BaseURL  + recordURL + '</p> <p>' +UserInfo.getName()+ ' has set the status of ' + objectType + projName + ' to Closed – Fadeaway.</p>');
            }
            else if (objectType=='Project' && ProjStatus == 'Need Assistance'){
                mail.setSubject(objectType + ' (' + recordName + ') ' + ' needs Assistance');
                mail.setHtmlBody(objectType  + ' URL: <p>'  + BaseURL  + recordURL + '</p> <p>' +UserInfo.getName()+ ' has set the status of ' + objectType + projName + ' to Need Assistance.</p>');
            }
            else {
                mail.setSubject(objectType + ' (' + recordName + ') for '+ projName + ' has been updated');
                mail.setHtmlBody(objectType  + ' URL: <p>' + BaseURL  + recordURL + '</p> <p>' +UserInfo.getName()+ ' has updated the ' + objectType + '</p>');
           }
        }
        system.debug('mail****'+mail);
        SingleEmailMessages.add(mail);
        Messaging.sendEmail(SingleEmailMessages);
    }
    

    public static void sendLogEmail(Id recordID, String projId, String logName, Boolean isUpdate){
        String recordURL= '/apex/ViewLog?Id=' + recordID + '&projId=' + projId;
        sendLogChecklistEmail(projId, 'Log', logName, isUpdate, recordURL);
    }
    

    public static void sendChecklistEmail(Id recordID, String projId, String clName, Boolean isUpdate){
        String recordURL= '/apex/ViewChecklist?Id=' + recordID + '&projId=' + projId;
        sendLogChecklistEmail(projId, 'Checklist', clName, isUpdate, recordURL);
    }
    
    public static void sendProjectEmail(String projId, String projName, Boolean isUpdate){
        String recordURL= '/apex/ViewProject?Id=' + projId;
        sendLogChecklistEmail(projId, 'Project', projName, isUpdate, recordURL);
    }
    
    public static void sendProjectStatusEmail(String projId, String projName, Boolean isUpdate){
        String recordURL= '/apex/ViewProject?Id=' + projId;
        sendLogChecklistEmail(projId, 'Project', projName, isUpdate, recordURL);
    }
    
    @future
    public static void NotifyNewTeamMembers(String projId, String projName, List<String> emails){       
        List<Messaging.SingleEmailMessage> SingleEmailMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(emails);
        mail.setSubject(Label.EmailSubjectNewMember);
        mail.setHtmlBody(' You have been added as team member to the ' + projName + ' project : <p>' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/ViewProject?Id='+ projId + '</p>');       
        system.debug('mail****'+mail);
        SingleEmailMessages.add(mail);
        Messaging.sendEmail(SingleEmailMessages);
    }
    
}