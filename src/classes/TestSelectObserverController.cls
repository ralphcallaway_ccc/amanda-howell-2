@isTest
private class TestSelectObserverController {
     public static testMethod void testSelectObserverController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
    
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;
        
        String[] teamMemberRole = new String[]{Label.TeamLeader, Label.TeamMember,Label.TeamLeader, Label.TeamMember, Label.Observer};
        String[] CNMSRoles = new String[]{Label.TeamMember, Label.CEO, Label.TeamMember, Label.Coach, Label.TeamMember};
        List<User> users = new List<User>();
        List<Our_Team_Member__c>  otms = new List<Our_Team_Member__c>();
        List<CNMS_Role__c>  roles = new List<CNMS_Role__c>();
        // Create total 5 users
        for(Integer i = 0; i<5; i++){
            User u = createUser('testing'+i+'@testorg.com', 't'+i);
            u.isTeamMember__c = true;
            u.Client_Account__c = acc.id;
            u.isActive = true;
            u.lastname='Testing'+i;
            u.firstname = 'Testing'+i;
            users.add(u);
            
        } 
        insert users;
        
        // Create total 5 Roles
        for(Integer i = 0; i<5; i++){
            CNMS_Role__c rl = new CNMS_Role__c();
            rl.User__c = users[i].Id;
            rl.Role_Type__c = Label.TeamMember;
            roles.add(rl);
        } 
        insert roles;
        //Create total 3 already selected Observers. user0,1 and 2.
        for(Integer i = 0; i<3; i++){
            Our_Team_Member__c tm = new Our_Team_Member__c (User__c =users[i].Id, Project__c = pro.id); 
            tm.Role__c = Label.Observer;
            tm.Access__c = 'View';
            otms.add(tm);            
        }         
        insert otms ;
        // Note that user 3 and 4 are not Team members, hence should be in unselected list.
        PageReference currentPage = new PageReference('/apex/selectObservers');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',acc.id);        
        Test.setCurrentPage(currentPage );
        SelectObserverController  selectObserverController  = new SelectObserverController ();
        
        //Verify data is prepared as per expectation.       
        System.assert(selectObserverController.getSelectedValues().size()== otms.size()); // Check selected values.
        System.assert(selectObserverController.getunSelectedValues().size()== roles.size() - otms.size()); // Check unselected values.
        
        
        // Test Select Click. 
        selectObserverController.leftselected.add(users[3].firstName + ' ' + users[3].LastName);              
        selectObserverController.selectclick();
        //Verify the item is added in the right list and removed from left list.       
        System.assertEquals(selectObserverController.getSelectedValues().size(),otms.size()+1);
        System.assertEquals(selectObserverController.getunSelectedValues().size(),roles.size() - otms.size() -1 );
        
        // Test UnSelect Click. 
        selectObserverController.RightSelected.add(users[0].firstName + ' ' + users[0].LastName);              
        selectObserverController.unselectclick();
        //Verify the item is added in the right list and removed from left list.
        System.assertEquals(selectObserverController.getSelectedValues().size(),otms.size());
        System.assertEquals(selectObserverController.getunSelectedValues().size(),roles.size() - otms.size());      
        
        //Test Done              
        PageReference testPage = selectObserverController.done();
        List<Our_Team_Member__c> newOtms = [select id from Our_Team_Member__c where User__c =: users[3].Id];
        List<Our_Team_Member__c> deletedOtms = [select id from Our_Team_Member__c where User__c =: users[0].Id];
        System.assert(testPage.getUrl().contains('viewProject')); 
        System.assert(newOtms.size()>0);
        System.assert(deletedOtms.size()==0);
    }
    
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}