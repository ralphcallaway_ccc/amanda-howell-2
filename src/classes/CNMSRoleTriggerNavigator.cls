public Class CNMSRoleTriggerNavigator{
    List<CNMS_Role__c> newRecords;
    List<CNMS_Role__c> oldRecords;
    //Constructor
    public CNMSRoleTriggerNavigator(List<CNMS_Role__c> TriggerNewRecords,  List<CNMS_Role__c> TriggerOldRecords, Map<Id,CNMS_Role__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        CNMSRoleTriggerUtility.setNewRecords(newRecords);
        CNMSRoleTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void ShareAccountsWithNewUserRole(){
        CNMSRoleTriggerUtility.ShareAccountsWithNewUserRole();
    }
    
    public void UnshareAccountsWithDeletedUserRole(){
        CNMSRoleTriggerUtility.UnshareAccountsWithDeletedUserRole();
    }
	
	public void createOTMForCEORoles(){
        CNMSRoleTriggerUtility.createOTMForCEORoles();
    }
    
    public void updateRolesOperations(){
        CNMSRoleTriggerUtility.updateRolesOperations();
    } 
    
    public void preventRoleDeletionForProjectowners(){
        CNMSRoleTriggerUtility.preventRoleDeletionForProjectowners();
    } 
    
    public void deleteRelatedOTMS(){
        CNMSRoleTriggerUtility.deleteRelatedOTMS();
    } 
    
}