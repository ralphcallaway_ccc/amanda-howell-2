public class TeamBuilder{
    Set<String> originalValuesLeft = new Set<String>();
    Set<String> originalValuesRight = new Set<String>();
    Map<String,Our_Team_Member__c> currentTeamMemberMap = new Map<String,Our_Team_Member__c>();
    Map<String,ID> userNameIDMap = new Map<String,Id>();
    Map<String,CNMS_Role__c> userNameRoleMap = new Map<String,CNMS_Role__c>();
    Set<Id> existingObserverUserId;
    Public List<String> leftselected{get;set;}
    Public List<String> rightselected{get;set;}
    Set<String> leftvalues = new Set<String>();
    Set<String> rightvalues = new Set<String>();
    Set<String> membersToAdd = new Set<String>();
    Set<String> membersToDelete = new Set<String>();
    public String accId {get; set;}
    public String projId {get; set;}
    private Id ProjectOwnerId;
    public String acctype {get; set;}
    public Project__c currentProject {get; set;}
    public Boolean hasEditAccess {get;set;}
    public String AccessLevel {get;set;}
    public Boolean NotifyTM {get;set;}
    private String[] teamMemberRole = new String[]{Label.TeamMember, Label.Coach, Label.TeamLeader};
    private List<String> newMembersEmail; 
    public TeamBuilder(){
        leftselected = new List<String>();
        rightselected = new List<String>();
        //leftvalues.addAll(originalValues);
        accId = ApexPages.currentPage().getParameters().get('accId');
        projId = ApexPages.currentPage().getParameters().get('projId');
        acctype = ApexPages.currentPage().getParameters().get('acctype');
        hasEditAccess = false;
        //Get project owner id, in order to skip project owner from selection/unselection
        currentProject = [select OwnerId, name, Adversary_Account__r.isActive__c from Project__c where Id = :projId];
        ProjectOwnerId = currentProject.OwnerId;
        // Ensure account is active, else give error.
        if(!currentProject.Adversary_Account__r.isActive__c){
            hasEditAccess = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, Label.AdversaryAccountInactiveError)); 
            return;
        }   
        existingObserverUserId = new Set<Id>();
        existingObserverUserId.add(ProjectOwnerId);
        for( Our_Team_Member__c otm : [select id,User__c from Our_Team_Member__c where Project__c =: projId AND Role__c = :Label.Observer]){      
            existingObserverUserId.add(otm.User__c);
        }
        //Get all already select team members
        for(Our_Team_Member__c teamMember : [select id,User__r.Name, User__r.username from Our_Team_Member__c where Project__c =: projId AND Role__c IN :teamMemberRole AND User__c<>:ProjectOwnerId ]){
            String displayText = teamMember.User__r.Name;
            rightValues.add(displayText); 
            currentTeamMemberMap.put(displayText,teamMember);
        }
        
        originalValuesRight.addAll(rightValues);
        
        //Get all Roles which are eligible to become our team members
        for(CNMS_Role__c objRole : [select id, Role_Type__c, User__c, User__r.Name, User__r.email, User__r.username from CNMS_Role__c where Role_Type__c IN :teamMemberRole AND User__c NOT IN :existingObserverUserId And User__r.IsActive = true]){
            String displayText = objRole.User__r.Name;
            userNameIDMap.put(displayText,objRole.User__r.Id);
            userNameRoleMap.put(displayText,objRole);
            if(!rightValues.contains(displayText))
                leftvalues.add(displayText);
        }     
        originalValuesLeft.addAll(leftValues);
        
        AccessLevel  = CNMSUtility.getCurrentUserProjectAccessLevel(projId);
        if(AccessLevel =='Edit' || AccessLevel =='All'){
            for(Our_Team_Member__c otm: [select User__c from Our_Team_Member__c where Project__c =: projId]){
                if(otm.User__c == UserInfo.getUserId()){
                    hasEditAccess = true;
                    break;
                }
            }                
        }
    }

    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }

    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
    
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<String> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(String s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }

    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<String> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    
    public PageReference done(){
        System.debug('rightvalues****'+rightValues);
        System.debug('originalRightvalue****'+originalvaluesRight);
        System.debug('leftvalues****'+leftValues);
        System.debug('originalLeftvalue****'+originalValuesLeft);
        newMembersEmail = new List<String>();
        try{
            //Creating set of newly selected values
            for(String rightValue : rightValues){
                if(!originalValuesRight.contains(rightValue))
                    membersToAdd.add(rightValue);
            }
            //Creating set of unselected values
            for(String originalValueRight : originalValuesRight){
                if(!rightValues.contains(originalValueRight))
                    membersToDelete.add(originalValueRight);
            }
            
            System.debug('membersToAdd****'+membersToAdd);
            System.debug('membersToDelete****'+membersToDelete);
            
            //Create Team Memebers list to be inserted
            List<Our_Team_Member__c> teamMembersToInsert = new List<Our_Team_Member__c>();
            for(String userName : membersToAdd){
                CNMS_Role__c myRole = userNameRoleMap.get(userName);
                Our_Team_Member__c newMember = new Our_Team_Member__c(Project__c=projId,User__c= myRole.User__c, Role__c=myRole.Role_Type__c, Access__c= 'View');
                teamMembersToInsert.add(newMember);
                newMembersEmail.add(myRole.User__r.Email);
            }
            
            //Create Team Memebers list to be deleted
            List<Our_Team_Member__c> teamMembersToDelete = new List<Our_Team_Member__c>();
            for(String userName : membersToDelete){
                Our_Team_Member__c delMember = currentTeamMemberMap.get(userName);
                teamMembersToDelete.add(delMember);
            }
            
            System.debug('teamMembersToInsert****'+teamMembersToInsert);
            System.debug('teamMembersToDelete****'+teamMembersToDelete);
            
            if(teamMembersToInsert!=null && teamMembersToInsert.size()>0){
                insert teamMembersToInsert;
                if(NotifyTM){
                    CNMSUtility.NotifyNewTeamMembers(projId, currentProject.name,newMembersEmail);
                }
            }    
                
            if(teamMembersToDelete!=null && teamMembersToDelete.size()>0)
                delete teamMembersToDelete;
            
            PageReference redirectPage = new PageReference('/apex/viewProject?Id='+projId);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    } 
}