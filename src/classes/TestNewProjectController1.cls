@isTest
private class TestNewProjectController1 {
   
     public static testMethod void testNewProjectController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        
        Adversary_Account__c advacc1 = new Adversary_Account__c (name__c='test', isActive__c = true);
        insert advacc1 ;
        
        Adversary_Contact__c con =      new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
        Adversary_Contact__c conTest0 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='test@test.com');
        Adversary_Contact__c conTest1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = false,First_Name__c='test3',Last_Name__c='test3',Add_To_Team__c=true, Email__c='test@test.com');        
        Adversary_Contact__c conTest2 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test4',Last_Name__c='test4',Email__c='test@test.com');
        insert con ;
        insert conTest0; 
             
        Project__c pro = new Project__c(name='test',Client_Account__c = acc.id, Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
                 
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = u2.Id, Role_Type__c = Label.teamMember);
        insert r2;  
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r3;  
        
        Checklist__c cl = new Checklist__c (Project__c = pro.id, Checklist_Name__c= 'Test Checklist');
        insert cl;
        
        Log__c log = new Log__c(Project__c = pro.id, Checklist__c = cl.id, Log_Name__c='Test Log');
        insert log;
        
        Our_Team_Member__c  oldTm = new Our_Team_Member__c (User__c = u1.Id, Project__c = pro.id, Project_Lead__c=true, Role__c = 'Team Leader', Access__c = 'View');
        insert oldTm;
        
        Adversary_Team_Member__c oldAtm = new Adversary_Team_Member__c (Adversary_Contact__c = con.Id, Project__c = pro.id, Adversary_Lead__c=true);
        insert oldAtm ;
        PageReference currentPage  = new PageReference('/apex/ViewProject?Id='+pro.Id+ '&clId='+cl.Id +'&logId='+ log.Id + '&accId='+advacc.Id);
        Test.setCurrentPage(currentPage);
        // Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(pro);
 
        // create the controller
         
        NewProjectController newProjectController = new NewProjectController(sc);    
        
        //Test  back Method
        PageReference testPage = newProjectController.back();        
        System.assert(testPage.getUrl().contains('ViewChecklist'));
        
        //Test  cancel Method
        testPage = newProjectController.cancel();        
        System.assert(testPage.getUrl().contains('ViewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('Id'));
        
        //Test  acccancel Method
        testPage = newProjectController.acccancel(); 
        System.assert(testPage.getUrl().contains('InitiateProject'));       
        
        //Test  editProject Method
        testPage = newProjectController.editProject();        
        System.assert(testPage.getUrl().contains('EditProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
        
        //Test  createAccount Method
        testPage = newProjectController.createAccount();      
        System.assert(testPage.getUrl().contains('CreateAccountFromProject'));  
        
        //Test  accsave Method
        newProjectController.account.name__c='advesary1';
        testPage = newProjectController.accsave();
        System.assert(testPage.getUrl().contains('InitiateProject'));
        
        //Test updateProject
        
        testPage = newProjectController.updateProject();//Should result in exception as adv is inactive.
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        //Now first activate Client account.
        newProjectController.project.Client_Account__r.isActive__c = true;      
        //Now activate Adv account.
        newProjectController.project.Adversary_Account__r.isActive__c = true;       
        //call update proj again   
        testPage = newProjectController.updateProject() ; 
        System.assert(testPage.getUrl().contains('ViewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
       
        //Test save
        testPage = newProjectController.save();//Should result in exception as trying to insert existing record.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        //Set contrller project as new project
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController.save();
        System.assert(testPage.getUrl().contains('ViewProject'));
        Id newprojid = testPage.getParameters().get('id');
        List<Project__c> newProj = [select Id from Project__c where id = :newProjId ];
        System.assert(newProj.size()>0);
        
        //Test save n new
        newProjectController.saveAndNew();//Should result in exception as trying to insert existing record.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        //Set contrller project as new project
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        newProjectController.saveAndNew();              
        System.assert(newProjectController.project.Adversary_Account__c==null);
        
        //reset Controller
        Test.setCurrentPage(currentPage);
        
        
        // create the controller
         
        NewProjectController newProjectController2 = new NewProjectController(sc);     
         //Test saveAndCreateLog
        newProjectController.save(); 
        testPage = newProjectController.saveAndCreateLog();  // cover exception
        System.assertEquals(null, testPage);
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController.saveAndCreateLog();  
        System.assert(testPage.getUrl().contains('createLog'));  
        
        //Test saveAndCreateChecklist
        testPage = newProjectController.saveAndCreateChecklist(); // cover exception
        System.assertEquals(null, testPage);
        newProjectController.project = new Project__c(name='test',Adversary_Account__c=advacc1.Id);              
        testPage = newProjectController.saveAndCreateChecklist();  
        System.assert(testPage.getUrl().contains('createChecklist')); 
                   
        //Test  advsersaryTeamBuilder Method
        testPage = newProjectController.advsersaryTeamBuilder();        
        System.assert(testPage.getUrl().contains('AdversaryTeamBuilder'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
        //Test  clientTeamBuilder Method
        testPage = newProjectController.clientTeamBuilder();        
        System.assert(testPage.getUrl().contains('TeamBuilderTool'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));

        
        //Test  deleteChecklist Method
        testPage = newProjectController.deleteChecklist();        
        System.assert(testPage.getUrl().contains('viewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
       
        //Test  deleteLog Method
        testPage = newProjectController.deleteLog();        
        System.assert(testPage.getUrl().contains('viewProject'));
        System.assertEquals(pro.id, testPage.getParameters().get('id'));
        
        //Test  createChecklist Method
        testPage = newProjectController.createChecklist();        
        System.assert(testPage.getUrl().contains('createChecklist'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
        //Test  createLog Method
        testPage = newProjectController.createLog();        
        System.assert(testPage.getUrl().contains('createLog'));
        System.assertEquals(pro.id, testPage.getParameters().get('projid'));
        
         //Test  updateProjectLead Method
        testPage = newProjectController.updateProjectLead();        
        System.assert(testPage.getUrl().contains('LeadUpdatedConfirmation'));
        
         //Test  updateAdversaryLead Method
        testPage = newProjectController.updateAdversaryLead();        
        System.assert(testPage.getUrl().contains('LeadUpdatedConfirmation'));
    
    }    
    
    
    
   
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}