@isTest
private class TestProjectController {

    public static testMethod void testProjectController() {
        Client_Account__c acc = new Client_Account__c(name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
        
        Adversary_Account__c inactiveAdvacc = new Adversary_Account__c (name__c='InactiveAdvesary', isActive__c = false);
        insert inactiveAdvacc;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
        
        Project__c InactivePro = new Project__c(name='test',Adversary_Account__c=advacc.id, isActive__c = false);
        insert InactivePro;
        
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;  
        
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r2;
        
        PageReference currentPage = new PageReference('/apex/ProjectList');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('accId',advacc.id);
        Test.setCurrentPage(currentPage );
        ProjectController projectController = new ProjectController();  
        //Test  createNewProject Method
        PageReference testPage = projectController.createNewProject();     
        System.assert(testPage.getUrl().contains('InitiateProject'));    
        
        
        //Test  createNewProjectAll Method
        testPage = projectController.createNewProjectAll();      
        System.assert(testPage.getUrl().contains('InitiateProject'));  
        //Test createNewAdversaryAccount
        testPage = projectController.createNewAdversaryAccount();
        System.assert(testPage.getUrl().contains('CreateAccount'));
        //Test createNewAdversaryAccountAll
        testPage = projectController.createNewAdversaryAccountAll();
        System.assert(testPage.getUrl().contains('CreateAccount')); 
        
         //Test editProject
        testPage = projectController.editProject();
        System.assert(testPage.getUrl().contains('EditProject')); 
        
        //Test editAdversaryAccount
        testPage = projectController.editAdversaryAccount();
        System.assert(testPage.getUrl().contains('EditAccount'));
        
        //Test viewProject
        testPage = projectController.viewProject();
        //System.assert(testPage.getUrl().contains('viewProject')); 
        
        //Test viewAdversaryAccount
        testPage = projectController.viewAdversaryAccount();
        System.assert(testPage.getUrl().contains('ViewAccount')); 
        
        //Test confirmDelete
        testPage = projectController.confirmDelete();
        System.assert(testPage.getUrl().contains('ConfirmProjectDelete'));
        
        //Test confirmAdversaryDelete
        testPage = projectController.confirmAdversaryDelete();
        System.assert(testPage.getUrl().contains('ConfirmAccountDelete'));
        
        //Test deactivateProject, deactivateAccount, activate account
        //first deactivate Adv account.
        projectController.accIdToDeActivate = advacc.Id;
        projectController.deactivateAccount();
        Adversary_Account__c ad1 = [select isActive__c from Adversary_Account__c where id=:advacc.Id];
        System.assert(!ad1.isActive__c);//Acc is Deactivted
        //deactivate proj
        projectController.projIdToDeActivate=pro.id;
        testPage = projectController.deactivateProject();
        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR)); // Project cannot be deactivated as the account is inactive.
        //Now activate Client account.
        acc.isActive__c = true;
        update acc;
        //Now activate Adv account.
        projectController.activateAccount();//Should result in exception as no accountidis set for activation.
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL)); //To cover exception.
        projectController.accIdToActivate = advacc.Id; //Set acc id for activation
        projectController.activateAccount(); // try to activate now,
        ad1 = [select isActive__c from Adversary_Account__c where id=:advacc.Id];
        System.assert(ad1.isActive__c);//Acc is activted
        
        //Try deactivating proj now.
        testPage = projectController.deactivateProject();
        Project__c deActiveProj = [select id,isActive__c from Project__c where id=:pro.id];
        System.Assert(!deActiveProj.isActive__c);
        
        //Test deleteProject
        testPage = projectController.deleteProject();
        List<Project__c> deletedProj = [select id from Project__c where id=:pro.id];
        //as the above project is deleted, hence query should return null.
        System.assert(deletedProj.size()==0);
        System.assert(testPage.getUrl().contains('ProjectList'));
        //Now try to delete the same project again to get an exception.
        projectController.deleteProject();
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
         //Test deleteAccount
        testPage = projectController.deleteAccount();
        List<Adversary_Account__c> deleteAccount= [select id from Adversary_Account__c where id=:advacc.Id];
        //as the above Account is deleted, hence query should return null.
        System.assert(deleteAccount.size()==0);
        System.assert(testPage.getUrl().contains('ProjectList'));
        //Now try to delete the same Account again to get an exception.
        projectController.deleteAccount();
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
        //Test cancel
        testPage = projectController.cancel();
        System.assert(testPage.getUrl().contains('ProjectList')); 
        
        //Test addContact
        testPage = projectController.addContact();
        System.debug(testPage.getUrl());
        System.assert(testPage.getUrl().contains('CreateContact'));
        System.assertEquals('home', testPage.getParameters().get('source'));
        
        //Test addContactRoster
        testPage = projectController.addContactRoster();
        System.assert(testPage.getUrl().contains('CreateContact'));
        System.assertEquals('roster', testPage.getParameters().get('source'));
        
        //Test selectOurTeamMembers
        testPage = projectController.selectOurTeamMembers();
         System.assert(testPage.getUrl().contains('SelectOurTeamMembers'));
       
        
        //Test showInactiveAdversaryAccounts
        testPage = projectController.showInactiveAdversaryAccounts();
        System.assert(testPage.getUrl().contains('AllInactiveAdversaryAccounts'));
        
        //Test showInactiveProjects
        testPage = projectController.showInactiveProjects();
        System.assert(testPage.getUrl().contains('AllInactiveProjects'));
        projectController.projIdToActivate = InactivePro.Id;
        projectController.activateProject();
           
    }

     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}