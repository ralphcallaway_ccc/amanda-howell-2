public with sharing class Error_Log {
    public static void logError(Exception e){
        String type = '';
        String className = '';
        String methodName = '';
        String lineNumber = '';
        String message = '';
        
        try {
            //Create a custom message out of exception message
            System.debug('exp***'+String.valueOf(e));
            message = String.valueOf(e);
            String exp = e.getStackTraceString();
            System.debug('exp1***'+exp);
            
            System.debug('Class/trigger***'+exp.substring(0,exp.indexOF('.')));
            type = exp.substring(0,exp.indexOF('.'));
            
            exp = exp.substring(exp.indexOf('.')+1);
            System.debug('exp2***'+exp);            
            System.debug('Class/Trigger Name***'+exp.substring(0,exp.indexOF('.')));
            className = exp.substring(0,exp.indexOF('.'));
            
            exp = exp.substring(exp.indexOF('.')+1);
            System.debug('exp3***'+exp);
            System.debug('Method***'+exp.substring(0,exp.indexOF(':')));
            methodName = exp.substring(0,exp.indexOF(':'));
            
            exp = exp.substring(exp.indexOF(':')+1);
            System.debug('Line***'+exp);
            lineNumber = exp;
            
            String emailBody  = 'Below are the details of exception occured : \n\n';
            emailBody+='Exception Message : '+message;
            emailBody+='\nClass Name : '+className;
            emailBody+='\nMethod Name : '+methodName;
            emailBody+='\nLine Number : '+lineNumber;
            
            //Send an email telling exception handling code failed
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(new String[] { Label.ErrorAlertEmail });
            email.setSubject('Exception occured in '+className+' class\n');
            email.setPlainTextBody(emailBody);
            List<Messaging.SendEmailResult> results = 
            Messaging.sendEmail(new Messaging.Email[] { email });
            System.debug('results ****'+results );
        }
        catch(Exception ex){
            //Send an email telling exception handling code failed
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(new String[] { 'chitiz24sep@gmail.com' });
            email.setSubject('Exception handling failure');
            email.setPlainTextBody('Error message : '+ex.getStackTraceString());
            List<Messaging.SendEmailResult> results = 
            Messaging.sendEmail(new Messaging.Email[] { email });
        }
    }
    
    public static testMethod void testErrorLog(){
        String step = 'ABC';
        //generate an exception to test
        try{
            // This will generate a system type exception that is used in the test.
            Date myDate = date.valueOf('100-100-100');
        }
        catch(Exception e) {
            e.setMessage('test');
            Error_Log.logError(e);
        }
        //Test loggin w/o exception
        Error_Log.logError(null);
       
    }
}