public Class ChecklistTriggerNavigator{
    List<Checklist__c> newRecords;
    List<Checklist__c> oldRecords;
    //Constructor
    public ChecklistTriggerNavigator(List<Checklist__c> TriggerNewRecords,  List<Checklist__c> TriggerOldRecords, Map<Id,Checklist__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        ChecklistTriggerUtility.setNewRecords(newRecords);
        ChecklistTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void NotifyTeamMembersOnCheklistCreation(){
        ChecklistTriggerUtility.NotifyTeamMembersOnCheklistCreation();
    }
    
    public void CheckAccess(){
        ChecklistTriggerUtility.CheckAccess();
    }
    
}