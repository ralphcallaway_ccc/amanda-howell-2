public  Class ProjectTriggerUtility{
    static List<Project__c> newRecords;
    static List<Project__c> oldRecords;
    static Map<Id,Project__c> oldRecordsMap;
    
    public static void setNewRecords(List<Project__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Project__c> records, Map<Id,Project__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
    
    public static void DeleteTeamMembersOnAccountChange(){
        List<Id> TMToDeleteProjectIds = new List<Id>();
        List<Id> ATMToDeleteProjectIds = new List<Id>();
        List<Our_Team_Member__c> TeamMembersToDelete = new List<Our_Team_Member__c>();
        List<Adversary_Team_Member__c> AdversaryTeamMembersToDelete = new List<Adversary_Team_Member__c>();
        for(Integer i=0;i<newRecords.size();i++){
            if(newRecords[i].Client_Account__c!=oldRecords[i].Client_Account__c && oldRecords[i].Client_Account__c!=null){
                 TMToDeleteProjectIds.add(newRecords[i].id);
                 system.debug('Proj I to delete---' + newRecords[i].id);
            }     
            if(newRecords[i].Adversary_Account__c!=oldRecords[i].Adversary_Account__c && oldRecords[i].Adversary_Account__c!=null){
                 ATMToDeleteProjectIds.add(newRecords[i].id);  
                 system.debug('Proj Id in adv delete---' + newRecords[i].id);
            }              
        }
        
        TeamMembersToDelete = [select id from Our_Team_Member__c where Project__C in: TMToDeleteProjectIds] ;   
        AdversaryTeamMembersToDelete = [select id from Adversary_Team_Member__c  where Project__C in: ATMToDeleteProjectIds];
                
        if(TeamMembersToDelete.size()>0)
            delete TeamMembersToDelete;
        if(AdversaryTeamMembersToDelete.size()>0)
            delete AdversaryTeamMembersToDelete; 
    }
    
    public static void UpdateCollaborationOwnerOnOwnerChange(){
        List<ID> ProjectOwnerChanged = new List<ID>();
        for(Project__c p: newRecords){
            if(p.OwnerId != oldRecordsMap.get(p.Id).OwnerId){
                ProjectOwnerChanged.add(p.Id);
            }
        }
        if(ProjectOwnerChanged.size()>0){
            List<Collaboration__c> collabList = new List<Collaboration__c>();
            for(Collaboration__c objCollab : [select id,Project__c, Project__r.OwnerId from Collaboration__c  where Project__c in:ProjectOwnerChanged]){
               objCollab.OwnerId = objCollab.Project__r.OwnerId; 
               collabList.add(objCollab);
            }
            update collabList ;
        }    
    
    }
    
    public static void UpdateOverallMission(){
        List<Id> UpdatedProjectIds = new List<Id>();
        Map<Id,String> ProjIdMissionMap = new Map<Id,String>();
        List<Checklist__c> CLToUpdate = new List<Checklist__c>();
        
        for(Integer i=0;i<newRecords.size();i++){
            if(newRecords[i].Mission_and_Purpose__c!=oldRecords[i].Mission_and_Purpose__c){
                 UpdatedProjectIds.add(newRecords[i].id);
                 ProjIdMissionMap.put(newRecords[i].id,newRecords[i].Mission_and_Purpose__c);
            }      
        }
    
        //Update related checklists
        for(Checklist__c objCL : [select id,Overall_Mission__c,Project__c from CheckList__c where Project__c in:UpdatedProjectIds]){
            objCL.Overall_Mission__c = ProjIdMissionMap.get(objCL.Project__c);
            CLToUpdate.add(objCL);
        }
        if(CLToUpdate!=null && CLToUpdate.size()>0)
            update CLToUpdate;
            
       
    }
    
     public static void UnsubscribeOnProjectDeactivation(){     
     	Set<Id> deactivatedProjectIds = new Set<Id>();
     	Set<Id> collabIds = new Set<Id>();
     	for(Project__c pr : newRecords){
     		if(!pr.isActive__c && oldRecordsMap.get(pr.Id).isActive__c){
     			deactivatedProjectIds.add(pr.Id);
     		}
     	}
     	
     	if(deactivatedProjectIds.size()>0){
     		for(Collaboration__c collab: [select id from Collaboration__c where Project__c In: deactivatedProjectIds]){
     			collabIds.add(collab.Id);
     		}
     	}
     	
     	if(collabIds.size()>0){
     		Delete [select id from EntitySubscription where ParentId IN :collabIds];
     	}
     }
     
     public static void SubscribeOnProjectActivation(){     
     	Set<Id> activatedProjectIds = new Set<Id>();
     	List<EntitySubscription> ESToInsert = new List<EntitySubscription>();
     	for(Project__c pr : newRecords){
     		if(pr.isActive__c && !oldRecordsMap.get(pr.Id).isActive__c){
     			activatedProjectIds.add(pr.Id);
     		}
     	}
     	
     	if(activatedProjectIds.size()>0){
     		for(Project__c proj: [select id,(select id from Collaborations__r),(select id, User__c from Our_Team_Members__r) from Project__c where Id In: activatedProjectIds]){
     			for(Collaboration__c collab: proj.Collaborations__r){
     				for(Our_Team_Member__c otm: proj.Our_Team_Members__r){
     					EntitySubscription es = new EntitySubscription();
	                    es.ParentId = collab.id;
	                    es.SubscriberId = otm.User__c;
	                    ESToInsert.add(es);
     				}
     			}
     		}
     	}
     	
     	if(ESToInsert.size()>0){
     		Database.insert(ESToInsert, false);
     	}
     }
}