public with sharing class ManageCNMSRolesController {
    public Boolean isAdmin {get;set;}
    public List<CNMS_Role__c> allCNMSRoles {get;set;}              // List holds all the CNMS Roles currently present in the system
    public String roleId {get;set;}
    public String userId {get;set;}
    public List<selectOption> RoleTypeOptions {get;set;}    
    Client_Account__c clientAcc;
    public Map<Id, String> usersOriginalRole {get;set;} 
    
    public ManageCNMSRolesController (){
        isAdmin = true;
        // Check if curent user is admin, then only allow access.      
        if(!CNMSUtility.isAdminUser(UserInfo.getUserId())){
            isAdmin = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, Label.NoAccessPrivilegeMessage));
        } 
        
        //Client Account Prep       
        try{
            clientAcc = [select Id from Client_Account__c where name__c = :UserInfo.getOrganizationName() Limit 1];
        }
        catch(Exception e){
			clientAcc = new Client_Account__c (name__c = UserInfo.getOrganizationName(), IsActive__c = true);           
        }
        
        prepareCNMSRolesList(); // Create the individual lists of Roles/Users/Ids
        createRoleTypeOptions();
    }
    
    private void prepareCNMSRolesList(){
        allCNMSRoles = [select Id, name, Role_Type__c, user__c, user__r.Name from CNMS_Role__c order by user__r.Name]; // Pull all the CNMS Roles records from the system.
        usersOriginalRole = new Map<Id, String>();
        for(CNMS_Role__c role: allCNMSRoles){
            usersOriginalRole.put(role.user__c,role.Role_Type__c);
        }
        if(allCNMSRoles.size()==0){
            allCNMSRoles.add(new CNMS_Role__c(Client_Account__c = clientAcc.Id));
        }
    }
    
    private void createRoleTypeOptions(){
        RoleTypeOptions = new List<selectOption>();
        RoleTypeOptions.add(new SelectOption('all','All'));
        Schema.DescribeSObjectResult R = CNMS_Role__c.SObjectType.getDescribe();        
        map<String, Schema.SObjectField> fieldMap = R.fields.getMap();      
        list<Schema.PicklistEntry> values = fieldMap.get('Role_Type__c').getDescribe().getPickListValues();    
        for(Schema.PicklistEntry a : values){
            RoleTypeOptions.add(new SelectOption(a.getLabel(),a.getValue()));
        }
    }
    
    public PageReference addRole(){
        //Check if role already exist for user
        if(!processLastRow()){
            return null;
        }        
        allCNMSRoles.add(new CNMS_Role__c(Client_Account__c = clientAcc.Id));
        return null;
    }
    
    private boolean processLastRow(){
        Set<Id> existingUsersWithRole = usersOriginalRole.keySet();
        CNMS_Role__c lastRow;
        if(allCNMSRoles.size()!=0){
            lastRow = allCNMSRoles[allCNMSRoles.size()-1];
            if(lastRow.Id==null){ // This is a role added previously, so check for duplicate and process accordingly.
                //Check for duplicate
                if(existingUsersWithRole.contains(lastRow.user__c)){ // User already has some role, provide error.
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'User role already exists'));
                    return false;
                }
                try{
				if(clientAcc.Id==null){
					insert clientAcc ; // insert the client account first.
					lastRow.Client_Account__c = clientAcc.Id;
				}
                insert lastRow;
                usersOriginalRole.put(lastRow.User__c, lastRow.Role_Type__c);
                }
                catch(Exception e){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                }
            }
        }
        return true;
    }
    
    public void removeRole(){
        CNMS_Role__c roleToDelete;
        System.debug('Role to delete Id------->' + roleId);
        if(roleId==null || roleId==''){
            if(allCNMSRoles[allCNMSRoles.size()-1].Id==null){
                allCNMSRoles.remove(allCNMSRoles.size()-1);
            }
            return;
        }   
        System.debug('roleId***'+roleId);
            
        for(Integer i=0; i<allCNMSRoles.size();i++){
            if(allCNMSRoles[i].Id == roleId){
                roleToDelete = allCNMSRoles[i];   
                try{
                    delete roleToDelete;
                    usersOriginalRole.remove(roleToDelete.User__c);
                    allCNMSRoles.remove(i);
                }
                catch(Exception e){
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                    System.debug('Exception is--->' + e.getMessage());
                }
                break;
            }
        }       
    }
    
    public PageReference save(){
        if(!processLastRow()){
            return null;
        }
        
        List<CNMS_Role__c> rolesForUpdate = new List<CNMS_Role__c>();
        for(CNMS_Role__c role: allCNMSRoles){
            if(role.Role_Type__c!=usersOriginalRole.get(role.User__c)){ // Role has been updated for this user, hence update role.
                rolesForUpdate.add(role);               
            }
        }
        
        if(rolesForUpdate.size()>0){
            try{
                update rolesForUpdate;
                for(CNMS_Role__c role: rolesForUpdate){
                    usersOriginalRole.put(role.User__c, role.Role_Type__c); // update the original role of this user
                }               
            }
            catch(Exception e){
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                return null;
            }
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Roles Saved Successfully'));
        PageReference redirectPage = new PageReference('/apex/CNMSRoles?success=true');
        redirectPage.setredirect(true);
        return redirectPage;
    }
    
    public PageReference back(){
        PageReference pr = new PageReference('/apex/CNMSRoles');
        pr.setRedirect(true);        
        return pr;
    } 
}