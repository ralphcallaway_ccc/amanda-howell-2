@isTest
private class TestNewChecklistController {
     public static testMethod void TestNewChecklistController(){  
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id);
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;
       
        User u = createUser('testing@testorg.com', 't');
        u.isTeamMember__c = true;       
        u.isActive = true;
        u.lastname='Testing';
        u.firstname = 'Testing';
        insert u;
            
        Checklist__c cl1 = new Checklist__c (Checklist_Name__c='Test Checklist', Project__c = pro.id);
        insert cl1;
        system.debug('###First Checklist ' + cl1); 
        
        Log__c log = new Log__c(Log_Name__c='Test Log', Project__c = pro.id); // used for passing id in page parameter       
        insert log;
        system.debug('###First Log ' + log); 
        
        PageReference currentPage = new PageReference('/apex/createChecklist');
        currentPage.getParameters().put('projId',pro.id);
        Test.setCurrentPage(currentPage );    
        
          // Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(cl1);
 
        // create the controller
         
        NewChecklistController newchecklistController = new NewChecklistController(sc);   
        
        // Verify an Error is returned as the first checklist 'cl1' doesn't have any log. hence new checklist not allowed.
        //System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        
        // Now create a log and insert it for existing checklist.
        Log__c log2 = new Log__c(Log_Name__c='Test Log 2', Project__c = pro.id, CheckList__c = cl1.Id); // used for passing id in page parameter       
        insert log2;
        system.debug('###Log ' + log2); 
        system.debug('###CL Plus Log ' + cl1); 
        // Also, pass the first log as parameter in page so that it's checklist is updated with new one.
        currentPage.getParameters().put('logId',log.id);
        system.debug('###Current Page ' + currentpage.getparameters()); 
        
       
        
        

        //currentPage.getParameters().put('Id',cl.id); 
        
         //Test  back Method
        PageReference testPage = newChecklistController.back();
        System.assert(testPage.getUrl().contains('ViewProject'));                
        System.assertEquals(pro.id, testPage.getParameters().get('Id'));
        
        //Test  cancel Method
        testPage = newChecklistController.cancel();        
        System.assert(testPage.getUrl().contains('ViewChecklist'));        
        
        //Test  edit Method
        testPage = newChecklistController.edit();        
        System.assert(testPage.getUrl().contains('EditChecklist'));
        
        //Test update method exception        
        testPage = newChecklistController.updateCL();//Should throw exception as new CL cannot be updated.  
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
        //Test save n New
        newChecklistController.checklist.Checklist_Name__c = 'My checklist needs a name';
        testPage = newChecklistController.saveAndNew();
        system.debug('###Save n New TestPage ' + testpage);                 
        List<CheckList__c> newcls = [select Id from CheckList__c where Project__c=:pro.Id];
        system.debug('###1st List newcls should have 2 ' + newcls); 
        System.Assert(newcls.size()==2); // As already there was one CL created under this project.
        //Ensure the Log's CL is assigned with the new created CL.
        Log__c lg = [select CheckList__c from Log__c where id = :log.Id];
        System.Assert(lg.CheckList__c !=null);
        
        // Test save
        newChecklistController.notifyTM = true;
        testPage = newChecklistController.save();                
        newcls = [select Id from CheckList__c where Project__c=:pro.Id];
        system.debug('###2nd List newcls should have 3 ' + newcls); 
        System.Assert(newcls.size()==3);// 2 were already there: one initially, and one created using save n new.            
        
        // Test saveAndCreateLog
        newChecklistController.CheckList =  new Checklist__c (Project__c = pro.id);
        testPage = newChecklistController.saveAndCreateLog();  
        newcls = [select Id from CheckList__c where Project__c=:pro.Id];
        system.debug('###3rd List newcls should have 4 ' + newcls); 
        System.Assert(newcls.size()==4);// 3 were already there: one initially, and 2 created using save n new and save.
        System.assert(testPage.getUrl().contains('createLog'));
        
        //Test various Save methods in order to cover exception
        testPage = newChecklistController.save(); // Exception as existing CL cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        testPage = newChecklistController.saveAndNew(); // Exception as existing CL cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        testPage = newChecklistController.saveAndCreateLog(); // Exception as existing CL cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
       // Test Update Method
        String mission = 'Test Mission';
        newChecklistController.CheckList.overall_Mission__c = mission ;
        testPage = newChecklistController.updateCL();        
        System.assert(testPage.getUrl().contains('ViewChecklist'));
        CheckList__c updatedCL = [select overall_Mission__c from CheckList__c where id= :newChecklistController.CheckList.Id];
        System.Assert(updatedCL.overall_Mission__c == mission );
        
         //Test  createLog Method
        testPage = newChecklistController.createLog();
        System.assert(testPage.getUrl().contains('createLog'));                
        System.assertEquals(pro.id, testPage.getParameters().get('projId'));
        
        // Test controller with CL id is passed in page.
        currentPage = new PageReference('/apex/ViewChecklist');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('clId',cl1.id);
        Test.setCurrentPage(currentPage ); 
        newChecklistController = new NewChecklistController(sc);
        System.assert(newChecklistController.logs.size()>0);
        System.assert(newChecklistController.logsToDisplay.size()>0);
        
        testPage = newChecklistController.getCollaborationData();
        System.assert(newChecklistController.problemsCollabId !=null);
        System.assert(newChecklistController.ourBaggageCollabId !=null);
        System.assert(newChecklistController.adversaryBaggageCollabId !=null);
        System.assert(newChecklistController.changesAdversaryCollabId !=null);
        System.assert(newChecklistController.criticalResearchCollabId !=null);
        System.assert(newChecklistController.currentMissionCollabId !=null);
        //System.assert(newChecklistController.datePreparedCollabId !=null);
       // System.assert(newChecklistController.projectCollabId !=null);
        System.assert(newChecklistController.teamActivityCollabId !=null);
        System.assert(newChecklistController.teamBehaviorCollabId !=null);
        System.assert(newChecklistController.whatHappensNextCollabId !=null);
        System.assert(newChecklistController.whatWeWantCollabId !=null);
        
        Our_Team_Member__c otm1 = new Our_Team_Member__c(User__c =u.Id, project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm1;  // to cover collab sharing on otm isertion.
    }
    
    private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}