public with sharing class NewLogController {
    public Log__c log {get; set;}
    public Checklist__c relatedCL{get; set;}
    public Project__c project {get; set;}
    public String projId {get; set;}
    public String id {get; set;}
    public String clId {get; set;}
    public String source {get; set;}
    public Boolean hasEditAccess {get;set;}
    public Boolean hasViewAccess {get;set;}
    public String AccessLevel {get;set;}
    public Boolean NotifyTM {get;set;}
    //Collaboration Ids
    //public Id checklistCollabId {get; set;}
    //public Id dateCreatedCollabId {get; set;}
    public Id discussCollabId {get; set;}
    public Id emotionCollabId {get; set;}
    public Id energyCollabId {get; set;}
    public Id moneyCollabId {get; set;}
    public Id potentialCollabId {get; set;}
    //public Id projectCollabId {get; set;}
    //public Id timeCollabId {get; set;}
    public Id whoIsCollabId {get; set;}
    public Id yourThoughtsCollabId {get; set;}
    public Boolean isObserver {get; set;}
    public Integer existingCLCount {get; set;}
    public Integer existingLogCount {get; set;}
    public NewLogController(ApexPages.StandardController controller) {       
        projId = ApexPages.currentPage().getParameters().get('projId');
        id = ApexPages.currentPage().getParameters().get('id');
        clId = ApexPages.currentPage().getParameters().get('clId');
        source = ApexPages.currentPage().getParameters().get('source');
        existingCLCount=0;
        existingLogCount=0;
         hasEditAccess = false;
         hasViewAccess = false;
         isObserver = false;
         NotifyTM  = false;
        log = new log__c(Project__c=projId, Checklist__c = clId);
        
        try{
            if(id!=null && id!=''){
                log = [select id,Project__c,name, Project__r.name,Checklist__c,Checklist__r.Name,Checklist__r.Checklist_Name__c, Date_Created__c,Who_is__c,Log_Name__c,Time__c,Energy__c,Money__c,Emotion__c,Discuss__c,Your_Thoughts__c,Potential_Agenda__c from log__c where id=:id];
            }
            if(clId!=null && clId!=''){
                relatedCL = [select id,Name from Checklist__c where id=:clId];
            }
            if(projId!=null && projId!=''){
                project = [select name,id,Adversary_Account__r.isActive__c from project__c where id=:projId];
                // Ensure account is active, else give error.
                if(!project.Adversary_Account__r.isActive__c){
                    hasEditAccess = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, Label.AdversaryAccountInactiveError)); 
                    return;
                }
                // get existing log count for this project
                existingLogCount = [select count() from Log__c where Project__c = :projId];
                // get existing checlist count for this project
                existingCLCount = [select count() from Checklist__c where Project__c = :projId];
               
            }
        }
        catch(Exception e){
            hasViewAccess = false;
            return;
        }
        AccessLevel  = CNMSUtility.getCurrentUserProjectAccessLevel(projId);
        
            for(Our_Team_Member__c otm: [select User__c, Role__c from Our_Team_Member__c where Project__c =: projId]){
                if(otm.User__c == UserInfo.getUserId()){
                    hasViewAccess = true;
                    if(AccessLevel =='Edit' || AccessLevel =='All'){
                        hasEditAccess = true;
                    }
                    isObserver = otm.Role__c == Label.Observer;
                    break;
                }
            }                
        
    }
    
    public PageReference back(){
        PageReference redirectPage = new PageReference('/apex/ViewProject?Id='+projId);
        return redirectPage;
    }
    
    public PageReference viewPDF(){
        PageReference redirectPage = new PageReference('/apex/PrintableLog?projId='+projId+'&id='+id);
        return redirectPage;
    }
    
    public PageReference cancel(){
        PageReference redirectPage = new PageReference('/apex/ViewLog?id='+id+'&projId='+projId);
        return redirectPage;
    }
    
    public PageReference edit(){
        PageReference redirectPage = new PageReference('/apex/EditLog');
        source='';
        return redirectPage;
    }
    
    private void sendNewLogMail(){
        String name = [select name from Log__c where id = :log.Id Limit 1].Name;
        CNMSUtility.sendLogEmail(log.id, log.Project__c, name , false);
    }
    
    public PageReference save(){
        try{
            insert log;
            if(notifyTM){
                sendNewLogMail();
            }
            PageReference redirectPage = new PageReference('/apex/ViewLog?id='+log.Id+'&projId='+projId);
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public PageReference saveAndNew(){
        try{
            insert log;
            if(notifyTM){
                sendNewLogMail();
            }
            log = new Log__c(Project__c=projId,Date_Created__c=Date.Today());
            if(clId!=null){
                log.CheckList__c = clId ;
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }
        return null;
    }
    
    public PageReference saveAndCreateCheckList(){
        try{
            insert log;
            if(notifyTM){
                sendNewLogMail();
            }
            if(log.Checklist__c != null){
                return new PageReference('/apex/createChecklist?projId=' + project.Id + '&source=proj');
            }
            return new PageReference('/apex/createChecklist?projId=' + project.Id +'&logId=' + log.Id + '&source=proj');
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public PageReference CreateCheckList(){
        try{            
            if(log.Checklist__c != null){
                return new PageReference('/apex/createChecklist?projId=' + project.Id + '&source=proj');
            }
            return new PageReference('/apex/createChecklist?projId=' + project.Id +'&logId=' + log.Id + '&source=proj');
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public PageReference updateLog() {
        try{
            update log;
            if(notifyTM){
                CNMSUtility.sendLogEmail(log.id, log.Project__c, log.name, true);
            }
            PageReference redirectPage = new PageReference('/apex/ViewLog?id='+id+'&projId='+projId);
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    } 
    
    public PageReference getCollaborationData(){
        Map<String,Id> FieldNameCollabIdMap;
        try{
            FieldNameCollabIdMap = new CollaborationUtil().createLogCollaboration(projId,Id);
        }
        catch(Exception e){
            hasViewAccess = false;
            return null;
        }
        for(String fieldName : FieldNameCollabIdMap.keySet()){
            //if(fieldName=='Checklist')
            //    checklistCollabId=  FieldNameCollabIdMap.get(fieldName); 
            //if(fieldName=='Date Created')
            //    dateCreatedCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Discuss')
                discussCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Emotion')
                emotionCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Energy')
                energyCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Money')
                moneyCollabId =  FieldNameCollabIdMap.get(fieldName);    
            if(fieldName=='Potential Agenda')
                potentialCollabId =  FieldNameCollabIdMap.get(fieldName);    
            //if(fieldName=='Project')
            //    projectCollabId =  FieldNameCollabIdMap.get(fieldName);
            //if(fieldName=='Time')
            //    timeCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Who is')
                whoIsCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Your Thoughts')
                yourThoughtsCollabId =  FieldNameCollabIdMap.get(fieldName);
        }
        return null;
    }
}