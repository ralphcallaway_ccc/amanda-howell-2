public  Class OTMTriggerUtility{
    static List<Our_Team_Member__c> newRecords;
    static List<Our_Team_Member__c> oldRecords;
	
    public static void setNewRecords(List<Our_Team_Member__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Our_Team_Member__c> records){
        oldRecords = records;
    }
    
    public static void PopulateProjectLead(){
        Map<Id,Our_Team_Member__c> ProjIdOTM = new Map<Id,Our_Team_Member__c>();
        Map<Id,Id> OldProjIdOTMId = new Map<Id,Id>();
        
        for(Our_Team_Member__c objOTM : newRecords){
            if(objOTM.Project_Lead__c)
                ProjIdOTM.put(objOTM.Project__c,objOTM);
        }
        System.debug('ProjIdOTM***'+ProjIdOTM);
        
        if(ProjIdOTM!=null && ProjIdOTM.size()>0){
            List<Project__c> projectsToUpdate = new List<Project__c>();
            for(Project__c proj : [select Project_Lead__c from Project__c where id in:ProjIdOTM.keySet() AND Adversary_Account__r.isActive__c = true]){
                proj.Project_Lead__c = ProjIdOTM.get(proj.Id).Id;
                proj.ownerId = ProjIdOTM.get(proj.Id).user__c; // change the project owner to corresponding user.
                projectsToUpdate.add(proj);
            }
            if(projectsToUpdate!=null && projectsToUpdate.size()>0)
                update projectsToUpdate;
                System.debug('Project Lead updated---->');
        }
    }
    
    public static void AssignChatterPermissions(){
        Map<Id,Id> OTMIdProjIdToAdd = new Map<Id,Id>();
        Map<Id,Id> OTMIdUserId = new Map<Id,Id>();
        Set<Id> ProjectIds = new Set<Id>();
        List<Id> CollabIds = new List<Id>();
        Map<Id,Id> CollabIdProjId = new Map<Id,Id>();
        Map<Id,List<Id>> ProjIdCollabIdList = new Map<Id,List<Id>>();
        List<EntitySubscription> ESToInsert = new List<EntitySubscription>();
        List<EntitySubscription> ESToInsertList = new List<EntitySubscription>();
        EntitySubscription es;
        
        for(Our_Team_Member__c objOTM : newRecords){
           // if(objOTM.Role__c != Label.Observer){     // skip observer        
                OTMIdProjIdToAdd.put(objOTM.id,objOTM.Project__c);
                ProjectIds.add(objOTM.Project__c);
                OTMIdUserId.put(objOTM.id,objOTM.User__c);  
           // } 
        }        
        
        for(Collaboration__c objCollab : [select id,Project__c from Collaboration__c  where Project__c in:ProjectIds]){
            CollabIds.add(objCollab.id);
            CollabIdProjId.put(objCollab.id,objCollab.Project__c);
            if(ProjIdCollabIdList.containsKey(objCollab.Project__c)){
                collabIds = ProjIdCollabIdList.get(objCollab.Project__c);
                collabIds.add(objCollab.id);
                ProjIdCollabIdList.put(objCollab.Project__c,collabIds);
            }
            else
               ProjIdCollabIdList.put(objCollab.Project__c,new List<Id>{objCollab.id});
        }
        
        System.debug('ProjIdCollabIdList****'+ProjIdCollabIdList);
        System.debug('OTMIdProjIdToAdd****'+OTMIdProjIdToAdd);      
        //Add access to existing collaboration for new members
        for(Id OTMId : OTMIdProjIdToAdd.keySet()){
            for(Id projId : ProjIdCollabIdList.keySet()){
                if(OTMIdProjIdToAdd.get(OTMId)==projId){
                    for(Id collabId : ProjIdCollabIdList.get(projId)){
                        es = new EntitySubscription();
                        es.SubscriberId = OTMIdUserId.get(OTMId);
                        es.ParentId = collabId;
                        ESToInsert.add(es);
                    } 
                }
            }
        }       
        System.debug('ESToInsert****'+ESToInsert);      
        if(ESToInsert.size()>0 && !Test.IsRunningTest()){
				ESToInsertList.addAll(ESToInsert);
				System.debug('ESToInsert size****'+ESToInsertList.size());
				Database.insert(ESToInsertList,false);
        	/*if(ESToInsert.size()<10000){
	            ESToInsertList.addAll(ESToInsert);
	            System.debug('ESToInsert size****'+ESToInsert.size());
	            Database.insert(ESToInsertList,false);
        	}
        	else{
        		Integer totalIteration = Integer.valueOf(Math.Ceil(ESToInsert.size()/10000));
        		for(Integer i=0; i<totalIteration ;i++){
        			List<EntitySubscription> TempESToInsert = new List<EntitySubscription>();
        			Integer recordToProcess = 10000;
        			if(i==totalIteration-1){ // this is the last iteration
        				recordToProcess = ESToInsert.size()-10000*i;
        			}
        			for(Integer j = 0; j<recordToProcess ; j++ ){
        				TempESToInsert.add(ESToInsert[i*10000 +j]);
        			}
        			Database.insert(TempESToInsert,false);
        		}
        	}*/
        }
    }
    
    public static void DeleteChatterPermissions(){
        Map<Id,Id> OTMIdProjIdToRemove = new Map<Id,Id>();
        Map<Id,Id> OTMIdUserId = new Map<Id,Id>();
        Set<Id> ProjectIds = new Set<Id>();
        List<Id> CollabIds = new List<Id>();
        Map<Id,Id> CollabIdProjId = new Map<Id,Id>();
        Map<Id,List<Id>> ProjIdCollabIdList = new Map<Id,List<Id>>();
        List<EntitySubscription> ESToDelete = new List<EntitySubscription>();

        for(Our_Team_Member__c objOTM : oldRecords){
           // if(objOTM.Role__c != Label.Observer){  // skip observer  
                OTMIdProjIdToRemove.put(objOTM.id,objOTM.Project__c);
                ProjectIds.add(objOTM.Project__c);  
                OTMIdUserId.put(objOTM.id,objOTM.User__c);
          //  }
        }   
        
        for(Collaboration__c objCollab : [select id,Project__c from Collaboration__c  where Project__c in:ProjectIds]){
            CollabIds.add(objCollab.id);
            CollabIdProjId.put(objCollab.id,objCollab.Project__c);
            if(ProjIdCollabIdList.containsKey(objCollab.Project__c)){
                collabIds = ProjIdCollabIdList.get(objCollab.Project__c);
                collabIds.add(objCollab.id);
                ProjIdCollabIdList.put(objCollab.Project__c,collabIds);
            }
            else
               ProjIdCollabIdList.put(objCollab.Project__c,new List<Id>{objCollab.id});
        }
            
        Map<Id,Id> CollabIdSubscriberId = new Map<Id,Id>();
        //Remove access to existing collaboration for members who have been removed
        for(Id OTMId : OTMIdProjIdToRemove.keySet()){
            for(Id projId : ProjIdCollabIdList.keySet()){
                if(OTMIdProjIdToRemove.get(OTMId)==projId){
                    for(Id collabId : ProjIdCollabIdList.get(projId)){
                        CollabIdSubscriberId.put(collabId,OTMIdUserId.get(OTMId));
                    } 
                }
            }
        }
        
        if(CollabIdSubscriberId.size()>0){
            for(EntitySubscription objES : [select id,SubscriberId,ParentId from EntitySubscription where ParentId in: CollabIdSubscriberId.keySet()]){
                if(CollabIdSubscriberId.get(objES.ParentId)==objES.SubscriberId)
                    ESToDelete.add(objES);
            }
        }
        
        if(ESToDelete.size()>0)
            delete ESToDelete;
    }
    
    public static void updateSharingAccess(){
        List<ID> UserIdList = new List<ID>();
        List<ID> ProjectIdList = new List<ID>();
        List<Id> collabIdList = new List<Id>();
        Set<ID> AccountIdList = new Set<ID>();		
        List<Project__Share> ProjectSharingforUpsert = new List<Project__Share>();
        List<Collaboration__Share> CollabSharingforUpsert = new List<Collaboration__Share>();
        List<Adversary_Account__Share> AccountSharingforUpsert = new List<Adversary_Account__Share>();
        Map<Id, Set<Our_Team_Member__c>> otmOnOtherProjects = new Map<Id, Set<Our_Team_Member__c>> (); // Actually all otms (in current trigger plus)  on other projects
        Map<Id, List<Id>> projectIdCollabListMapping = new Map<Id, List<Id>> ();
        Map<Id, Id> projectIdAccountIdMapping = new Map<Id, Id> ();
        //Prepare user and Project id list
        for(Our_Team_Member__c objOTM : newRecords){
            // Consider only if OTM has project assigned.
            if(objOTM.Project__c!=null){
                ProjectIdList.add(objOTM.Project__c);
                UserIdList.add(objOTM.User__c);
            }           
        }
        //Prepare collab id list.
        for(Collaboration__c objCollab : [select id,Project__c from Collaboration__c  where Project__c in:ProjectIdList]){
            collabIdList.add(objCollab.Id);
            if(projectIdCollabListMapping.containsKey(objCollab.Project__c)){
                projectIdCollabListMapping.get(objCollab.Project__c).add(objCollab.Id);
            }
            else{
                projectIdCollabListMapping.put(objCollab.Project__c,new List<Id>{objCollab.id});
            }
        }
        //Prepare Adversary account list.
        for(Project__c pr: [select id, Adversary_Account__c from Project__C where Id IN :ProjectIdList]){
            if(pr.Adversary_Account__c!=null){
                AccountIdList.add(pr.Adversary_Account__c);
                projectIdAccountIdMapping.put(pr.Id, pr.Adversary_Account__c);
            }
        }
		
		// Retrive all OTMs for these users, arrange them by user ID.
		for(Our_Team_Member__c objOTM : [select id, User__c, Access__c, Project__r.Adversary_Account__c from Our_Team_Member__c where User__c IN :UserIdList AND Project__r.Adversary_Account__c IN: AccountIdList]){
			Set<Our_Team_Member__c> accs = otmOnOtherProjects.get(objOTM.User__c);
			if(accs==null){
				accs = new Set<Our_Team_Member__c>();
				accs.add(objOTM);
				otmOnOtherProjects.put(objOTM.User__c, accs);
			}
			else{
				accs.add(objOTM);
			}
		}
		
		// Calculate Maximum Access for each user on each account.
		Map<Id, Map<Id,String>> maximumAccessOfUser= new Map<Id, Map<Id,String>>(); // Map<UserID, Map<AccountId, Access>>
		for(Id userId :otmOnOtherProjects.keySet()){
			Set<Our_Team_Member__c> objOTMs = otmOnOtherProjects.get(userId);
			Map<Id,String> accountAccess = new Map<Id,String>();
			for(Our_Team_Member__c otm: objOTMs){
				String thisOTMAccess = otm.Access__c;
				if(Trigger.NewMap.containsKey(otm.Id)){ // to retrieve latest access of the OTM
					thisOTMAccess = ((Our_Team_Member__c)Trigger.NewMap.get(otm.Id)).Access__c;
					System.debug('picked new access for user-->'+ otm.User__c + '--access--'+ thisOTMAccess);
				}
				if(thisOTMAccess=='View'){ // Convert view to read
					thisOTMAccess = 'Read';
				}
				String presentMaxAccess = accountAccess.get(otm.Project__r.Adversary_Account__c);
				if(presentMaxAccess==null){
					accountAccess.put(otm.Project__r.Adversary_Account__c, thisOTMAccess);
				}
				else {
					if(presentMaxAccess=='Read' && thisOTMAccess !='Read'){ // present Max is read, but found higher than read, then assign higher access as Max
						accountAccess.put(otm.Project__r.Adversary_Account__c, 'Edit');
					}
				}
			}
			maximumAccessOfUser.put(userId,accountAccess);			
		}
        
        // retrieve existing sharing for these users for these projects.
        List<Project__Share> existingProjectSharing = [select id, ParentId, UserOrGroupId , AccessLevel from Project__Share where  UserOrGroupId IN:UserIdList AND ParentId IN :ProjectIdList];
        List<Collaboration__Share> existingCollabSharing = [select id, ParentId, UserOrGroupId , AccessLevel from Collaboration__Share where  UserOrGroupId IN:UserIdList AND ParentId IN :collabIdList];
        List<Adversary_Account__Share> existingAccountSharing = [select id, ParentId, UserOrGroupId , AccessLevel from Adversary_Account__Share where  UserOrGroupId IN:UserIdList AND ParentId IN :AccountIdList];
        
		// Optimization
		Map<String, Project__Share> userIdParentIdProjectSharingMap = new Map<String, Project__Share>(); // format is Map<UserOrGroupId+ParentId,Project__Share >
		Map<String, Collaboration__Share> userIdParentIdCollaborationSharingMap = new Map<String, Collaboration__Share>(); // format is Map<UserOrGroupId+ParentId,Collaboration__Share >
		for(Project__Share ps: existingProjectSharing){
			userIdParentIdProjectSharingMap.put(ps.UserOrGroupId+''+ps.ParentId, ps);
		}
		for(Collaboration__Share cs: existingCollabSharing){
			userIdParentIdCollaborationSharingMap.put(cs.UserOrGroupId+''+cs.ParentId, cs);
		}
		
		// Loop through OTMs to create various sharings.
        for(Our_Team_Member__c objOTM : newRecords){
            // Consider only if OTM has project assigned.
            if(objOTM.Project__c!=null){
                String Access = objOTM.Access__c;
                                
                if(Access==null || Access ==''){
                    Access = 'None';
                }                
                // Covert View  to Read
                if(objOTM.Access__c=='View'){
                    Access = 'Read';
                }
                System.debug('Access for this OTM is---->' + Access);
                if(Access !='All'){      // set permissions only when it's not owner for project and collab.                              
                    // 1. PROJECT SHARING LOGIC START
                    //Check if already project sharing exist
                    Boolean alreadyProjectSharingExist = false;
					/*
                    for(Project__Share pr: existingProjectSharing){
                        if(pr.UserOrGroupId == objOTM.User__c && pr.ParentId == objOTM.Project__c){
                            //Alrady sharing exist
                            alreadyProjectSharingExist = true;
                            pr.AccessLevel = Access;                      
                            ProjectSharingforUpsert.add(pr);
                            break; // break this loop
                        }
                    }*/
					//Optimized code
					Project__Share pr = userIdParentIdProjectSharingMap.get(objOTM.User__c+''+objOTM.Project__c);
					if(pr!=null){
						//Alrady sharing exist
						alreadyProjectSharingExist = true;
						pr.AccessLevel = Access;                      
						ProjectSharingforUpsert.add(pr);
					}
                    if(!alreadyProjectSharingExist){
                    // create a new sharing
                        Project__Share newPr = new Project__Share(UserOrGroupId = objOTM.User__c, ParentId = objOTM.Project__c, AccessLevel = Access);
                        ProjectSharingforUpsert.add(newPr);
                    }
                    // PROJECT SHARING LOGIC END
                    
                    // 2. COLLABORATION SHARING LOGIC START
                    if(collabIdList.size()>0){
                        List<Id> collabIDsforThisProject = projectIdCollabListMapping.get(objOTM.Project__c);
                        if(collabIDsforThisProject!=null && collabIDsforThisProject.size()>0){
                            for(Id colabId: collabIDsforThisProject){
                                //Check if already Collab sharing exist
                                Boolean alreadyCollabSharingExist = false;
                                /*for(Collaboration__Share cs: existingCollabSharing){
                                    if(cs.UserOrGroupId == objOTM.User__c && cs.ParentId == colabId){
                                        //Alrady sharing exist
                                        alreadyCollabSharingExist = true;
                                        cs.AccessLevel = Access;
                                        CollabSharingforUpsert.add(cs);
                                        break; // break this loop
                                    }
                                }*/
								// Optimized code
								Collaboration__Share cs = userIdParentIdCollaborationSharingMap.get(objOTM.User__c+''+colabId);
								if(cs!=null){
									//Alrady sharing exist
									alreadyCollabSharingExist = true;
									cs.AccessLevel = Access;
									CollabSharingforUpsert.add(cs);
								}
                                if(!alreadyCollabSharingExist){
                                // create a new sharing
                                    Collaboration__Share newCS = new Collaboration__Share(UserOrGroupId = objOTM.User__c, ParentId = colabId, AccessLevel = Access);
                                    CollabSharingforUpsert.add(newCS);
                                }
                            }
                        }
                    }
                    // 2. COLLABORATION SHARING LOGIC END
                }
                // 3. ACCOUNT SHARING LOGIC START
                if(AccountIdList.size()>0){
                    /*//Check if already Account sharing exist
                    Boolean alreadyAccountSharingExist = false;
                    for(Adversary_Account__Share aar: existingAccountSharing){
                        if(aar.UserOrGroupId == objOTM.User__c && aar.ParentId==projectIdAccountIdMapping.get(objOTM.Project__c)){
                            //Already sharing exist
                            alreadyAccountSharingExist = true;
                            if(aar.AccessLevel == 'Read' && (Access == 'Edit' || Access == 'All')){
                                aar.AccessLevel = 'Edit';
                                AccountSharingforUpsert.add(aar);
                            }    
                            break; // break this loop
                        }
                    }
                    if(!alreadyAccountSharingExist && projectIdAccountIdMapping.get(objOTM.Project__c) !=null){
                    // create a new sharing
                        Adversary_Account__Share newPr = new Adversary_Account__Share(UserOrGroupId = objOTM.User__c, ParentId = projectIdAccountIdMapping.get(objOTM.Project__c), AccessLevel = Access);
                        AccountSharingforUpsert.add(newPr);
                    }*/
					
					// new code
					//Check if already Account sharing exist
                    Boolean alreadyAccountSharingExist = false;
                    for(Adversary_Account__Share aar: existingAccountSharing){
                        if(aar.UserOrGroupId == objOTM.User__c && aar.parentId==projectIdAccountIdMapping.get(objOTM.Project__c)){
                            //Already sharing exist
                            alreadyAccountSharingExist = true;
							String MaxAccess = null;
							Map<Id,String> accountAccess  = maximumAccessOfUser.get(objOTM.User__c );
							if(accountAccess !=null){
								MaxAccess = accountAccess.get(aar.parentId);
								System.debug('Max access for OTM--->' +objOTM.User__c + '---and Account-->' + aar.parentId + '--- is ---' + MaxAccess);
							}							
                            if(aar.AccessLevel != 'Read' && MaxAccess == 'Read'){ // degrade access to read
                                aar.AccessLevel = 'Read';
                                AccountSharingforUpsert.add(aar);
                                System.debug(' access downgraded for OTM--->' +objOTM.User__c + '---and Account-->' + aar.parentId + '---degraded access is ---' + MaxAccess);
                            } 
							else if(aar.AccessLevel == 'Read' && MaxAccess != 'Read'){ // upgrade access to edit
                                aar.AccessLevel = 'Edit';
                                AccountSharingforUpsert.add(aar);
                                System.debug('upgraded access for OTM--->' +objOTM.User__c + '---and Account-->' + aar.parentId + '--- upgraded access is ---' + MaxAccess);
                            } 							
                            break; // break this loop
                        }
                    } 
					if(!alreadyAccountSharingExist && projectIdAccountIdMapping.get(objOTM.Project__c) !=null){
                    // create a new sharing
                        Adversary_Account__Share newPr = new Adversary_Account__Share(UserOrGroupId = objOTM.User__c, ParentId = projectIdAccountIdMapping.get(objOTM.Project__c), AccessLevel = Access);
                        AccountSharingforUpsert.add(newPr);
                        System.debug('creating new access for OTM--->' +objOTM.User__c + '---and Account-->' + newPr.parentId + '--access is ---' + Access);
                    }					
                }
                 // 3. ACCOUNT SHARING LOGIC END
            }           
        }
        if(ProjectSharingforUpsert.size()>0 && !Test.IsRunningTest()){
        	System.debug('Project share to upsert size****'+ProjectSharingforUpsert.size());
			Database.upsert(ProjectSharingforUpsert, false);
			/*if(ProjectSharingforUpsert.size()<10000){
				System.debug('Total records being upserted are------->' + ProjectSharingforUpsert.size());
	            Database.upsert(ProjectSharingforUpsert, false);
        	}
        	else{
        		Integer totalIteration = Integer.valueOf(Math.Ceil(ProjectSharingforUpsert.size()/10000));
        		for(Integer i=0; i<totalIteration ;i++){
        			List<Project__Share> TempPSToInsert = new List<Project__Share>();
        			Integer recordToProcess = 10000;
        			if(i==totalIteration-1){ // this is the last iteration
        				recordToProcess = ProjectSharingforUpsert.size()-10000*i;
        			}
        			for(Integer j = 0; j<recordToProcess ; j++ ){
        				TempPSToInsert.add(ProjectSharingforUpsert[i*10000 +j]);
        			}
        			Database.upsert(TempPSToInsert,false);
        		}
        	}*/
            
		}
        if(CollabSharingforUpsert.size()>0 && !Test.IsRunningTest()){
            System.debug('Collab share to upsert size****'+CollabSharingforUpsert.size());
			Database.upsert(CollabSharingforUpsert, false);
            /*
			if(CollabSharingforUpsert.size()<10000){
	            Database.upsert(CollabSharingforUpsert, false);
        	}
        	else{
        		Integer totalIteration = Integer.valueOf(Math.Ceil(CollabSharingforUpsert.size()/10000));
        		for(Integer i=0; i<totalIteration ;i++){
        			List<Collaboration__Share> TempPSToInsert = new List<Collaboration__Share>();
        			Integer recordToProcess = 10000;
        			if(i==totalIteration-1){ // this is the last iteration
        				recordToProcess = CollabSharingforUpsert.size()-10000*i;
        			}
        			for(Integer j = 0; j<recordToProcess ; j++ ){
        				TempPSToInsert.add(CollabSharingforUpsert[i*10000 +j]);
        			}
        			Database.upsert(TempPSToInsert,false);
        		}
        	}*/
		}	
        if(AccountSharingforUpsert.size()>0 && !Test.IsRunningTest()){
            System.debug('Account share to upsert size****'+AccountSharingforUpsert.size());
			Database.upsert(AccountSharingforUpsert, false);
			/*if(AccountSharingforUpsert.size()<10000){
	            Database.upsert(AccountSharingforUpsert, false);
        	}
        	else{
        		Integer totalIteration = Integer.valueOf(Math.Ceil(AccountSharingforUpsert.size()/10000));
        		for(Integer i=0; i<totalIteration ;i++){
        			List<Adversary_Account__Share> TempPSToInsert = new List<Adversary_Account__Share>();
        			Integer recordToProcess = 10000;
        			if(i==totalIteration-1){ // this is the last iteration
        				recordToProcess = AccountSharingforUpsert.size()-10000*i;
        			}
        			for(Integer j = 0; j<recordToProcess ; j++ ){
        				TempPSToInsert.add(AccountSharingforUpsert[i*10000 +j]);
        			}
        			Database.upsert(TempPSToInsert,false);
        		}
        	}*/
		}	
    }
    
    public static void deleteSharingAccess(){
        List<ID> UserIdList = new List<ID>();
        List<ID> ProjectIdList = new List<ID>();
        List<Id> collabIdList = new List<Id>();
        Set<Id> ProjectSharingToDelete = new Set<Id>(); //Project__Share
        Set<Id> CollabSharingToDelete = new Set<Id>(); //Collaboration__Share>      
        Map<Id, List<Id>> projectIdCollabListMapping = new Map<Id, List<Id>> ();
		Map<Id, Set<Our_Team_Member__c>> otmOnOtherProjects = new Map<Id, Set<Our_Team_Member__c>> ();
		Set<ID> accountIds = new Set<ID>();
		Map<Id, Id> projectIdAccountIdMapping = new Map<Id, Id> ();
		List<Adversary_Account__Share> AccountSharingforUpdate = new List<Adversary_Account__Share>();
		Set<Id> AccountSharingforDelete = new Set<Id>(); //Adversary_Account__Share>();
		
        //Prepare user and Project id list
        for(Our_Team_Member__c objOTM : oldRecords){
            // Consider only if OTM has project assigned.
            if(objOTM.Project__c!=null){
                ProjectIdList.add(objOTM.Project__c);
                UserIdList.add(objOTM.User__c);
            }           
        }
        //Prepare collab id list.
        for(Collaboration__c objCollab : [select id,Project__c from Collaboration__c  where Project__c in:ProjectIdList]){
            collabIdList.add(objCollab.Id);
            if(projectIdCollabListMapping.containsKey(objCollab.Project__c)){
                projectIdCollabListMapping.get(objCollab.Project__c).add(objCollab.Id);
            }
            else{
                projectIdCollabListMapping.put(objCollab.Project__c,new List<Id>{objCollab.id});
            }
        }     
		
		for(Project__c pr: [select id, Adversary_Account__c from Project__c where id IN:ProjectIdList ]){
			accountIds.add(pr.Adversary_Account__c);
			projectIdAccountIdMapping.put(pr.Id, pr.Adversary_Account__c);
		}
		// Retrive all other OTMs for these users with Edit access, arrange them by user ID.
		for(Our_Team_Member__c objOTM : [select id, User__c, Access__c, Project__r.Adversary_Account__c from Our_Team_Member__c where User__c IN :UserIdList AND Project__r.Adversary_Account__c IN: accountIds AND Project__c NOT IN :ProjectIdList]){
			Set<Our_Team_Member__c> accs = otmOnOtherProjects.get(objOTM.User__c);
			if(accs==null){
				accs = new Set<Our_Team_Member__c>();
				accs.add(objOTM);
				otmOnOtherProjects.put(objOTM.User__c, accs);
			}
			else{
				accs.add(objOTM);
			}
		}
		
		// Calculate Maximum Access remaining for each user on each account.
		Map<Id, Map<Id,String>> maximumAccessOfUser= new Map<Id, Map<Id,String>>(); // Map<UserID, Map<AccountId, Access>>
		for(Id userId :otmOnOtherProjects.keySet()){
			Set<Our_Team_Member__c> objOTMs = otmOnOtherProjects.get(userId);
			Map<Id,String> accountAccess = new Map<Id,String>();
			for(Our_Team_Member__c otm: objOTMs){
				String access = otm.Access__c;
				if(access == 'View'){ // convert view to read;
					access = 'Read';
				}
				String presentMaxAccess = accountAccess.get(otm.Project__r.Adversary_Account__c);
				if(presentMaxAccess==null){
					accountAccess.put(otm.Project__r.Adversary_Account__c, access);
				}
				else {
					if(presentMaxAccess=='Read' && access !='Read'){ // present Max is read, but found higher than read, then assign higher access as Max
						accountAccess.put(otm.Project__r.Adversary_Account__c, 'Edit');
					}
				}
			}
			maximumAccessOfUser.put(userId,accountAccess);			
		}
        // retrieve existing sharing for these users for these projects.
        List<Project__Share> existingProjectSharing = [select id, ParentId, UserOrGroupId , AccessLevel from Project__Share where  UserOrGroupId IN:UserIdList AND ParentId IN :ProjectIdList];
        List<Collaboration__Share> existingCollabSharing = [select id, ParentId, UserOrGroupId , AccessLevel from Collaboration__Share where  UserOrGroupId IN:UserIdList AND ParentId IN :collabIdList];
        List<Adversary_Account__Share> existingAccountSharing = [select Id, parentId, AccessLevel, UserOrGroupId from Adversary_Account__Share where UserOrGroupId IN:UserIdList and parentId IN : accountIds ];
		
		Map<String, Project__Share> userIdParentIdProjectSharingMap = new Map<String, Project__Share>(); // format is Map<UserOrGroupId+ParentId,Project__Share >
		Map<String, Collaboration__Share> userIdParentIdCollaborationSharingMap = new Map<String, Collaboration__Share>(); // format is Map<UserOrGroupId+ParentId,Collaboration__Share >
		for(Project__Share ps: existingProjectSharing){
			userIdParentIdProjectSharingMap.put(ps.UserOrGroupId+''+ps.ParentId, ps);
		}
		for(Collaboration__Share cs: existingCollabSharing){
			userIdParentIdCollaborationSharingMap.put(cs.UserOrGroupId+''+cs.ParentId, cs);
		}
		// Loop through OTMs delete existing sharings.
        for(Our_Team_Member__c objOTM : oldRecords){
            // Consider only if OTM has project assigned.
            if(objOTM.Project__c!=null){                
                // 1. PROJECT SHARING LOGIC START 
				/*	
                for(Project__Share pr: existingProjectSharing){
                    if(pr.UserOrGroupId == objOTM.User__c && pr.ParentId == objOTM.Project__c){                                  
                        ProjectSharingToDelete.add(pr.Id);
                        break; // break this loop
                    }
                } */
				// Optimized code
				Project__Share pr = userIdParentIdProjectSharingMap.get(objOTM.User__c+''+objOTM.Project__c);
				if(pr!=null){
					ProjectSharingToDelete.add(pr.Id);
				}
                // PROJECT SHARING LOGIC END
                
                // 2. COLLABORATION SHARING LOGIC START
                if(collabIdList.size()>0){
                    List<Id> collabIDsforThisProject = projectIdCollabListMapping.get(objOTM.Project__c);
                    if(collabIDsforThisProject!=null && collabIDsforThisProject.size()>0){
                        for(Id colabId: collabIDsforThisProject){
							Collaboration__Share tempCS = userIdParentIdCollaborationSharingMap.get(objOTM.User__c+''+colabId);
                            /*for(Collaboration__Share cs: existingCollabSharing){
                                if(cs.UserOrGroupId == objOTM.User__c && cs.ParentId == colabId){                                   
                                    CollabSharingToDelete.add(cs.Id);
                                    break; // break this loop
                                }
                            }  */
							// Optimized code
							if(tempCS!=null){
								CollabSharingToDelete.add(tempCS.Id);
							}
                        }
                    }
                }
                // 2. COLLABORATION SHARING LOGIC END   
				// 3. Account Delete logic start
					//Check if already Account sharing exist
                    Boolean alreadyAccountSharingExist = false;
                    for(Adversary_Account__Share aar: existingAccountSharing){
                        if(aar.UserOrGroupId == objOTM.User__c && aar.parentId==projectIdAccountIdMapping.get(objOTM.Project__c)){
                            //Already sharing exist
                            alreadyAccountSharingExist = true;
							String MaxAccess = null;
							Map<Id,String> accountAccess  = maximumAccessOfUser.get(objOTM.User__c );
							if(accountAccess !=null){
								MaxAccess = accountAccess.get(projectIdAccountIdMapping.get(objOTM.Project__c));
							}
							if(MaxAccess==null){ // No more access
								AccountSharingforDelete.add(aar.Id);
							}
                            else if(aar.AccessLevel != 'Read' && MaxAccess == 'Read'){ // degrade access to read
                                aar.AccessLevel = 'Read';
                                AccountSharingforUpdate.add(aar);
                            }    
                            break; // break this loop
                        }
                    }                   
				// 3. Account Delete logic End
            }           
        }
        if(ProjectSharingToDelete.size()>0)
            Database.delete([select id from Project__Share where id In :ProjectSharingToDelete], false);
        if(CollabSharingToDelete.size()>0)
            Database.delete([select id from Collaboration__Share where id In :CollabSharingToDelete],false);
		if(AccountSharingforUpdate.size()>0)
			Database.Update(AccountSharingforUpdate,false);
		if(AccountSharingforDelete.size()>0)
			Database.delete([select id from Adversary_Account__Share where id in :AccountSharingforDelete],false);
    
    }
    
}