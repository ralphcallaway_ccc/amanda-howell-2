public with sharing class AllContactsController{
    public Adversary_Account__c account {get; set;}
    public Adversary_Contact__c contact {get; set;}
    public String accId {get; set;}
    public String contId {get; set;}
    public String projId {get; set;}
    public String source {get; set;}
    public List<Adversary_Contact__c> contactsList {get; set;}
    public List<Adversary_Contact__c> contactsToDisplay {get; set;}
    public List<Adversary_Contact__c> inactiveContactsList {get; set;}
    public List<Adversary_Contact__c> inactiveContactsToDisplay {get; set;}
   // public List<Adversary_Account__c> activeDuplicateAccountList {get; set;}
   // public List<Adversary_Account__c> inactiveDuplicateAccountList {get; set;}
    public List<Project__c> projectsToDisplay {get; set;}
    public List<Project__c> projectList {get; set;}
   // public boolean continueWithNewAccountCreation {get;set;}
    public boolean showContacts {get; set;}
    public boolean showInactiveContacts {get; set;}
    public boolean showProjects {get; set;}
    public boolean showAllContacts {get; set;}
    public boolean showAllInactiveContacts {get; set;}
    public boolean showAllProjects {get; set;}
    public Integer contactsCount {get; set;}
    public Integer inactiveContactsCount {get; set;}
    public Integer projectsCount {get; set;}
    //public String multiple = '';
    public String projIdToDeActivate {get;set;} 
    public String contIdToDeActivate {get;set;}
    public String contIdToActivate {get;set;}     
    public boolean accountCreationAllowed {get; set;}  
    public Boolean hasEditAccess {get;set;}
    public String AccessLevel {get;set;}  
   /* public boolean getActiveDuplicateExist() {
        return activeDuplicateAccountList !=null && activeDuplicateAccountList.size()>0 ? true:false;
    }
    
    public boolean getInactiveDuplicateExist() {
        return inactiveDuplicateAccountList !=null && inactiveDuplicateAccountList.size()>0 ? true:false;
    }
    */
    
    public AllContactsController(){
        Integer numberOfRecords = 10;
        showContacts = false;
        showAllContacts = false;
        accountCreationAllowed  = true;
        hasEditAccess = false;
        account = new Adversary_Account__c(isActive__c=true);
        accId = ApexPages.currentPage().getParameters().get('accId');
        contId = ApexPages.currentPage().getParameters().get('contId');
        projId = ApexPages.currentPage().getParameters().get('projId');
        //continueWithNewAccountCreation = false;
        if(accId!=null && accId!=''){
           account = [select Name__c,OwnerId,Phone__c,Fax__c,Website__c,isActive__c, access__c from Adversary_Account__c where id=:accId]; 
           contactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=true order by Name__c];
           inactiveContactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=false order by Name__c];
           //Get all related projects
           projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c from Project__c where isActive__c=true and Adversary_Account__c=:accId order by Name];
           
            //Create contacts to display list
            createContactsToDisplayList();

            //Create inactive contacts to display list
            createInactiveContactsToDisplayList();
            
                
                //Prepare projects to display list
                projectsToDisplay = new List<Project__c>();
                if(projectList !=null && projectList.size()>0){
                    showProjects = true;
                    projectsCount = projectList.size();
                    if(projectsCount <=numberOfRecords)
                            numberOfRecords = projectsCount ;
                    else
                        showAllProjects = true;
                    for(Integer i=0;i<numberOfRecords;i++){
                        projectsToDisplay.add(projectList[i]);
                    }
                }
                
                List<CNMS_Role__c> currentUserRole = CNMSUtility.getCurrentUserRole();
                if(currentUserRole.size()>0){
                    String roleType = currentUserRole[0].Role_Type__c;
                    if(roleType ==Label.CEO){
                        hasEditAccess = true;
                    }
                    else if(roleType ==Label.TeamMember || roleType == Label.Coach){
                        //AccessLevel  = CNMSUtility.getCurrentUserAccountAccessLevel(accId);                        
                        for(Our_Team_Member__c otm : [select User__c, Access__c from Our_Team_Member__c where Project__r.Adversary_Account__c =:accId AND User__c = :UserInfo.getUserId()]){                            
                            if(otm.Access__c == 'Edit' || otm.Access__c == 'All'){
                                hasEditAccess = true;
                                break;
                            }
                            
                        }                
                        
                    }
                }  
           }
           else{
               List<CNMS_Role__c> currentUserRole = CNMSUtility.getCurrentUserRole();
               if(currentUserRole == null || currentUserRole.size()==0 || currentUserRole[0].Client_Account__c ==null || currentUserRole[0].Role_Type__c == Label.Coach){                    
                    account = null;
                    accountCreationAllowed = false;
                   // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.NoActionPrivilegeMessage)); 
               }
           }
        }    
    private void createContactsToDisplayList(){
        Integer numberOfRecords = 10;
        showContacts = false;
        if(contactsList!=null && contactsList.size()>0){
                showContacts = true;
                contactsCount = contactsList.size();
                if(contactsCount<=numberOfRecords)
                        numberOfRecords = contactsCount;
                else
                    showAllContacts = true;
                contactsToDisplay = new List<Adversary_Contact__c>();
                for(Integer i=0;i<numberOfRecords;i++){
                    contactsToDisplay.add(contactsList[i]);
                }
            } 
    }
    
    public void createInactiveContactsToDisplayList(){
        Integer numberOfRecords = 10;
        showInactiveContacts = false;
        if(inactiveContactsList!=null && inactiveContactsList.size()>0){
            showInactiveContacts = true;
            inactiveContactsCount = inactivecontactsList.size();
            if(inactiveContactsCount<=numberOfRecords)
                    numberOfRecords = inactiveContactsCount;
            else
                showAllInactiveContacts = true;
            inactiveContactsToDisplay = new List<Adversary_Contact__c>();
            for(Integer i=0;i<numberOfRecords;i++){
                inactiveContactsToDisplay.add(inactiveContactsList[i]);
            }
        } 
    }
    public PageReference back(){
        if(projId!=null)
            return new PageReference('/apex/ViewProject?projId='+projId);
        PageReference home = new PageReference('/apex/ProjectList');
        return home;
    }
    
    public PageReference cancel(){
        PageReference redirectPage = new PageReference('/apex/ViewAccount?Id='+accId);
        return redirectPage;
    }
    
    public PageReference editAccount(){
        PageReference redirectPage = new PageReference('/apex/EditAccount?accId='+accId);
        return redirectPage;
    }
    
    public PageReference editContact(){
        contId = ApexPages.currentPage().getParameters().get('contId');
        PageReference redirectPage = new PageReference('/apex/EditContact?contId='+contId);
        return redirectPage;
    }
    
    public PageReference viewContact(){
        contId = ApexPages.currentPage().getParameters().get('contId');
        PageReference redirectPage = new PageReference('/apex/ViewContact?contId='+contId);
        return redirectPage;
    }
    
    public PageReference save(){
        try{
            /*if(!continueWithNewAccountCreation && checkDuplicates()){
                multiple  = '0';
                PageReference redirectPage = new PageReference('/apex/AccountValidation');
                return redirectPage;
            }*/
            List<Adversary_Account__c> duplicateAccounts = [select id from Adversary_Account__c where name__c = :account.Name__c];
            if(duplicateAccounts.size()>0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Account));
                return null;
            }
            insert account;
            PageReference redirectPage = new PageReference('/apex/ViewAccount?accId='+account.id);
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    /*
    public boolean checkDuplicates(){
        try{
            activeDuplicateAccountList = [select id, name__c, isActive__c, phone__c, fax__c, website__c from Adversary_Account__c where name__c = :account.name__c AND isActive__c=true];
            inactiveDuplicateAccountList = [select id, name__c, isActive__c, phone__c, fax__c, website__c from Adversary_Account__c where name__c = :account.name__c AND isActive__c = false];
            
            if((activeDuplicateAccountList != null && activeDuplicateAccountList.size()>0) ||
               (inactiveDuplicateAccountList != null && inactiveDuplicateAccountList.size()>0)){
            return true;
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        return false;
    }*/
    
    public PageReference updateAccount(){
        update account;
        PageReference redirectPage = new PageReference('/apex/ViewAccount?accId='+accId);
        return redirectPage;
    }
    
    public PageReference saveAndNew(){
        try{
            /*if(!continueWithNewAccountCreation && checkDuplicates()){
                 multiple  = '1';
                PageReference redirectPage = new PageReference('/apex/AccountValidation');
                return redirectPage;
            }*/
            List<Adversary_Account__c> duplicateAccounts = [select id from Adversary_Account__c where name__c = :account.Name__c];
            if(duplicateAccounts.size()>0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Account));
                return null;
            }
            insert account;
            account = new Adversary_Account__c(isActive__c=true);
        }
        catch(Exception e) {
            System.debug('Exception while inserting new account--->');
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));            
        }    
        return null;
    }
    
    public PageReference addContact(){
        PageReference redirectPage = new PageReference('/apex/CreateContact?accId='+accId);
        return redirectPage;
    }
    
    public PageReference confirmContactDelete() {
        contId = ApexPages.currentPage().getParameters().get('contId');
        PageReference confirmDelete = new PageReference('/apex/ConfirmContactDelete');
        return confirmDelete;
    }
    
    public PageReference activateContact(){
        try{
            Adversary_Contact__c objCont = [select isActive__c,name__c from Adversary_Contact__c where id=:contIdToActivate]; //ApexPages.currentPage().getParameters().get('contId')];
            objCont.isActive__c = true;
            update objCont;
            contactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=true order by Name__c];
            inactiveContactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=false order by Name__c];
            createContactsToDisplayList();
            createInactiveContactsToDisplayList();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, objCont.name__c + ' ' + Label.ContactActivationMessage));
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }     
        return null;
    }
    
    public PageReference deactivateContact() {
        try{
            contId = contIdToDeActivate ;//ApexPages.currentPage().getParameters().get('contId');
            Adversary_Contact__c contactToUpdate = [select id,isActive__c,name__c from Adversary_Contact__c where id=:contId];
            contactToUpdate.isActive__c = false;
            update contactToUpdate;
            contactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=true order by Name__c];
            inactiveContactsList = [select Name__c,Phone__c,Email__c from Adversary_Contact__c where Adversary_Account__c=:accId and isActive__c=false order by Name__c];
            createContactsToDisplayList();
            createInactiveContactsToDisplayList();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, contactToUpdate.name__c + ' ' + Label.ContactDeactivationMessage));
            /*if(source=='proj'){
                PageReference home = new PageReference('/apex/viewProject?projId='+ApexPages.currentPage().getParameters().get('projId'));
                return home;
            }
            PageReference accList = new PageReference('/apex/viewAccount?source=home&accId='+accId);
            accList.setRedirect(true);*/
            return null ;//accList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    /*
    public pageReference continueSave(){
        try{
            continueWithNewAccountCreation = true;
            if(multiple =='0')        
                return save();
                
            else{
               saveAndNew();
               continueWithNewAccountCreation = false;
               multiple = '';
               return new PageReference('/apex/CreateAccount');
            }
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        
    }
    
    public pageReference activateAccounts(){
        boolean noneSelected = true;
        for(Adversary_Account__c a : inactiveDuplicateAccountList ){
            if(a.isActive__c){
                noneSelected = false;
                break;
            }
        }
        if(noneSelected){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.NoAccountSelectedToActivate));
            return null;
        }
        try{
            update inactiveDuplicateAccountList ;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
        PageReference redirectPage;
        if(multiple =='0')        
            redirectPage = new PageReference('/apex/ProjectList');
            
        else{
            account = new Adversary_Account__c(isActive__c=true);
            continueWithNewAccountCreation = false;
            multiple = '';
            redirectPage = new PageReference('/apex/CreateAccount');
        }
        
        return redirectPage;
    } 
     
     
     public PageReference deactivateProject() {
        try{
            String projId = projIdToDeActivate;// ApexPages.currentPage().getParameters().get('projId');
            Project__c projToDeactivate = [select id,isActive__c,name , Client_Account__c,Client_Account__r.isActive__c,Adversary_Account__c, Adversary_Account__r.isActive__c from Project__c where id=:projId];
            
             if(projToDeactivate.Client_Account__c !=null && !projToDeactivate.Client_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ClientAccountInactiveError));
                return null;
            } 
            if(projToDeactivate.Adversary_Account__c !=null && !projToDeactivate.Adversary_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AdversaryAccountInactiveError));
                return null;
            } 
            projToDeactivate.isActive__c = false;
            update projToDeactivate;
            projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c from Project__c where isActive__c=true and Adversary_Account__c=:accId order by Name];
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, projToDeactivate.name + ' ' + Label.ProjectDeactivationMessage));
            //PageReference projList = new PageReference('/apex/ProjectList');
            //projList.setRedirect(true);
            return null;//projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }*/
}