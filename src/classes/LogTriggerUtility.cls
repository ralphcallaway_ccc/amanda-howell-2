public  Class LogTriggerUtility{
    static List<Log__c> newRecords;
    static List<Log__c> oldRecords;
    static Map<Id,Log__c> oldRecordsMap;
    
    public static void setNewRecords(List<Log__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Log__c> records, Map<Id,Log__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
       
    public static void NotifyTeamMembersOnLogCreation(){
        Map<Id,Id> LogIdProjId = new Map<Id,Id>();
        Map<Id,Log__c> LogIdLog = new Map<Id,Log__c>();
        Map<Id,List<String>> projIdMemberEmails = new Map<Id,List<String>>();
        for(Log__c objLog : newRecords){
            LogIdProjId.put(objLog.id,objLog.Project__c);
            LogIdLog.put(objLog.id,objLog);
        }
                
        List<String> emails = new List<String>();
        //Query al the team members 
        for(Our_Team_Member__c objOTM : [select id,User__r.Name,User__r.Email,Project__c,Project__r.Name from Our_Team_Member__c where Project__c in:LogIdProjId.values()]){
            //emails.clear();
            if(projIdMemberEmails.containsKey(objOTM.project__c)){
                emails = projIdMemberEmails.get(objOTM.project__c);
                emails.add(objOTM.User__r.Email);
                projIdMemberEmails.put(objOTM.project__c,emails);               
            }
            else{
               projIdMemberEmails.put(objOTM.project__c,new List<String>{objOTM.User__r.Email}); 
            }           
            System.debug('projIdMemberEmails in between***'+projIdMemberEmails);  
        }   
        System.debug('projIdMemberEmails***'+projIdMemberEmails);   
        List<Messaging.SingleEmailMessage> SingleEmailMessages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        for(Log__c objLog : LogIdLog.values()){
            //Create separate emails for each Logoration
            mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(projIdMemberEmails.get(LogIdProjId.get(objLog.id)));
            mail.setSubject('New log Created : ' + objLog.Name);
            mail.setPlainTextBody('New log has been created');
            system.debug('mail****'+mail);
            SingleEmailMessages.add(mail);
        }
        //Messaging.sendEmail(SingleEmailMessages); 
    }
    
    public static void CheckAccess(){   
        Set<ID> projectIDs = new Set<ID>();
        Map<Id, String> projectAccessMap;
        for(Log__c log: newRecords){
            projectIDs.add(log.Project__c);
        }
        projectAccessMap = CNMSUtility.getCurrentUserProjectAccessLevel(projectIDs);
        for(Log__c log: newRecords){
            String access = projectAccessMap.get(log.Project__c);
            if(access !='Edit' && access != 'All'){
                log.addError(Label.NoActionPrivilegeMessage);
            }
        }   
    }

}