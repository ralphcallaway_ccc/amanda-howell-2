@isTest
private class TestNewProjectController2 {
   
     public static testMethod void testNewProjectControllerPopupMethods(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        //acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='advesary', isActive__c = true);
        insert advacc;
              
        Project__c pro = new Project__c(name='test',Client_Account__c = acc.id, Adversary_Account__c=advacc.id, isActive__c = true);
        insert pro;
         
        Adversary_Contact__c con =      new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
        Adversary_Contact__c conTest0 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test2',Last_Name__c='test2',Add_To_Team__c=true, Email__c='test@test.com');
        insert con ;
        insert conTest0; 
        
        User u1 = createUser('testing1@testorg.com', 't1');
        u1.isTeamMember__c = true;
        insert u1;
        
        User u2 = createUser('testing2@testorg.com', 't2');
        u2.isTeamMember__c = true;
        insert u2;
        
        // insert roles for the users.
        CNMS_Role__c r1 = new CNMS_Role__c(User__c = u1.Id, Role_Type__c = Label.teamMember);
        insert r1;
        CNMS_Role__c r2 = new CNMS_Role__c(User__c = u2.Id, Role_Type__c = Label.teamMember);
        insert r2;  
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.CEO);
        insert r3;  
        Our_Team_Member__c  oldTm = new Our_Team_Member__c (User__c = u1.Id, Project__c = pro.id, Project_Lead__c=true, Role__c = 'Team Leader', Access__c = 'View');
        insert oldTm;
        
        Adversary_Team_Member__c oldAtm = new Adversary_Team_Member__c (Adversary_Contact__c = con.Id, Project__c = pro.id, Adversary_Lead__c=true);
        insert oldAtm ;
        
        PageReference currentPage  = new PageReference('/apex/ViewProject?Id='+pro.Id + '&accId='+advacc.Id);
        Test.setCurrentPage(currentPage);
        // Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(Pro);
        // create the controller
         
        NewProjectController newProjectController = new NewProjectController(sc);     
        
        //Test  closePopup Method      
        NewProjectController.selectedUser = UserInfo.getUserId();       
        Our_Team_Member__c  selectedTM = new Our_Team_Member__c (User__c = u2.Id, Project__c = pro.id, Role__c = 'Team Leader', Access__c = 'View');
        insert selectedTM;
        newProjectController.selectedMember = selectedTM.Id ;
        newProjectController.closePopup();
        Our_Team_Member__c newSelectedTM= [select id,Project_Lead__c, User__c from Our_Team_Member__c where id=:selectedTM.Id LIMIT 1];        
        System.assert(newSelectedTM.Project_Lead__c);//true
        Our_Team_Member__c oldSelectedTM= [select id,Project_Lead__c from Our_Team_Member__c where id=:oldTM.Id LIMIT 1];        
        System.assert(!oldSelectedTM.Project_Lead__c);//false
        Project__c prj = [select id, Project_Lead__c from Project__c where id = :pro.id];
        //System.assertEquals(selectedTM.Id, prj.Project_Lead__c);
             
        
        //Test  closeAdversaryPopup Method        
        Adversary_Team_Member__c selectedATM = new Adversary_Team_Member__c (Adversary_Contact__c = conTest0.Id, Project__c = pro.id);        
        insert selectedATM;
        newProjectController.selectedAdversaryMember = selectedATM.Id ;
        newProjectController.closeAdversaryPopup();        
        Adversary_Team_Member__c newSelectedATM= [select id,Adversary_Lead__c, Adversary_Contact__c  from Adversary_Team_Member__c where id=:selectedATM.Id LIMIT 1];        
        System.assert(newSelectedATM.Adversary_Lead__c);//true
        Project__c prj1 = [select id, Adversary_Lead__c from Project__c where id = :newProjectController.project.Id];
        System.assertEquals(selectedATM.Id, prj1.Adversary_Lead__c);
        
        //Test  closeSuccess Method
        newProjectController.closeSuccess();        
        System.assert(!newProjectController.updateInProgress);
        
        //Test  showPopup Method
        newProjectController.showPopup();        
        System.assert(!newProjectController.updateInProgress);
        
        //Test  hidePopup Method
        newProjectController.hidePopup();        
        System.assert(newProjectController.updateInProgress);
        
        //Test  getAllMembers Method
        List<SelectOption> options = newProjectController.getMembersForProjectLead();        
        System.assert(options.size()>0);
        
        //Test  getAllAdversaryMembers Method
        List<SelectOption> options1 = newProjectController.getAllAdversaryMembers();        
        System.assert(options1.size()>0);
        
        newProjectController.displayPermissionsPage();
        newProjectController.updateSharing();
        newProjectController.getAllUsers();
        
        // test new project crearion
        currentPage  = new PageReference('/apex/ViewProject?accId=' +advacc.Id);
        Test.setCurrentPage(currentPage);
        newProjectController = new NewProjectController(sc);
        
    }
    
   
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}