@isTest
private class TestSelectTeamMembersController {

  static Profile p;
 
  static {
 
    p = [select id from profile where name='System Administrator'];
 
  }
  
  private static User createUser(String eMail, String alias){ 
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
  }
  
  static testMethod void testUpdateTeamMember() {
    Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
    advacc.isActive__c = true;
    insert advacc ;
    
    List<User> usersList = new List<User>();     
    User u1 = createUser('testing1@testorg.com', 't1');
    u1.isTeamMember__c = true;
    insert u1;
    
   /* Contact c1 = new Contact(firstName = 'test', lastName = 'Testing', related_User__c = u1.Id);
    insert c1; */
    
    Adversary_Contact__c c1 = new Adversary_Contact__c(Adversary_Account__c=advacc.Id,isActive__c = true,First_Name__c='test',Last_Name__c='test');
    insert c1; 
    
    User u2 = createUser('testing2@testorg.com', 't2');
    insert u2;
    usersList.add(u1);
    usersList.add(u2);
    
    ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(usersList);
    SelectTeamMembersController ctmc = new SelectTeamMembersController(ssc);
    
    for(User u: ctmc.TeamMembers){
        u.isTeamMember__c = !u.isTeamMember__c;
    }
    
    ctmc.save();
    
    ctmc.back(); 
  }
 
  
}