public with sharing class NewChecklistController {
    public Checklist__c checklist {get; set;}
    public project__c project{get;set;}
    public String projId {get; set;}
    public String ID {get; set;}
    public String logId {get; set;}
    public String source {get; set;}
    public String prevSource {get; set;}
    public Boolean newCLNotAllowed {get;set;}
    public List<Log__c> logs {get; set;} 
    public List<Log__c> logsToDisplay {get;set;}
    public boolean showL {get; set;}
    public boolean showAllLinkL {get; set;}
    public Integer LCount {get; set;}
    public Boolean hasEditAccess {get;set;}
    public Boolean hasViewAccess {get;set;}
    public String AccessLevel {get;set;}
    public Boolean notifyTM {get;set;}
    //Collaboration Ids
    public Id problemsCollabId {get; set;}
    public Id ourBaggageCollabId {get; set;}
    public Id overallmissionCollabId {get; set;}
    public Id adversaryBaggageCollabId {get; set;}
    public Id changesAdversaryCollabId {get; set;}
    public Id criticalResearchCollabId {get; set;}
    public Id currentMissionCollabId {get; set;}
    //public Id datePreparedCollabId {get; set;}
    //public Id projectCollabId {get; set;}
    public Id teamActivityCollabId {get; set;}
    public Id teamBehaviorCollabId {get; set;}
    public Id whatHappensNextCollabId {get; set;}
    public Id whatWeWantCollabId {get; set;}
    public boolean isObserver {get;set;}
    public NewChecklistController(ApexPages.StandardController controller) {
        projId = ApexPages.currentPage().getParameters().get('projId');
        id = ApexPages.currentPage().getParameters().get('id');
        logId = ApexPages.currentPage().getParameters().get('logId');
        source = ApexPages.currentPage().getParameters().get('source');
        prevSource = ApexPages.currentPage().getParameters().get('prevSource');
        newCLNotAllowed = false;
        showL = false;
        showAllLinkL= false;
        hasEditAccess = false;
        hasViewAccess = false; 
        isObserver = false;
        notifyTM = false;
        if(id==null || id == ''){
            // this is case of creatting a new checkList, hence allow only if other checklists under this project has atleast one log with them.
            List<checklist__c> otherCLList = [select id, name, Checklist_Name__c,(select id from Logs__r) from checklist__c where Project__c = :projId];
            /* if(otherCLList.size()==1){
                if(otherCLList[0].Logs__r.Size()==0){
                    newCLNotAllowed =true; 
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.ChecklistCreationNotAllowed + ' - ' + otherCLList[0].name));                   
                }
            }*/
        }
        try{
            project = [select id, Adversary_Account__r.isActive__c, name,Mission_and_Purpose__c  from Project__c where id = :projId Limit 1];
            // Ensure account is active, else give error.
            if(!project.Adversary_Account__r.isActive__c){
                newCLNotAllowed =true; 
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, Label.AdversaryAccountInactiveError)); 
                return;
            }   
            checklist = new checklist__c(Project__c=projId,overall_Mission__c =  project.Mission_and_Purpose__c);
            if(id!=null && id!=''){
                checklist = [select id,Checklist_Name__c, Project__c,Overall_Mission__c,Changes_To_Adversary__c,Date_Prepared__c,name,
                             Current_Mission__c,Problems__c,Our_Baggage__c,Adversary_Baggage__c,What_We_Want__c,
                             What_Happens_Next__c,Team_Behavior_Goals__c,Team_Activity__c,Critical_Research__c, Project__r.name 
                             from checklist__c where id=:id];
                logs  = [select id, name, Date_Created__c from Log__c where  checklist__c = :id];
                populateLogListToBeDisplayed(parseInteger(Label.LogsToBeDisplayed));    
                
            }
        }
        catch(Exception e){
            hasViewAccess = false;
            return;
        }    
        
        AccessLevel  = CNMSUtility.getCurrentUserProjectAccessLevel(projId);
        
        for(Our_Team_Member__c otm: [select User__c, Role__c from Our_Team_Member__c where Project__c =: projId]){
            if(otm.User__c == UserInfo.getUserId()){
                hasViewAccess = true;
                if(AccessLevel =='Edit' || AccessLevel =='All'){
                    hasEditAccess = true;
                }
                isObserver = otm.Role__c==Label.Observer;               
                break;
            }
        }                
        
        
    }
    
    /*
    * This method populates the Logs list which is to be displayed on the  page.
    */
    private void populateLogListToBeDisplayed(Integer numberOfLRecords){        
            //Populate logs to be displayed
            if(logs!=null && logs.size()>0){
                showL = true;
                LCount = logs.size();
                if(LCount<=numberOfLRecords)
                        numberOfLRecords = LCount;
                else
                    showAllLinkL = true;
                logsToDisplay = new List<Log__c>();
                for(Integer i=0;i<numberOfLRecords;i++){
                    logsToDisplay.add(logs[i]);
                }
            }
    }
    
    /*
    This method converts an stirng to the equivalant integer. returns default 10, In case of exception.
    */
    private Integer parseInteger(String stringNumber){
        Integer IntNumber;
        try{
            IntNumber = Integer.valueOf(stringNumber);
        
        }
        catch(Exception e){
            IntNumber = 10;
        }
        return IntNumber;
    }
    
    public PageReference back(){
        PageReference redirectPage = new PageReference('/apex/ViewProject?Id='+projId);
        return redirectPage;
    }
    
    public PageReference viewPDF(){
        PageReference redirectPage = new PageReference('/apex/PrintableChecklist?projId='+projId+'&id='+id);
        return redirectPage;
    }
    
    public PageReference cancel(){
        PageReference redirectPage = new PageReference('/apex/ViewChecklist?projId='+projId+'&id='+id);  
        return redirectPage;
    }
    
    public PageReference edit(){
        PageReference redirectPage = new PageReference('/apex/EditChecklist');
        source='';
        return redirectPage;
    }
    private void sendNewCLMail(){
        String name = [select name from Checklist__c where id = :checklist.Id Limit 1].Name;
        CNMSUtility.sendChecklistEmail(checklist.Id, checklist.Project__c, name , false);
    }
    public PageReference save(){
        try{ 
            insert checklist;
            if(notifyTM){
                sendNewCLMail();
            }
            System.debug('Checklist Created susccessfully--->');
            updateLog(); 
            PageReference redirectPage = new PageReference('/apex/ViewChecklist?projId='+projId+'&id='+checklist.id);
            redirectPage.setRedirect(true);
            return redirectPage;
        }
        catch(Exception e) {
            System.debug('Exception while creatin checklist--->' + e);
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    
    public PageReference saveAndNew(){
        try{
            insert checklist;
            if(notifyTM){
                sendNewCLMail();
            }
            updateLog(); 
            checklist = new checklist__c(Checklist_Name__c=project.name, Project__c=projId,Date_Prepared__c=System.Today(),overall_Mission__c =  project.Mission_and_Purpose__c);
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));            
        }
        ApexPages.CurrentPage().setRedirect (true);
        return ApexPages.CurrentPage();
    }
    
    public PageReference saveAndCreateLog(){
        try{
            insert checklist;   
            if(notifyTM){
                sendNewCLMail();
            }
            updateLog();   
            return new PageReference('/apex/createLog?projId=' + projId + '&clId=' + checklist.Id + '&source=proj');
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    }
    private void updateLog(){
        if(logId!=null){
            try{
                Log__c log = [select id, checklist__c from Log__c where id = :logId Limit 1];
                if(log.checklist__c == null){
                    log.checklist__c = checklist.Id;
                    update log;
                }
            }
            catch(Exception e) {
                Error_Log.logError(e);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
                return;
            }    
        }
    }
    public PageReference updateCL() {
        try{
            update checklist;
            if(notifyTM){
                CNMSUtility.sendChecklistEmail(checklist.Id, checklist.Project__c, checklist.Name, true);
            }            
            PageReference redirectPage = new PageReference('/apex/ViewChecklist?projId='+projId+'&id='+id); 
            return redirectPage;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }    
    } 
    
    /*
    * This method redirect user to the create Log page.
    */
    public PageReference createLog(){
        PageReference redirectPage = new PageReference('/apex/createLog?projId='+projId + '&clId=' + Id + '&source=new');
        redirectPage.setRedirect(true);
        return redirectPage;
    } 
    
    public PageReference getCollaborationData(){
        Map<String,Id> FieldNameCollabIdMap ; 
        try{
            FieldNameCollabIdMap = new CollaborationUtil().createChecklistCollaboration(projId,id);
        }
        catch(Exception e){
            hasViewAccess = false;
            return null;
        }
        for(String fieldName : FieldNameCollabIdMap.keySet()){
            if(fieldName=='Problems')
                problemsCollabId =  FieldNameCollabIdMap.get(fieldName); 
            if(fieldName=='Our Baggage')
                ourBaggageCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Adversary Baggage')
                adversaryBaggageCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Overall Mission')
                overallmissionCollabId=  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Changes To Adversary')
                changesAdversaryCollabId=  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Critical Research')
                criticalResearchCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Current Mission')
                currentMissionCollabId =  FieldNameCollabIdMap.get(fieldName);    
           // if(fieldName=='Date Prepared')
            //    datePreparedCollabId =  FieldNameCollabIdMap.get(fieldName);    
            //if(fieldName=='Project')
            //    projectCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Team Activity')
                teamActivityCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='Team Behavior Goals')
                teamBehaviorCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='What Happens Next')
                whatHappensNextCollabId =  FieldNameCollabIdMap.get(fieldName);
            if(fieldName=='What We Want')
                whatWeWantCollabId =  FieldNameCollabIdMap.get(fieldName); 
        }
        return null;
    }
}