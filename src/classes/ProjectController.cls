public with sharing class ProjectController{
    public String projId {get; set;}
    public String accId {get; set;}
    public List<Project__c> projectList{get; set;}
    public List<Project__c> inactiveProjectList{get; set;}
    public List<Project__c> inactiveProjectListAccessible{get; set;}
    public List<Project__c> projectListAccessible{get; set;}
    public List<Project__c> projectListToDisplay{get; set;}
    public List<Adversary_Account__c> adversaryAccList{get; set;}
    public List<Adversary_Account__c> inactiveAdversaryAccList{get; set;}
    public List<Adversary_Account__c> adversaryAccListAccessible{get; set;}
    public List<Adversary_Account__c> adversaryAccListToDisplay{get; set;}
    public List<User> UsersList {get; set;}
    public List<User> UsersListToDisplay {get; set;}
    public String ourAccId {get; set;}
    public boolean showAllLink {get; set;}
    public boolean showAllLinkAdvAcc {get; set;}
    public boolean showAllLinkProj {get; set;}
    public boolean showOurMembers {get; set;}
    public boolean showAdvAccounts {get; set;}
    public boolean showInactiveAdvAccounts {get; set;}
    public boolean showProjects {get; set;}
    public boolean showInactiveProjects {get; set;}
    public Integer contactsCount {get; set;}
    public Integer usersCount {get; set;}
    public Integer advAccCount {get; set;}
    public Integer projectsCount {get; set;}
    public Account companyAccount {get; set;}
    public Integer numberOfRecords = 10;
    public String projIdToActivate {get; set;}
    public String projIdToDeActivate {get; set;}
    public String accIdToDeActivate {get; set;}
    public String accIdToActivate {get; set;}
    public Boolean isAdmin {get;set;}
    public String currentUserRole {get;set;}
    public boolean projectCreationNotAllowed {get;set;}
    public ProjectController(){
        String orgName = UserInfo.getOrganizationName();
        //Get logged in user's client account
        try{
            ourAccId = [select Client_Account__c from User where id=:UserInfo.getUserId()].Client_Account__c;
            }
        catch(Exception e){
            
        }    
        isAdmin = false;
        usersListToDisplay  = new List<User>();
        projectListToDisplay  = new List<Project__c>();
        projectListAccessible = new List<Project__c>();
        inactiveProjectListAccessible = new List<Project__c>();
        adversaryAccListToDisplay= new List<Adversary_Account__c>();
        adversaryAccListAccessible = new List<Adversary_Account__c>();
        projectList = [select Name, Account__c, Opportunity__c, AP_Value__c, Closed_Reason__c, Last_Activity_Date__c, Project_Status__c, Start_Date__c, Client_Account__r.Name__c,Adversary_Account__r.Name__c,Adversary_Account__r.isActive__c,Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c, Access__c from Project__c where isActive__c=true order by Name];
        inactiveProjectList = [select Name, Account__c, Opportunity__c, AP_Value__c, Closed_Reason__c, Last_Activity_Date__c, Project_Status__c, Start_Date__c,Client_Account__r.Name__c,Adversary_Account__r.Name__c, Adversary_Account__r.isActive__c, Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c, Access__c from Project__c where isActive__c=false];
        adversaryAccList = [select Name__c,Phone__c,Fax__c,Website__c, access__c from Adversary_Account__c where isActive__c=true];
        inactiveAdversaryAccList = [select Name__c,Phone__c,Fax__c,Website__c, access__c from Adversary_Account__c where isActive__c=false];
        currentUserRole = null;
        projectCreationNotAllowed = false;
        List<CNMS_Role__c> userRole =  CNMSUtility.getCurrentUserRole();    
        if(userRole.size()>0){
            currentUserRole = userRole[0].Role_Type__c;
        }
        if(currentUserRole==null || currentUserRole==Label.Coach){
            projectCreationNotAllowed = true;
        }
        //contactsList = [select Name,Phone,Email from Contact where accountId=:ourAccId and isActive__c = true order by name];
        
        /*
        usersList = [select Name,Phone,Email from User where isTeamMember__c = true order by name];
        
        
        if(usersList!=null && usersList.size()>0){
            showOurMembers = true;
            usersCount = usersList.size();
            if(usersCount<=numberOfRecords)
                    numberOfRecords = usersCount;
            else
                showAllLink = true;
            for(Integer i=0;i<numberOfRecords;i++){
                usersListToDisplay.add(usersList[i]);
            }
        }*/
        if(CNMSUtility.isAdminUser(UserInfo.getUserId())){
            isAdmin = true;
        }    
        //createProjectListToDisplay
        createProjectListToDisplay();
        createAdvAccListToDisplay();
        if(inactiveProjectList!=null && inactiveProjectList.size()>0){           
            createInactiveProjectListAccessible(); //inactiveProjectListAccessible
        }   
        if(inactiveAdversaryAccList!=null && inactiveAdversaryAccList.size()>0){
            //showInactiveAdvAccounts = true;
            createInactiveAdvAccListAccessible();   
        }
          
    }
    
    private void createProjectListToDisplay(){
        numberOfRecords = 10;
        projectListAccessible.clear();
        projectListToDisplay.clear();
        if(projectList !=null && projectList.size()>0){
           
            Set<Id> ProjectIds = new Set<Id>();
            for(Project__c pr: projectList){
                ProjectIds.add(pr.Id);
                //projectListAccessible.add(pr);
            }
            Map<Id, String> projectAccessMap = CNMSUtility.getCurrentUserProjectAccessLevel(ProjectIds );
            for(Project__c pr: projectList){
                String access = projectAccessMap.get(pr.Id);
                pr.Access__c = access;
                if(!pr.Adversary_Account__r.isActive__c){
                    pr.Access__c = 'Read';
                }
                if(access == 'All' || access=='Edit' || access == 'Read')
                    projectListAccessible.add(pr);
            }
                   
            projectsCount = projectListAccessible.size();
            if(projectsCount >0)showProjects = true;
            if(projectsCount <=numberOfRecords)
                    numberOfRecords = projectsCount ;
            else
                showAllLinkProj = true;
            for(Integer i=0;i<numberOfRecords;i++){
                projectListToDisplay.add(projectListAccessible[i]);
            }
        } 
    }
    
    private void createInactiveProjectListAccessible(){        
        inactiveProjectListAccessible.clear();        
        if(inactiveProjectList !=null && inactiveProjectList.size()>0){
           
            Set<Id> ProjectIds = new Set<Id>();
            for(Project__c pr: inactiveProjectList){
                ProjectIds.add(pr.Id);               
            }
            Map<Id, String> projectAccessMap = CNMSUtility.getCurrentUserProjectAccessLevel(ProjectIds);
            for(Project__c pr: inactiveProjectList){
                String access = projectAccessMap.get(pr.Id);                
                if(currentUserRole == Label.CEO)access = 'All';
                pr.Access__c = access;
                if(access == 'All' || access=='Edit' || access == 'Read'){                    
                    if(!pr.Adversary_Account__r.isActive__c){
                        pr.Access__c = 'Read' ; // do not allow reactivation if account is inactive.
                    }
                    inactiveProjectListAccessible.add(pr);
                    
                }
            } 
            if(inactiveProjectListAccessible.size()>0){
                showInactiveProjects = true;
            }          
        } 
    }
    
    
    private void createAdvAccListToDisplay(){
        numberOfRecords = 10;      
        adversaryAccListToDisplay.clear();
        adversaryAccListAccessible.clear();
        if(adversaryAccList !=null && adversaryAccList.size()>0){ 
            Set<Id> AccountIds = new Set<Id>();
            for(Adversary_Account__c aa: adversaryAccList){
                AccountIds.add(aa.Id);
            }
            Map<Id, String> AccountAccessMap = new Map<Id, String>();//CNMSUtility.getCurrentUserAccountAccessLevel(AccountIds );
            List<Our_Team_Member__c> otms = [select User__c, Access__c, Project__r.Adversary_Account__c from Our_Team_Member__c where Project__r.Adversary_Account__c IN :AccountIds AND User__c = :UserInfo.getUserId()];
            for(Our_Team_Member__c otm: otms){
                String oldAccess = AccountAccessMap.get(otm.Project__r.Adversary_Account__c);
                if(oldAccess ==null){
                    AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Add access for this account // do nothing, already access is All
                } 
                else if(oldAccess =='Read' && otm.Access__c != 'Read'){
                     AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Update access for this account
                }  
                else if(oldAccess =='Edit' && otm.Access__c == 'All'){
                     AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Update access for this account
                }                                                 
            }
            for(Adversary_Account__c aa: adversaryAccList){                
                String access = AccountAccessMap.get(aa.Id);
                if(access!=null){
                    aa.Access__c = access;  
                    adversaryAccListAccessible.add(aa);
                }
            }
                       
            if(currentUserRole!=null){  // a role is present for this user, hence display all accounts.
                showAdvAccounts = true;
                advAccCount = adversaryAccListAccessible.size();
                if(advAccCount <=numberOfRecords)
                        numberOfRecords = advAccCount ;
                else
                    showAllLinkAdvAcc = true;
                for(Integer i=0;i<numberOfRecords;i++){
                    adversaryAccListToDisplay.add(adversaryAccListAccessible[i]);
                }
            }   
        }
    }
    
    private void createInactiveAdvAccListAccessible(){      
        /*
        if(inactiveAdversaryAccList !=null && inactiveAdversaryAccList.size()>0){ 
            Set<Id> AccountIds = new Set<Id>();           
            if(currentUserRole==Label.CEO){
                for(Adversary_Account__c aa: inactiveAdversaryAccList){
                    aa.Access__c = 'All';             
                }
                return;
            }
           
            for(Adversary_Account__c aa: inactiveAdversaryAccList){
                AccountIds.add(aa.Id);               
            }            
            
            List<Project__c> projectsOfInactiveAcounts = [select Id, OwnerId, Adversary_Account__c from Project__c where Adversary_Account__c IN :AccountIds AND OwnerId = :UserInfo.getUserId()];
            
            //Map<Id, String> AccountAccessMap = CNMSUtility.getCurrentUserAccountAccessLevel(AccountIds );
            for(Adversary_Account__c aa: inactiveAdversaryAccList){
                for(Project__c pr: projectsOfInactiveAcounts ){
                    if(aa.Id == pr.Adversary_Account__c){
                        //String access = AccountAccessMap.get(aa.Id);
                        aa.Access__c = 'All';
                        break; 
                    }
                }               
            }             
        }  */

        //##################################3
        List<Adversary_Account__c> inactiveAdversaryAccListAccessible = new List<Adversary_Account__c>();
        if(inactiveAdversaryAccList !=null && inactiveAdversaryAccList.size()>0){ 
            Set<Id> AccountIds = new Set<Id>();
            for(Adversary_Account__c aa: inactiveAdversaryAccList){
                AccountIds.add(aa.Id);
            }
            Map<Id, String> AccountAccessMap = new Map<Id, String>();//CNMSUtility.getCurrentUserAccountAccessLevel(AccountIds );
            List<Our_Team_Member__c> otms = [select User__c, Access__c, Project__r.Adversary_Account__c from Our_Team_Member__c where Project__r.Adversary_Account__c IN :AccountIds AND User__c = :UserInfo.getUserId()];
            for(Our_Team_Member__c otm: otms){
                String oldAccess = AccountAccessMap.get(otm.Project__r.Adversary_Account__c);
                if(oldAccess ==null){
                    AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Add access for this account // do nothing, already access is All
                } 
                else if(oldAccess =='Read' && otm.Access__c != 'Read'){
                     AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Update access for this account
                }  
                else if(oldAccess =='Edit' && otm.Access__c == 'All'){
                     AccountAccessMap.put(otm.Project__r.Adversary_Account__c,otm.Access__c); //Update access for this account
                }                                                 
            }
            for(Adversary_Account__c aa: inactiveAdversaryAccList){                
                String access = AccountAccessMap.get(aa.Id);
                if(access!=null){
                    aa.Access__c = access;  
                    inactiveAdversaryAccListAccessible.add(aa);
                }
            }
                       
            if(currentUserRole!=null){  // a role is present for this user, hence display all accounts.
                inactiveAdversaryAccList = inactiveAdversaryAccListAccessible;
                showInactiveAdvAccounts = inactiveAdversaryAccList.size()>0;
            }   
        }
    }
    public PageReference createNewProject() {
        PageReference createProject = new PageReference('/apex/InitiateProject');
        return createProject;
    }
    
     public PageReference createNewProjectAll() {
        PageReference createProject = new PageReference('/apex/InitiateProject?source=all');
        return createProject;
    }
    
    public PageReference createNewAdversaryAccount() {
        PageReference createAccount = new PageReference('/apex/CreateAccount?source=home');
        return createAccount;
    }
    
     public PageReference createNewAdversaryAccountAll() {
        PageReference createAccount = new PageReference('/apex/CreateAccount?source=all');
        return createAccount;
     }

    public PageReference editProject() {
        String projId = ApexPages.currentPage().getParameters().get('projId');
        System.debug('projId ****'+projId);
        PageReference editProject = new PageReference('/apex/EditProject?projId='+projId);
        editProject.setRedirect(true);
        return editProject;
    }
    
    public PageReference editAdversaryAccount() {
        accId = ApexPages.currentPage().getParameters().get('accId');
        PageReference editAccount = new PageReference('/apex/EditAccount?accId='+accId);
        editAccount.setRedirect(true);
        return editAccount;
    }
    
    public PageReference viewProject() {
        String projId = ApexPages.currentPage().getParameters().get('projId'); 
        PageReference viewProject = new PageReference('/apex/ViewProject?projId='+projId);
        viewProject.setRedirect(true);
        return viewProject;
    }
    
    public PageReference viewAdversaryAccount() {
        accId = ApexPages.currentPage().getParameters().get('accId');
        PageReference viewAccount = new PageReference('/apex/ViewAccount?accId='+accId);
        viewAccount.setRedirect(true);
        return viewAccount;
    }
    
    public PageReference confirmDelete() {
        projId = ApexPages.currentPage().getParameters().get('projId');
        PageReference confirmDelete = new PageReference('/apex/ConfirmProjectDelete');
        return confirmDelete;
    }
    
    public PageReference confirmAdversaryDelete() {
        accId = ApexPages.currentPage().getParameters().get('accId');
        PageReference confirmDelete = new PageReference('/apex/ConfirmAccountDelete');
        return confirmDelete;
    }
    
    public PageReference deleteProject() {
        try{
            String projId = ApexPages.currentPage().getParameters().get('projId');
            Project__c projToDelete = [select id from Project__c where id=:projId];
            delete projToDelete;
            PageReference projList = new PageReference('/apex/ProjectList');
            projList.setRedirect(true);
            return projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    public PageReference deactivateProject() {
        try{
            String projId = projIdToDeActivate;// ApexPages.currentPage().getParameters().get('projId');
            Project__c projToDeactivate = [select id, name, isActive__c, Client_Account__c,Client_Account__r.isActive__c,Adversary_Account__c, Adversary_Account__r.isActive__c from Project__c where id=:projId];
            
            if(projToDeactivate.Client_Account__c !=null && !projToDeactivate.Client_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.ClientAccountInactiveError));
                return null;
            } 
            if(projToDeactivate.Adversary_Account__c !=null && !projToDeactivate.Adversary_Account__r.isActive__c){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AdversaryAccountInactiveError));
                return null;
            } 
            String access = CNMSUtility.getCurrentUserProjectAccessLevel(projId);
            if(!(access == 'All' || access=='Edit')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.NoActionPrivilegeMessage));
                return null;
            }
            projToDeactivate.isActive__c = false;
            update projToDeactivate;
            projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Adversary_Account__r.isActive__c,Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c, Access__c from Project__c where isActive__c=true order by Name];
            createProjectListToDisplay();
            showInactiveProjects = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, projToDeactivate.name + ' ' + Label.ProjectDeactivationMessage));
            //PageReference projList = new PageReference('/apex/ProjectList');
            //projList.setRedirect(true);
            return null;//projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    public PageReference deleteAccount() {
        try{
            accId = ApexPages.currentPage().getParameters().get('accId');
            Adversary_Account__c accToDelete = [select id from Adversary_Account__c where id=:accId];
            delete accToDelete;
            PageReference projList = new PageReference('/apex/ProjectList');
            projList.setRedirect(true);
            return projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    public PageReference deactivateAccount() {
        try{
            accId = accIdToDeActivate; //ApexPages.currentPage().getParameters().get('accId');
            Adversary_Account__c accToUpdate = [select id,name__c ,isActive__c, access__c from Adversary_Account__c where id=:accId];
            accToUpdate.isActive__c = false;
            update accToUpdate;
            adversaryAccList = [select Name__c,Phone__c,Fax__c,Website__c, access__c from Adversary_Account__c where isActive__c=true];
            projectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Adversary_Account__r.isActive__c,Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c, Access__c from Project__c where isActive__c=true order by Name];
            inactiveProjectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c, Adversary_Account__r.isActive__c, Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c, Access__c from Project__c where isActive__c=false];
            createAdvAccListToDisplay();
            createProjectListToDisplay();
            createInactiveProjectListAccessible();
            showInactiveAdvAccounts = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, accToUpdate.name__c + ' ' + Label.AccountDeactivationMessage));
            //PageReference projList = new PageReference('/apex/ProjectList');
           // projList.setRedirect(true);
            return null; //projList;
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            return null;
        }
    }
    
    public PageReference cancel() {
        PageReference projList = new PageReference('/apex/ProjectList');
        projList.setRedirect(true);
        return projList;
    }
    
    public PageReference addContact(){
        PageReference redirectPage = new PageReference('/apex/CreateContact?source=home&accId='+ourAccId);
        return redirectPage;
    }
    
    public PageReference addContactRoster(){
        PageReference redirectPage = new PageReference('/apex/CreateContact?source=roster&accId='+ourAccId);
        return redirectPage;
    }
    
    public PageReference selectOurTeamMembers(){
        PageReference redirectPage = new PageReference('/apex/SelectOurTeamMembers');
        return redirectPage;
    }
    
    public PageReference showInactiveProjects(){
        PageReference redirectPage = new PageReference('/apex/AllInactiveProjects');
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    public PageReference showInactiveAdversaryAccounts(){
        PageReference redirectPage = new PageReference('/apex/AllInactiveAdversaryAccounts');
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    
    public pageReference activateProject(){
        System.debug('projIdToActivate****'+projIdToActivate);
        try{
            Project__c objProj = [select isActive__c,Name from Project__c where id=:projIdToActivate]; //ApexPages.currentPage().getParameters().get('projId')];
            objProj.isActive__c = true;
            update objProj;
            inactiveProjectList = [select Name,Client_Account__r.Name__c,Adversary_Account__r.Name__c,Adversary_Account__r.isActive__c,Project_Lead__c,Project_Lead__r.Member_Name__c,Description__c,Mission_and_Purpose__c,Adversary_Lead__r.Member_Name__c from Project__c where isActive__c=false];
            createInactiveProjectListAccessible();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, objProj.Name + ' ' +Label.ProjectActivationMessage));            
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }
        return null;
    }
    
    public PageReference activateAccount(){
        try{
            Adversary_Account__c objAcc = [select isActive__c, name__c from Adversary_Account__c where id=:accIdToActivate]; //ApexPages.currentPage().getParameters().get('accId')];
            objAcc.isActive__c = true;
            update objAcc;
            inactiveAdversaryAccList = [select Name__c,Phone__c,Fax__c,Website__c, access__c from Adversary_Account__c where isActive__c=false];
            createInactiveAdvAccListAccessible();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, objAcc.name__c + ' ' +Label.AccountActivationMessage)); 
        }
        catch(Exception e) {
            Error_Log.logError(e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, Label.ExceptionErrorMessage));
            
        }
            
        return null;
    }
}