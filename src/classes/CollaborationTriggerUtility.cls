public Class CollaborationTriggerUtility{
    static List<Collaboration__c> newRecords;
    static List<Collaboration__c> oldRecords;
    static Map<Id,Collaboration__c> oldRecordsMap;
    
    public static void setNewRecords(List<Collaboration__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Collaboration__c> records, Map<Id,Collaboration__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
  
    public static void ShareCollaborationWithOTMs(){    
        List<Collaboration__Share> CollabSharingforUpsert = new List<Collaboration__Share>();
        Map<Id, List<Id>> projectIdCollabListMapping = new Map<Id, List<Id>> ();  
        
         //Prepare collab id list.
        for(Collaboration__c objCollab : newRecords){
            if(objCollab.Project__c!=null){
                if(projectIdCollabListMapping.containsKey(objCollab.Project__c)){
                    projectIdCollabListMapping.get(objCollab.Project__c).add(objCollab.Id);
                }
                else{
                    projectIdCollabListMapping.put(objCollab.Project__c,new List<Id>{objCollab.id});
                }
            }   
        }             
              // Loop through OTMs to create various sharings.
        for(Our_Team_Member__c objOTM : [select id, User__c, Project__c, Role__c, Access__c from Our_Team_Member__c where Project__c IN :projectIdCollabListMapping.keySet() AND Role__c <> :Label.Observer]){
            // Consider only if OTM has project assigned.          
            String Access = objOTM.Access__c;                
            if(Access =='All'){
                Access = 'Edit'; // give edit access.
            } 
            if(Access==null || Access ==''){
                Access = 'None';
            }                
            // Covert View  to Read
            if(objOTM.Access__c=='View'){
                Access = 'Read';
            }
            System.debug('Access for this OTM is---->' + Access);
            //  COLLABORATION SHARING LOGIC START
            List<Id> collabIDsforThisProject = projectIdCollabListMapping.get(objOTM.Project__c);
            if(collabIDsforThisProject!=null && collabIDsforThisProject.size()>0){
                for(Id colabId: collabIDsforThisProject){ 
                    // create a new sharing
                    Collaboration__Share newCS = new Collaboration__Share(UserOrGroupId = objOTM.User__c, ParentId = colabId, AccessLevel = Access);
                    CollabSharingforUpsert.add(newCS);                           
                }
            }
            //  COLLABORATION SHARING LOGIC END          
        }     
        if(CollabSharingforUpsert.size()>0)
            Database.upsert(CollabSharingforUpsert,false);
    }
    
    public static void AutoFollowChatter(){ 
        Map<Id,Id> CollabIdProjId = new Map<Id,Id>();
        Map<Id,Collaboration__c> CollabIdCollab = new Map<Id,Collaboration__c>();
        Map<Id,List<Id>> projIdMembersId = new Map<Id,List<Id>>();
        for(Collaboration__c objCollab : newRecords){
            CollabIdProjId.put(objCollab.id,objCollab.Project__c);
            CollabIdCollab.put(objCollab.id,objCollab);
        }           
        List<Id> users = new List<Id>();
        //Query al the team members 
        for(Our_Team_Member__c objOTM : [select id,User__c,Project__c from Our_Team_Member__c where Project__c in:CollabIdProjId.values()]){
            if(projIdMembersId.containsKey(objOTM.project__c)){
                users = projIdMembersId.get(objOTM.project__c);
                users.add(objOTM.User__c);
                projIdMembersId.put(objOTM.project__c,users);
            }
            else{
               projIdMembersId.put(objOTM.project__c,new List<String>{objOTM.User__c}); 
            }        
        }   
        System.debug('projIdMembersId***'+projIdMembersId);
        List<EntitySubscription> ESToInsert = new List<EntitySubscription>();
        EntitySubscription es;
        for(Collaboration__c objCollab : CollabIdCollab.values()){
            if(CollabIdProjId.get(objCollab.id)!=null && projIdMembersId.get(CollabIdProjId.get(objCollab.id))!=null){
                for(Id memberID : projIdMembersId.get(CollabIdProjId.get(objCollab.id))){
                    es = new EntitySubscription();
                    es.ParentId = objCollab.id;
                    es.SubscriberId = memberID;
                    ESToInsert.add(es);
                }
            }    
        } 
        if(ESToInsert.size()>0){
          	Database.SaveResult[] lsr = Database.insert(ESToInsert,false);
			// Iterate through the Save Results
			for(Integer i = 0; i<lsr.size();i++){
			   Database.SaveResult sr = lsr[i];
			   if(!sr.isSuccess()){
			      Database.Error err = sr.getErrors()[0];
			      System.debug('------Entity Subscription Error For User---->\n' + ESToInsert[i].SubscriberId);
			      System.debug('\n------Error Is---->\n' + err);
			      System.debug('\n------Error Message is---->\n' + err.getMessage());
			   }   
			} 
   	    }	   
    }

}