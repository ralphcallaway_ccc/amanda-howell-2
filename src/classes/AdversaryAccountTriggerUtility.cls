public Class AdversaryAccountTriggerUtility{
    static List<Adversary_Account__c> newRecords;
    static List<Adversary_Account__c> oldRecords;
    static Map<Id,Adversary_Account__c> oldRecordsMap;
    
    public static void setNewRecords(List<Adversary_Account__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Adversary_Account__c> records, Map<Id,Adversary_Account__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
       
    public static void ShareNewAccountsWithAllUsers(){
        List<Adversary_Account__Share> AccountSharingList = new List<Adversary_Account__Share>();
        List<CNMS_Role__c> allCNMSRoles = [select user__c from CNMS_Role__c];
        for(Adversary_Account__c aa : newRecords){
            for(CNMS_Role__c role : allCNMSRoles ){
                Adversary_Account__Share newShare = new Adversary_Account__Share(UserOrGroupId = role.User__c, ParentId = aa.Id, AccessLevel = 'Read');
                AccountSharingList.add(newShare );
            }
        }
        
        if(AccountSharingList.size()>0){
            Database.Insert(AccountSharingList, false);
        }
    
    }
    
    public static void DeactivateContactsOnAccountDeactivation(){       
        List<Id>accIds = new List<Id>();        
        List<Adversary_Contact__c> contacts = new List<Adversary_Contact__c>();
        for(Adversary_Account__c a: newRecords){
            //Check if account is deactivated.
            if(oldRecordsMap.get(a.Id).isActive__c && !a.isActive__c){
                accIds.add(a.Id);               
            }
        }
        
        contacts = [select Id, isActive__c, Adversary_Account__c from Adversary_Contact__c where Adversary_Account__c In :accIds ];
        for(Adversary_Contact__c c: contacts){          
            c.isActive__c = false;
        }
        if(contacts.size()>0)
            update contacts ; 
    }
    
    public static void CloseProjectsOnAccountDeactivation(){       
        List<Id>accIds = new List<Id>();        
        List<Project__c> projects = new List<Project__c>();
        for(Adversary_Account__c a: newRecords){
            //Check if account is deactivated.
            if(oldRecordsMap.get(a.Id).isActive__c && !a.isActive__c){
                accIds.add(a.Id);               
            }
        }
        
        projects = [select Id, isActive__c from Project__c where Adversary_Account__c In :accIds AND isActive__c=true];
        for(Project__c p: projects ){          
            p.isActive__c = false;
        }
        if(projects.size()>0)
            update projects ; 
    }
    
    public static void CopyAccountName(){
        for(Adversary_Account__c a: newRecords){                      
            a.name = a.name__c;               
        }
        
    }
    
}