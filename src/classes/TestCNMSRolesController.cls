@isTest
private class TestCNMSRolesController {
     public static testMethod void testCNMSRolesController(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro;
        Project__c pro1 = new Project__c(name='test1',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro1;
        
        String[] RoleTypes = new String[]{Label.CEO,Label.COACH, Label.TeamMember};
        
        List<User> users = new List<User>();
        List<Our_Team_Member__c>  otms = new List<Our_Team_Member__c>();
        List<CNMS_Role__c>  roles = new List<CNMS_Role__c>();
        // Create total 10 users (3+5, 2 extra)
        for(Integer i = 0; i<10; i++){
            User u = createUser('testing'+i+'@testorg.com', 't'+i);
            u.isTeamMember__c = true;
            u.Client_Account__c = acc.id;
            u.isActive = true;
            u.lastname='Testing'+i;
            u.firstname = 'Testing'+i;
            users.add(u);
            
        } 
        insert users;
        
        // Create total 3 Roles
        for(Integer i = 0; i<3; i++){
            CNMS_Role__c rl = new CNMS_Role__c();
            rl.User__c = users[i].Id;
            rl.Role_Type__c = RoleTypes[i];
            roles.add(rl);
        } 
        //create a role for current user
        roles.add(new CNMS_Role__c(User__c = users[3].Id, Role_Type__c = Label.TeamMember));
        insert roles;
        //Create total 3 TMs. user0,1 and 2.
        for(Integer i = 0; i<3; i++){
            Our_Team_Member__c tm = new Our_Team_Member__c (User__c =users[i].Id, Project__c = pro.id); 
            tm.Role__c = RoleTypes[i];
            tm.Access__c = 'View';
            otms.add(tm);            
        }         
        insert otms ;
        
        PageReference currentPage = new PageReference('/apex/CNMSRoles');            
        Test.setCurrentPage(currentPage );
        CNMSRolesController CNMSRolesController = new CNMSRolesController();
        
        //Verify data is prepared as per expectation.       
        System.assert(CNMSRolesController.getCEOCNMSRolesCount()== CNMSRolesController.ceoCNMSRoles.size()); // CEO.
        System.assert(CNMSRolesController.getCoachCNMSRolesCount()== CNMSRolesController.coachCNMSRoles.size()); // COACH.      
        System.assert(CNMSRolesController.getTMCNMSRolesCount()== CNMSRolesController.TMCNMSRoles.size()); // TM.
       
        CNMSRolesController.UpdateCNMSRoles(); 
        CNMSRolesController.back();                
    }
    
     private static User createUser(String eMail, String alias){ 
      Profile  p = [select id from profile where name='System Administrator'];
      User testUser = new User(alias = alias, email=eMail,
      emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US', profileid = p.Id, country='United States',
      timezonesidkey='America/Los_Angeles', username=eMail, isActive=true);
      
      return testUser;
   }

}