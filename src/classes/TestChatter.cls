@isTest
private class TestChatter {
     public static testMethod void testChatterAccess(){
        Client_Account__c acc = new Client_Account__c (name__c='client');
        acc.isActive__c = true;
        insert acc;
    
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id, Client_Account__c = acc.Id );
        insert pro;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        
        Checklist__c cl = new Checklist__c ( Checklist_Name__c='Test Checklist', Project__c = pro.id);
        insert cl;
        
        Collaboration__c collab = new  Collaboration__c(Checklist__c=cl.id,Related_Field__c='date');
        insert collab;
        
        update collab;
        
        FeedItem a = new FeedItem(parentId=collab.id,body='test');
        insert a; 
        
        FeedComment b = new FeedComment(CommentBody='test',FeedItemId=a.id);
        insert b;
        
        }
}