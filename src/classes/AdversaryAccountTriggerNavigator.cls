public Class AdversaryAccountTriggerNavigator{
    List<Adversary_Account__c> newRecords;
    List<Adversary_Account__c> oldRecords;
    //Constructor
    public AdversaryAccountTriggerNavigator(List<Adversary_Account__c> TriggerNewRecords,  List<Adversary_Account__c> TriggerOldRecords, Map<Id,Adversary_Account__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        AdversaryAccountTriggerUtility.setNewRecords(newRecords);
        AdversaryAccountTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void ShareNewAccountsWithAllUsers (){
        AdversaryAccountTriggerUtility.ShareNewAccountsWithAllUsers();
    }
    
    public void DeactivateContactsOnAccountDeactivation (){
        AdversaryAccountTriggerUtility.DeactivateContactsOnAccountDeactivation();
    }
    
    public void CloseProjectsOnAccountDeactivation(){
        AdversaryAccountTriggerUtility.CloseProjectsOnAccountDeactivation();
    }
    
    public void CopyAccountName(){
        AdversaryAccountTriggerUtility.CopyAccountName();
    }
    
}