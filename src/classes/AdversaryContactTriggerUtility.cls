public Class AdversaryContactTriggerUtility{
    static List<Adversary_Contact__c> newRecords;
    static List<Adversary_Contact__c> oldRecords;
    static Map<Id,Adversary_Contact__c> oldRecordsMap;
    
    public static void setNewRecords(List<Adversary_Contact__c> records){
        newRecords = records;
    }
    
    public static void setOldRecords(List<Adversary_Contact__c> records, Map<Id,Adversary_Contact__c> RecordsMap){
        oldRecords = records;
        oldRecordsMap = RecordsMap;
    }
       
    public static void SetUniqueName(){
        for(Adversary_Contact__c objCon : newRecords){
            objCon.Unique_Name__c = objCon.First_Name__c+''+objCon.Last_Name__c+''+objCon.Adversary_Account__c;
            objCon.Unique_Name__c.trim();
        }
    }
    
    public static void DeleteRelatedAdversaryTeamMembers(){
        delete [select id from Adversary_Team_Member__c where Adversary_Contact__c IN :oldRecordsMap.KeySet()];
    }

}