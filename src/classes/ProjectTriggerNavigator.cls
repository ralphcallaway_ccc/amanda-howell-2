public Class ProjectTriggerNavigator{
    List<Project__c> newRecords;
    List<Project__c> oldRecords;
    //Constructor
    public ProjectTriggerNavigator(List<Project__c> TriggerNewRecords,  List<Project__c> TriggerOldRecords, Map<Id,Project__c> TriggerOldRecordsMap){
        newRecords = TriggerNewRecords;
        oldRecords = TriggerOldRecords;
        ProjectTriggerUtility.setNewRecords(newRecords);
        ProjectTriggerUtility.setOldRecords(oldRecords, TriggerOldRecordsMap);        
    }
    
    public void DeleteTeamMembersOnAccountChange (){
        ProjectTriggerUtility.DeleteTeamMembersOnAccountChange();
    }
    
    public void UpdateCollaborationOwnerOnOwnerChange (){
        ProjectTriggerUtility.UpdateCollaborationOwnerOnOwnerChange();
    }
    
    public void UpdateOverallMission(){
        ProjectTriggerUtility.UpdateOverallMission();
    }
    
    public void UnsubscribeOnProjectDeactivation(){
        ProjectTriggerUtility.UnsubscribeOnProjectDeactivation();
    }
    
    public void SubscribeOnProjectActivation(){
        ProjectTriggerUtility.SubscribeOnProjectActivation();
    }
    
}