@isTest
private class TestNewLogController {
     public static testMethod void TestNewLogController(){  
        
        Adversary_Account__c advacc = new Adversary_Account__c (name__c='client');
        advacc.isActive__c = true;
        insert advacc ;
        
        Project__c pro = new Project__c(name='test',Adversary_Account__c=advacc.id);
        insert pro;
        
        Checklist__c cl = new Checklist__c (Project__c = pro.id, Checklist_Name__c='Test Project');
        insert cl;
        
        Checklist__c cl2 = new Checklist__c (Project__c = pro.id, Checklist_Name__c='Test Project 2'); // will be used for Log updation.
        insert cl2;
        
        Log__c log = new Log__c(Project__c = pro.id, Log_Name__c='Test Log', Checklist__c = cl.Id );
        insert log;
        
        CNMS_Role__c r3 = new CNMS_Role__c(User__c = UserInfo.getUserId(), Role_Type__c = Label.TeamMember);
        insert r3;  
        Our_Team_Member__c otm = new Our_Team_Member__c(User__c = UserInfo.getUserId(), project__c = pro.Id, Access__c = 'Edit', Role__c = Label.TeamMember);
        insert otm;
        
        PageReference currentPage = new PageReference('/apex/createLog');
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('Id',log.id);
        currentPage.getParameters().put('clId',cl.id);
        Test.setCurrentPage(currentPage );
        
         // Construct the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(Log);
 
        // create the controller
         
        NewLogController newLogController = new NewLogController(sc);
         
         //Test  back Method
        PageReference testPage = newLogController.back();
        System.assert(testPage.getUrl().contains('ViewProject'));                
        System.assertEquals(pro.id, testPage.getParameters().get('Id'));
        
        //Test  cancel Method
        testPage = newLogController.cancel();        
        System.assert(testPage.getUrl().contains('ViewLog'));
        System.assertEquals(log.id, testPage.getParameters().get('id'));
        
        //Test  edit Method
        testPage = newLogController.edit();        
        System.assert(testPage.getUrl().contains('EditLog'));
        
        //Test various Save methods in order to cover exception
        testPage = newLogController.save(); // Exception as existing Log cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        testPage = newLogController.saveAndNew(); // Exception as existing Log cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        testPage = newLogController.saveAndCreateCheckList(); // Exception as existing Log cannot be re-inserted.   
        System.assert(ApexPages.hasMessages(ApexPages.severity.FATAL));
        
        // Test Update Method
        newLogController.log.CheckList__c = cl2.Id;
        testPage = newLogController.updateLog();        
        System.assert(testPage.getUrl().contains('ViewLog'));
        Log__c updatedlg = [select CheckList__c from Log__c where id= :newLogController.log.Id];
        System.Assert(updatedlg.CheckList__c == cl2.Id);
        
        // Start testing with New log creation...
        currentPage.getParameters().put('projId',pro.id);
        currentPage.getParameters().put('Id',null);
		currentPage.getParameters().put('Log_Name__c','Test New Log');
        currentPage.getParameters().put('clId',cl.id);
        Test.setCurrentPage(currentPage );
        
        
        
        //Test save n New
        testPage = newLogController.saveAndNew();                
        List<Log__c> newlgs = [select Id from Log__c where CheckList__c= :cl.Id];
        System.Assert(newlgs.size()>0);
        
        // Test save
        newLogController.NotifyTM  = true;
        testPage = newLogController.save();                
        newlgs = [select Id from Log__c where CheckList__c= :cl.Id];
        System.Assert(newlgs.size()>1);//As ther is already one log created using save n new, hence verify that more than one log are there.
        
        // Test saveAndCreateCheckList
        newLogController.log =  new Log__c(Project__c = pro.id, Checklist__c = cl.Id );
        testPage = newLogController.saveAndCreateCheckList();                
        newlgs = [select Id from Log__c where CheckList__c= :cl.Id];
        System.Assert(newlgs.size()>2);//As ther are already 2 log created using save n new,and save. hence verify that more than one log are there.
        System.assert(testPage.getUrl().contains('createChecklist'));
       
        // Test getCollaborationData
        testPage = newLogController.getCollaborationData();
        //System.assert(newLogController.checklistCollabId !=null);
        //System.assert(newLogController.dateCreatedCollabId !=null);        
        System.assert(newLogController.discussCollabId !=null);
        System.assert(newLogController.emotionCollabId !=null);
        System.assert(newLogController.energyCollabId !=null);
        System.assert(newLogController.moneyCollabId !=null);
        System.assert(newLogController.potentialCollabId !=null);
        //System.assert(newLogController.projectCollabId !=null);
        //System.assert(newLogController.timeCollabId !=null);
        System.assert(newLogController.whoIsCollabId !=null);
        System.assert(newLogController.yourThoughtsCollabId !=null);
       

  
    }

}