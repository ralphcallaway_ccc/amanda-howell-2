trigger ProjectBeforeUpdate on Project__c (before update) {
    ProjectTriggerNavigator ptn = new ProjectTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    ptn.DeleteTeamMembersOnAccountChange();    
}