trigger AdversaryAccountBeforeInsert on Adversary_Account__c (Before Insert) {
    AdversaryAccountTriggerNavigator aatn = new AdversaryAccountTriggerNavigator(Trigger.new, null, null);
    aatn.CopyAccountName();
}