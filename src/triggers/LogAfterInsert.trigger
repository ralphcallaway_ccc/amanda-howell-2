trigger LogAfterInsert on Log__c(after insert) {
    LogTriggerNavigator ltn = new LogTriggerNavigator(Trigger.new, null, null);
    ltn.NotifyTeamMembersOnLogCreation();    
}