trigger OTMAfterUpdate on Our_Team_Member__c (After Update) {
    OTMTriggerNavigator otmNavigator = new OTMTriggerNavigator(Trigger.New, Trigger.Old); // Instantiate navigator
    
    otmNavigator.PopulateProjectLead();//Populate proj lead on projects.
    otmNavigator.updateSharingAccess();
    
}