trigger AdversaryContactBeforeUpdate on Adversary_Contact__c (Before update) {
    AdversaryContactTriggerNavigator actn = new AdversaryContactTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    actn.SetUniqueName();
}