trigger UserBeforeUpdate on User (before update) {
    //Check if a client account exists with name as organization name
    Client_Account__c companyAccount;
    String orgName = UserInfo.getOrganizationName();
    try{
        companyAccount = [select Id from Client_Account__c where name__c=:orgName Limit 1];
    }
    catch(Exception e){
        companyAccount = new Client_Account__c();
        companyAccount.name__c = orgName;
        companyAccount.isActive__c = true;
        insert companyAccount;
    }    
    
    
    for(User objUser : Trigger.new){
        //For users using Jim's system, we will use partner users
        /*
        if(objUser.isPartner){
            //Logic goes here
        }
        */
        
        
        
        //For the users of organization using this as package
        if(objUser.isTeamMember__c)
           objUser.Client_Account__c = companyAccount.id;
    }
}