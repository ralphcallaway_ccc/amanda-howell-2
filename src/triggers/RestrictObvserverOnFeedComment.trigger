trigger RestrictObvserverOnFeedComment on FeedComment (before insert) {
    //Get Checklist and Log Prefix
    Schema.DescribeSObjectResult CollaborationDesc = Collaboration__c.SObjectType.getDescribe();
    String collabPrefix = CollaborationDesc.getKeyPrefix();
    List<FeedComment> collabFeeds = new List<FeedComment>();
    Set<Id> parentCollabIds = new Set<ID>();
    Set<Id> projectIds = new Set<ID>();
    Map<Id,Collaboration__c> CollabIdToCollabMap = new Map<ID,Collaboration__c>();
    Set<Id> projectWithObserverRole = new Set<ID>();
    //Schema.DescribeSObjectResult L = Log__c.SObjectType.getDescribe();
    //String logtPrefix = L.getKeyPrefix();
    
    for(FeedComment fi: Trigger.new){
        if(String.ValueOf(fi.parentId).startsWith(collabPrefix)){
            collabFeeds.add(fi);
            parentCollabIds.add(fi.parentId);
        }
    }
    
    if(parentCollabIds.size()>0){
        for(Collaboration__c collab: [select id, Project__c from Collaboration__c where id IN :parentCollabIds]){
            CollabIdToCollabMap.put(collab.Id, collab);
            projectIds.add(collab.Project__c);
        }
        
        for(Our_Team_Member__c otm : [select id, Project__c from Our_Team_Member__c where User__c = :UserInfo.getUserID() AND Role__c = :Label.Observer AND Project__c IN :projectIds]){      
            projectWithObserverRole.add(otm.Project__c);
        }
        
        if(projectWithObserverRole.size()>0){
            for(FeedComment fi: Trigger.new){
                if(projectWithObserverRole.contains(CollabIdToCollabMap.get(fi.parentId).Project__c)){
                   fi.addError('You are not allowed to comment on this feed as you are the observer. Please contact system admin.');
                }
            }
        }
    }
    
        
}