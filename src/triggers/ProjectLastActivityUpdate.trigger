trigger ProjectLastActivityUpdate on Collaboration__c (after insert, after update) {
    
    // map tracks records on Custom Collaboration ID
    
    Map<String, Collaboration__c> CollaborationMap = new Map<String, Collaboration__c>();
    Map<Id, Project__c> ProjectMap = new Map<Id, Project__c>{};
    
    for( Collaboration__c record : Trigger.new )
    {
       
ProjectMap.put(record.Project__c, new Project__c(Id=record.Project__c, LAST_Activity_Date__c = record.LastModifiedDate.date()));

    }
    
        
    if(!ProjectMap.isEmpty())    
    {        
        // Save Prior Last Activity value to the database
        Database.SaveResult[] results = Database.update(ProjectMap.values());  
        for( Database.SaveResult result : results )
        if( !result.isSuccess() ) System.debug( '<< '
        + result.getErrors().size() + ' update error(s) on Projects: ' + result.getErrors()[0].getMessage() );
    }       
}