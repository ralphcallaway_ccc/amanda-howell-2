trigger CNMSRoleAfterInsert on CNMS_Role__c (after Insert) {
    CNMSRoleTriggerNavigator crtn = new CNMSRoleTriggerNavigator(Trigger.new, null, null);
    crtn.createOTMForCEORoles();
    crtn.ShareAccountsWithNewUserRole();    
}