trigger ProjectStatusUpdateTeam on Project__c (after update) {

    public Boolean NotifyTM {get;set;}

 // map tracks records on Opportunity ID

    Map<String, Project__c> ProjectMap = new Map<String, Project__c>();
    private void init(){
     }

    for( Project__c record : Trigger.new )
    {
        if( record.Project_Status__c != trigger.oldMap.get(record.id).Project_Status__c && (record.Project_Status__c == 'Closed – Fadeaway' || record.Project_Status__c == 'Need Assistance' ))

        {
              ProjectMap.put( record.ID, record );   
              CNMSUtility.sendProjectEmail(record.Id, record.Name, true);         
        }
        
    }


}