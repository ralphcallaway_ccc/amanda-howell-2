trigger CNMSRoleBeforeUpdate on CNMS_Role__c (before update) {
    CNMSRoleTriggerNavigator crtn = new CNMSRoleTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    crtn.updateRolesOperations();
}