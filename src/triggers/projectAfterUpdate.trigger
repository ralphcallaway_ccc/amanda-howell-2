trigger projectAfterUpdate on Project__c (after Update) {
    ProjectTriggerNavigator ptn = new ProjectTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    ptn.UpdateCollaborationOwnerOnOwnerChange();
    ptn.UpdateOverallMission();
    ptn.UnsubscribeOnProjectDeactivation();
    ptn.SubscribeOnProjectActivation();
}