trigger AdversaryAccountAfterUpdate on Adversary_Account__c(after update) {
    AdversaryAccountTriggerNavigator aatn = new AdversaryAccountTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    aatn.DeactivateContactsOnAccountDeactivation();  
}