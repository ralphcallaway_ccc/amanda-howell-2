trigger CollaborationAfterInsert on Collaboration__c (after insert) {
    CollaborationTriggerNavigator ctn = new CollaborationTriggerNavigator(Trigger.new,null,null);
    ctn.ShareCollaborationWithOTMs();
    ctn.AutoFollowChatter();
}