trigger AssignChatterPermissions on Our_Team_Member__c (after insert,after delete) {
    Map<Id,Id> OTMIdProjIdToAdd = new Map<Id,Id>();
    Map<Id,Id> OTMIdProjIdToRemove = new Map<Id,Id>();
    Map<Id,Id> OTMIdUserId = new Map<Id,Id>();
    Set<Id> ProjectIds = new Set<Id>();
    List<Id> CollabIds = new List<Id>();
    Map<Id,Id> CollabIdProjId = new Map<Id,Id>();
    Map<Id,List<Id>> ProjIdCollabIdList = new Map<Id,List<Id>>();
    Set<EntitySubscription> ESToInsert = new Set<EntitySubscription>();
    List<EntitySubscription> ESToInsertList = new List<EntitySubscription>();
    List<EntitySubscription> ESToDelete = new List<EntitySubscription>();
    EntitySubscription es;
    
    if(Trigger.isInsert){
        for(Our_Team_Member__c objOTM : Trigger.new){
            if(objOTM.Role__c != Label.Observer){     // skip observer        
                OTMIdProjIdToAdd.put(objOTM.id,objOTM.Project__c);
                ProjectIds.add(objOTM.Project__c);
                OTMIdUserId.put(objOTM.id,objOTM.User__c);  
            } 
        }        
    }
    
    if(Trigger.isDelete){
        for(Our_Team_Member__c objOTM : Trigger.old){
            if(objOTM.Role__c != Label.Observer){  // skip observer  
                OTMIdProjIdToRemove.put(objOTM.id,objOTM.Project__c);
                ProjectIds.add(objOTM.Project__c);  
                OTMIdUserId.put(objOTM.id,objOTM.User__c);
            }
        }
    }
    
    System.debug('OTMIdProjIdToAdd****'+OTMIdProjIdToAdd);
    System.debug('ProjectIds****'+ProjectIds);
    System.debug('OTMIdUserId****'+OTMIdUserId);
    
    
    for(Collaboration__c objCollab : [select id,Project__c from Collaboration__c  where Project__c in:ProjectIds]){
        CollabIds.add(objCollab.id);
        CollabIdProjId.put(objCollab.id,objCollab.Project__c);
        if(ProjIdCollabIdList.containsKey(objCollab.Project__c)){
            collabIds = ProjIdCollabIdList.get(objCollab.Project__c);
            collabIds.add(objCollab.id);
            ProjIdCollabIdList.put(objCollab.Project__c,collabIds);
        }
        else
           ProjIdCollabIdList.put(objCollab.Project__c,new List<Id>{objCollab.id});
    }
    
    System.debug('ProjIdCollabIdList****'+ProjIdCollabIdList);
    System.debug('OTMIdProjIdToAdd****'+OTMIdProjIdToAdd);
    
    if(Trigger.isInsert){
        //Add access to existing collaboration for new members
        for(Id OTMId : OTMIdProjIdToAdd.keySet()){
            for(Id projId : ProjIdCollabIdList.keySet()){
                if(OTMIdProjIdToAdd.get(OTMId)==projId){
                    for(Id collabId : ProjIdCollabIdList.get(projId)){
                        es = new EntitySubscription();
                        es.SubscriberId = OTMIdUserId.get(OTMId);
                        es.ParentId = collabId;
                        ESToInsert.add(es);
                    } 
                }
            }
        }
        
        System.debug('ESToInsert****'+ESToInsert);
        
        if(ESToInsert.size()>0){
            ESToInsertList.addAll(ESToInsert);
            System.debug('ESToInsert size****'+ESToInsert.size());
            insert ESToInsertList;
        }
    }
    
    if(Trigger.isDelete){
        
        Map<Id,Id> CollabIdSubscriberId = new Map<Id,Id>();
        //Remove access to existing collaboration for members who have been removed
        for(Id OTMId : OTMIdProjIdToRemove.keySet()){
            for(Id projId : ProjIdCollabIdList.keySet()){
                if(OTMIdProjIdToRemove.get(OTMId)==projId){
                    for(Id collabId : ProjIdCollabIdList.get(projId)){
                        CollabIdSubscriberId.put(collabId,OTMIdUserId.get(OTMId));
                    } 
                }
            }
        }
        
        System.debug('CollabIdSubscriberId****'+CollabIdSubscriberId);
        
        if(CollabIdSubscriberId.size()>0){
            for(EntitySubscription objES : [select id,SubscriberId,ParentId from EntitySubscription where ParentId in: CollabIdSubscriberId.keySet()]){
                if(CollabIdSubscriberId.get(objES.ParentId)==objES.SubscriberId)
                    ESToDelete.add(objES);
            }
        }
        
        if(ESToDelete.size()>0)
            delete ESToDelete;
        
        
    }
    
}