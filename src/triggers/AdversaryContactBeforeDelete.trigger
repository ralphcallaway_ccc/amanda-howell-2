trigger AdversaryContactBeforeDelete on Adversary_Contact__c (Before delete) {
    AdversaryContactTriggerNavigator actn = new AdversaryContactTriggerNavigator(null,Trigger.old, Trigger.oldMap);
    actn.DeleteRelatedAdversaryTeamMembers();
}