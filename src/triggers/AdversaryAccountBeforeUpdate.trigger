trigger AdversaryAccountBeforeUpdate on Adversary_Account__c(Before update) {
    AdversaryAccountTriggerNavigator aatn = new AdversaryAccountTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);    
    aatn.CloseProjectsOnAccountDeactivation();
}