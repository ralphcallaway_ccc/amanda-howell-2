trigger CollaborationAfterUpdate on Collaboration__C (after update) {
    CollaborationTriggerNavigator ctn = new CollaborationTriggerNavigator(Trigger.new, Trigger.old, Trigger.oldMap);
    ctn.AutoFollowChatter();   
}