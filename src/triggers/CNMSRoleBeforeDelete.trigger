trigger CNMSRoleBeforeDelete on CNMS_Role__c (before delete) {
	CNMSRoleTriggerNavigator crtn = new CNMSRoleTriggerNavigator(null, Trigger.old, Trigger.oldMap);
    crtn.preventRoleDeletionForProjectowners();
}