trigger OTMAfterDelete on Our_Team_Member__c (After Delete) {
    OTMTriggerNavigator otmNavigator = new OTMTriggerNavigator(null, Trigger.Old); // Instantiate navigator
    otmNavigator.DeleteChatterPermissions();//DeleteChatterPermission.
    otmNavigator.deleteSharingAccess();//deleteSharingAccess.
}