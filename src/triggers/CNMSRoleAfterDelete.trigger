trigger CNMSRoleAfterDelete on CNMS_Role__c (After delete) {
    CNMSRoleTriggerNavigator crtn = new CNMSRoleTriggerNavigator(null, Trigger.old, Trigger.oldMap);
    crtn.UnshareAccountsWithDeletedUserRole();
    crtn.deleteRelatedOTMS();
}