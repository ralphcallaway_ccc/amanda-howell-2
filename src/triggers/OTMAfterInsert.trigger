trigger OTMAfterInsert on Our_Team_Member__c (After Insert) {
    OTMTriggerNavigator otmNavigator = new OTMTriggerNavigator(Trigger.New, Trigger.Old); // Instantiate navigator
    
    otmNavigator.PopulateProjectLead();//Populate proj lead on projects.
    otmNavigator.AssignChatterPermissions();//assign chatter permission.
    otmNavigator.updateSharingAccess(); // updateSharing
}