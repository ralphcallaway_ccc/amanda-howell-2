trigger ChecklistAfterInsert on Checklist__c (after insert) {
    ChecklistTriggerNavigator ctn = new ChecklistTriggerNavigator(Trigger.new, null, null);
    ctn.NotifyTeamMembersOnCheklistCreation();
}